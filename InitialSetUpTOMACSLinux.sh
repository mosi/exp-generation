
cd ..

# download and install provenance editor
git clone -b ProvenancModel "https://git.informatik.uni-rostock.de/mosi/provenance-editor.git"
cd provenance-editor/
mvn clean install -U
cd ..

# download and install provenance gui
git clone "https://git.informatik.uni-rostock.de/mosi/GREASE/provenance-gui.git"
cd provenance-gui/
mvn clean install -U
cd ..

# download case study material - migration
git clone "https://github.com/jasonhilton/screen_run.git"
git clone "https://github.com/mhinsch/rgct_data.git"

# download case study material - wnt cross-validation
mkdir BIOMD0000000658
wget https://www.ebi.ac.uk/biomodels/model/download/BIOMD0000000658.3 -O BIOMD0000000658.omex
unzip BIOMD0000000658.omex -d BIOMD0000000658

