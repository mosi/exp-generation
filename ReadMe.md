# Automatic Simulation Experiment Generator

## Usages

The automatic experiment generator can be used in a variety of ways:
* **Specification, generation and execution via a GUI:**
Running the main program without any arguments will open the GUI, the experiment editor.
There, you can choose from a number of simulation domains for your base experiment, 
and from a number of experiment types for your experiment design.
Clicking the Generate Experiment button will produce two experiment specifications.
One in our intermediate JSON format, and one in the specification language of the selected backend.
If a binding exists to the selected backend, by clicking on the Generate Experiment button
the generated script will also be executed automatically.
The simulation results will appear in the same folder where your simulation model is located.

* **Loading of existing specifications:**
When using the option `-l fileName`, an existing experiment specification (given as JSON file) can be loaded to the GUI
to be adapted and generated again.

* **Code transformations:** 
The experiment generator can be used to transform simulation experiment specifications between backends.
The file containing the current experiment as well as the name of the target backend need to be specified:
`-t fileName backendName`.
The new file will be generated in the same folder as the old specification.

* **Provenance-based reuse:**
Provenance provides important meta information about the development story of a simulation model.
This information is exploited by our provenance-based generation mechanism.
Whenever an activity is added into the application, it checks
whether the there exists a previous simulation experiment that needs to be repeated or if information about
previous simulation experiments and their context can be used to generate a new experiment.
For instance, a model building step might trigger the re-calibration of the simulation model.
To use this provenance-based experiment generation, the provenance graph of the simulation study needs to 
be provided as a case study. 
This project currently includes two case studies, the Wnt simulation study, and the migration simulation study. 
To run these studies, start the program providing the identifier of the simulation study, e.g., `-c WNT true` or 
`-c MIGRATION` respectively.
Use the option `-s` to skip the automatic execution of the generated simulation experiments. This will produce a 
dummy entity for the simulation data that can be filled later.

## Setup

1. Make sure you have Git installed on your system. To install git for Windows see https://git-scm.com/download/win.
   For Linux, see https://git-scm.com/download/linux.

2. Maven is used to manage project dependencies. To install Maven for Windows see http://maven.apache.org/install.html.
   To install it for Linux see https://www.journaldev.com/33588/install-maven-linux-ubuntu.

3. Using git, check out this repository in the folder, where you want this project to live. 

4. On Windows, run the script *InitialSetUpWindows.bat* and on Linux run the script *InitialSetUpLinux.sh*.
   This downloads two additional repositories that provide the provenance functionalities, as well as two repositories
   needed to run the case studies. 

5. Note that the migration case study requires a compute cluster with Torque PBS to be set up.
   Instructions, e.g., for setting up a local single node cluster can be found here: https://www.programmersought.com/article/41867156748/

6. Once you set up your cluster, rename the file *remote* to *remote.config* and enter the information about your server. In addition, change *pwd* to *pwd.config* and store your password in the file.

7. On Windows, to establish an SSH connection to the server, PuTTY needs to be installed: https://www.putty.org/

8. Read instructions on how to reproduce the case studies [here](https://git.informatik.uni-rostock.de/mosi/exp-generation/-/tree/master/src/main/resources/case-studies/TOMACS/ReadMe.md).

## Publications
The experiment generator was developed and applied in a number of publications:
 * Pia Wilsdorf, Nadine Fischer, Fiete Haack, and Adelinde M. Uhrmacher (2021) Exploiting Provenance and Ontologies in Supporting Best Practices for Simulation Experiments: A Case Study on 
   Sensitivity Analysis. In: *2021 Winter Simulation Conference (WSC)*.
 * Pia Wilsdorf, Anja Wolpers, Jason Hilton, Fiete Haack, and Adelinde M. Uhrmacher (2021) Automatic Reuse, Adaption, 
   and Execution of Simulation Experiments via Provenance Patterns. *arXiv, Cornell University*. 
   https://arxiv.org/abs/2109.06776
 * Pia Wilsdorf, Jakob Heller, Kai Budde, Julius Zimmermann, Tom Warnke, Christian Haubelt, Dirk Timmermann,  
   Ursula van Rienen, and Adelinde M. Uhrmacher (2021) A Model-Driven Approach for Conducting Simulation Experiments.
   *TechRxiv*. https://doi.org/10.36227/techrxiv.16836868.v1
 * Andreas Ruscheinski, Pia Wilsdorf, Julius Zimmermann, Ursula van Rienen, and Adelinde M. Uhrmacher (2020)
   An Artifact-based Workflow for Finite-Element Simulation Studies. *arXiv, Cornell University*. 
   https://arxiv.org/abs/2010.07625
 * Pia Wilsdorf, Julius Zimmermann, Marcus Dombrowsky, Ursula van Rienen, and Adelinde M. Uhrmacher (2019) Simulation 
   Experiment Schemas - Beyond Tools and Simulation Approaches. In: *2019 Winter Simulation Conference (WSC)*. 
   https://doi.org/10.1109/WSC40007.2019.9004710
 * Andreas Ruscheinksi, Kai Budde, Tom Warnke, Pia Wilsdorf, Bjarne Christian Hiller, Marcus Dombrowsky, and 
   Adelinde M. Uhrmacher (2018) Generating Simulation Experiments Based on Model Documentations and Templates.
   In: *2018 Winter Simulation Conference (WSC)*. https://doi.org/10.1109/WSC.2018.8632515


## Development
The experiment generation project provides plenty of opportunities for extension.
In particular, we plan to grow the knowledge base about modeling and simulation (i.e., the application domains,
the tools, the experiment types and designs, the activities and activity patterns, etc.)
to further automate simulation experiments and thus simulation studies.

In the following we describe the different areas, where extensions are possible.

### Adding new Application Domains
To add a new application domain for which automatic experiment generation shall be enabled, 
you need to create a new schema in src/main/resources/schemas and integrate this with the experiment
generation engine located in src/main/java/org/mosi/grease/experimentgeneration.

### Adding new Experiment Types
The experiment generator currently supports a range of commonly used experiment types such
as parameter scans, local sensitivity analysis, global sensitivity analysis, statistical 
model checking, and convergence analysis.
However, many more could be added to support even bigger, more realistic simulation studies. 
To add a new experiment type, simply add a new schema to src/main/resources/Types and register it with
the generation engine.

### Integrating new Backends
Currently, a number of tools and languages are supported for executing the simulation experiments.
These include SESSL, Python, R and Julia, YAML and EMStimTools, as well as SED-ML.
If you intend to use another modeling and simulation tool,
this requires creating a new backend controller in src/main/java/org/mosi/grease/backend as
well as defining the templates for the code generation within src/main/resources/templates.

### Creating new Provenance-based Generation Rules
A rule consists of three parts.
First, a trigger component that is used to check if the latest activity could be followed
by an experiment generation step.
Then, a pattern filter that searches for specific previous simulation experiments that 
fulfill a set of conditions. 
And finally, a generation component that reuses the information from the matched information
from the other two components together to create a new simulation experiment.
These can be flexibly combined to create new experiment generation rules.

### Creating new Adaption Rules
Sometimes, reusing a previous simulation experiment requires making adaptations to the old 
experiment based on the new context. 
These adaptations are performed on the intermediate JSON representation of the experiment. 
To implement further adaption rules that, for example, include information about available 
validation data, the AdaptionController in src/main/java/org/mosi/grease/provenancefiltering/controllers
needs to be extended.

### Extending the Provenance Model
Provenance information plays a crucial role, when automatically generating 
simulation experiments for a simulation study.
The currently implemented provenance model covers the most frequently used
artifacts in a simulation study such as Simulation Model, Simulation Experiment,
Simulation Data, Research Question, Assumption, and Requirement.
However, some application domains require further entity or activity
types to be specified in the provenance model. 
This can be done by creating new node types in src/main/java/org/mosi/grease/provenancefiltering/model.

### Creating own Case Studies
If you are satisfied with the current feature set, you can simply create a new case study to run 
and automate you simulation experiments.
This can be done by creating a case study controller and a graph generator in a separate folder in
src/main/java/org/mosi/grease/caseStudies.
When configuring the case study, many options exist, e.g., the predefined rule set can be customized
depending on the degree of support that is needed, and graphical support can be enabled or disabled on demand.

## License 
GNU GPLv3