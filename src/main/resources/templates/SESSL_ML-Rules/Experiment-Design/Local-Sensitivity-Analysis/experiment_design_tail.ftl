<#macro experiment_design_tail>
) using new TwoLevelFullFactorialSetup {
    baseCase(
        <#list factorNames as name >"${name}" <~ ${baseCaseValues[name?index]}<#if name_has_next>,</#if></#list>
    )
    sensitivityCase(
        <#list factorNames as name>"${name}" <~ ${sensitivityCaseValues[name?index]}<#if name_has_next>, </#if></#list>
    )
    withAnalysisResult(result => {
        result.printCompleteReport()
    })
}
</#macro>