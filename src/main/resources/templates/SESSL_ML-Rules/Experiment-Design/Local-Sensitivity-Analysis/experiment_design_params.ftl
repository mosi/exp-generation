<#macro experiment_design_params>
        for ((name, value) <- params) {
            set(name <~ value)
        }
</#macro>