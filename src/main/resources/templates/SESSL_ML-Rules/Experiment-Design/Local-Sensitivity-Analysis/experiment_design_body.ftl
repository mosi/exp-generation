<#macro experiment_design_body>
        withExperimentResult(results =>
            objective <~ results.mean(${objective})
        )
</#macro>