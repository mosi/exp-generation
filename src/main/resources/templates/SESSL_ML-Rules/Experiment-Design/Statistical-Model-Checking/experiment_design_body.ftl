<#macro experiment_design_body>
	test = SequentialProbabilityRatioTest(p = ${pValue}, alpha = 0.05, beta = 0.05, delta = 0.05)
	prop = MITL(
		${mcProperty}
	)
	
	withCheckResult { result =>
		println(result.satisfied)
	}
</#macro>