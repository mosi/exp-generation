<#macro simulation>
        simulator = <#if simulator="Hybrid"> HybridSimulator() <#else> SimpleSimulator() </#if>
        replications = ${replications}
<#if parallelThreads??>
        parallelThreads = ${parallelThreads}
</#if>
<#--Stop Condition-->
<#if stopTime??>
        stopTime = ${stopTime}
<#else>
        stopCondition = ${stopExpression}
</#if>
</#macro>