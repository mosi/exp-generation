<#macro modelInit >
<#if fileName??>
        model = "${folder}/${fileName}"
<#else>
        model = "${reference}"
</#if>
<#if parameterName??>
<#list parameterName as key>
        set("${key}" <~ ${parameterValue[key_index]})
</#list>
</#if>
</#macro>