<#import "stochastic_head.ftl" as head>
<#import "stochastic_imports.ftl" as imports>
<#import "stochastic_tail.ftl" as tail>
<#import "stochastic_model_init.ftl" as model>
<#import "stochastic_simulation.ftl" as simulation>
<#import "stochastic_observation.ftl" as observation>
<#import "Experiment-Design/Local-Sensitivity-Analysis/experiment_design_body.ftl" as saExperimentDesignBody>
<#import "Experiment-Design/Local-Sensitivity-Analysis/experiment_design_params.ftl" as saExperimentDesignParams>
<#import "Experiment-Design/Local-Sensitivity-Analysis/experiment_design_head.ftl" as saExperimentDesignHead>
<#import "Experiment-Design/Local-Sensitivity-Analysis/experiment_design_tail.ftl" as saExperimentDesignTail>
<#import "Experiment-Design/Statistical-Model-Checking/experiment_design_head.ftl" as smcExperimentDesignHead>
<#import "Experiment-Design/Statistical-Model-Checking/experiment_design_body.ftl" as smcExperimentDesignBody>

<@imports.stochastic_imports/>
<#if localSensitivityAnalysis??>
    <@saExperimentDesignHead.experiment_design_head/>
<#elseif statisticalModelChecking??>
	<@smcExperimentDesignHead.experiment_design_head/>
</#if>

<#if outputFormat == "CSV">
	<#assign v_csv = "with CSVOutput">
</#if>
<#if statisticalModelChecking??>
	<#assign v_smc = "with StatisticalModelChecking">
</#if>
<@head.header observationTrait="with Observation" expressionObservation="with ExpressionObservation" csv=v_csv smc=v_smc />


<@model.modelInit/>
<#if localSensitivityAnalysis??>
    <@saExperimentDesignParams.experiment_design_params/>
</#if>

<@simulation.simulation/>

<#if parameterScan??>
    scan(<#list factorName as factor>"${factor}" <~ range(factorMinimum[factor_index], interval[factor_index], factorMaximum[factor_index])<#if point_has_next>,</#if> </#list>)
</#if>

<@observation.observation/>

<#if localSensitivityAnalysis??>
    <@saExperimentDesignBody.experiment_design_body/>
<#elseif statisticalModelChecking??>
	<@smcExperimentDesignBody.experiment_design_body/>
</#if>

<@tail.tail/>
<#if localSensitivityAnalysis??>
    <@saExperimentDesignTail.experiment_design_tail/>
</#if>
