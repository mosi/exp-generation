<#macro observation>
<#if observationTimes??>
        observeAt(<#list observationTimes as point>${point}<#if point_has_next>,</#if> </#list>)
<#else>
        observeAt(range(${observationRangeStart}, ${observationRangeInterval}, ${observationRangeEnd}))
</#if>
<#list observationAlias as key>
        val ${key} = observe("${key}" ~ count("${observationExpression[key_index]}"))
</#list>
<#if outputFormat == "CSV" && statisticalModelChecking??>
        withExperimentResult(writeCSV)
<#elseif outputFormat == "CSV">
        withRunResult(writeCSV)
</#if>
</#macro>