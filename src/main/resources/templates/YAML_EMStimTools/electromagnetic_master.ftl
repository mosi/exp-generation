SALOMEfile:
    ${studyName}
dimension:
    ${dimensions}
<#if geometrySubDomains ??>
geometry:
    [<#list geometrySubDomains as geometry> ${geometry} <#if geometry_has_next>,</#if></#list>]
geometryvalues:
    <#list geometrySubDomains as geometry>
        ${geometry} : ${geometryValues[geometry_index]}
    </#list>
</#if>
physics:
    ${physicsModelType}

materials:
    [<#list material as material> ${material} <#if material_has_next>,</#if></#list>]

conductivity:
    <#list material as material>
        ${material} :<#if factorNames?? > <#if  factorNames?seq_contains("conductivity-"+ material)><#assign x = factorNames?seq_index_of("conductivity-"+ material)> [${levelOneValues[x]}, ${levelTwoValues[x]}]<#else> ${conductivityValues[material_index]} </#if> </#if>
    </#list>

permittivity:
    <#list material as material>
        ${material} :<#if factorNames?? > <#if factorNames?seq_contains("permittivity-"+ material)><#assign x = factorNames?seq_index_of("permittivity-"+ material)> [${levelOneValues[x]}, ${levelTwoValues[x]}]  <#else> ${permittivityValues[material_index]} </#if>  </#if>
    </#list>


boundaries:
<#if boundaryCondition == "Dirichlet">
    Dirichlet:
        <#list dirichletBoundaries as boundary>
            ${boundary} : ${dirichletValues[boundary_index]}
        </#list>
</#if>
frequencies:
    ${frequency}

solver:
<#if solver == "MUMPS"> linear_solver:mumps</#if>

element:
    ${element}
degree:
    ${degree}
output:
    <#if outputFormat == "XMDF">XMDF:yes</#if>
properties:
    <#list propertyNames as names>
        ${names} : ${propertyValues[names_index]}
    </#list>
<#if sensitivityAnalysisType ??>
dataanalysis:
        sensitivityanalysis:
            solution: [${objective}]
</#if>

