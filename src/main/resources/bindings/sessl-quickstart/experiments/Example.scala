import sessl._
import sessl.mlrules._

execute {
  new Experiment with Observation with ParallelExecution with CSVOutput {
    model = /*model*/"models/prey-predator.mlrj"
    simulator = SimpleSimulator()
    parallelThreads = -1

    stopTime = 5

    replications = 1

    scan("wolfGrowth" <~ (0.0001, 0.0002))

    observe("s" ~ count("Sheep"))
    observeAt(range(/*rangeFrom*/0, /*rangeStep*/1, /*rangeTo*/5))

    withRunResult(writeCSV)
  }
}