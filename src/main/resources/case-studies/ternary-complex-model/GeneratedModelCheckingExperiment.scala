import sessl._
import sessl.mlrules._
import sessl.verification._
import sessl.verification.mitl._

execute {
  new Experiment with Observation with StatisticalModelChecking {
    model = "TernaryComplexModel.mlrj"
    set("nR " <~ 60000)
    set("nL " <~ 1050000000)
    set("nX " <~ 24000)
    set("nCells " <~ 1)
    set("kf " <~ 5.7E-13)
    set("kr " <~ 0.017)
    set("ka " <~ 0.000000005)
    set("ku " <~ 0.00008)

    simulator = StandardSimulator()
    stopTime =  3000

    observeAt(3000)
    val observationResult = observe("Complexes" ~ count("C"))
    test = SequentialProbabilityRatioTest(p = 0.8, alpha = 0.05, beta = 0.05, delta = 0.05)

    prop = MITL(
      G(900,30000)(OutVar(observationResult) > Constant(1000))
    )

    withCheckResult { result =>
      println(result)
    }

  }
}