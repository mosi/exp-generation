import sessl._
import sessl.analysis.sensitivity._
import sessl.mlrules._

analyze((params, objective) => execute(

  new Experiment with Observation	{
    model = "TernaryComplexModel.mlrj"
    set("nR " <~ 60000)
    set("nL " <~ 1050000000)
    set("nX " <~ 24000)
    set("nCells " <~ 1)
    set("kf " <~ 5.7E-13)
    set("kr " <~ 0.017)
    set("ka " <~ 0.000000005)
    set("ku " <~ 0.00008)

    simulator = StandardSimulator()
    replications =  1

    observeAt(range(0,1,100))
    val c =observe("Complexes" ~ count("C"))

    for((name, value) <- params)
      set(name <~ value)

    withReplicationsResult(result =>
      objective <~ result.mean(c)
    )
  })) using new OneAtATimeSetup {
  baseCase(
    "ka" <~ 5e-9,
    "kr" <~ 1.7e-2,
    "kf" <~ 4.5e-13
  )
  sensitivityCase(
    "ka" <~ 5.5e-9,
    "kr" <~ 1.87e-2,
    "kf" <~ 4.95e-13
  )
  withAnalysisResult(result => {
    result.printCompleteReport()
  })
}