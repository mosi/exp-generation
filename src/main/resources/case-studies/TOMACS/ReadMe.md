# Reproducibility of the Case Studies

## 1. Software Requirements:
* On your own machine (for importing and running the main project):
   * Java 8 https://www.oracle.com/java/technologies/javase/javase8u211-later-archive-downloads.html
   * Apache Maven https://maven.apache.org/ (comes e.g. with IntelliJ IDEA)
* On your own machine (for creating the plots):
   * R-4.x https://www.r-project.org/
   * RStudio (optional)
* On your remote server (to run the Migration simulation study):
   * TORQUE (http://docs.adaptivecomputing.com/torque/3-0-5/1.1installation.php, https://www.programmersought.com/article/41867156748/)
   * R-4.x.x https://www.r-project.org/
   * Julia 1.7.x
   * Also follow the instructions at https://github.com/mhinsch/rgct_data and https://github.com/jasonhilton/screen_run

## 2. WNT Case Study

* Run the Main.java with commandline arguments `-c WNT`.

* Check the console output to see which provenance activities are created.

* After running the case study, the raw data are available in the  folder 
*dataGeneratedByExperiments* and the experiment script and json can be viewed in the folder *experiments*.
The plot can be generated as follows.

* Run the script *analysis/analysis.R* (e.g., in RStudio).
This first runs the scaled configuration for comparison (mimicking the manual activity of the modeler).
Then it plots the mean trajectories for scaling factor 1 and scaling factor 0.28 as well as the trajectory produced by 
the Lee model.

* The plot of this cross-validation is saved in the directory *analysis/Plots*.


## 3. MIGRATION Case Study

* Run the Main.java with commandline arguments `-c MIGRATION` and wait for it to fishish.

* Navigate to *screen-run*, which was loaded by the initial setup script, see parent directory
of this project. There, the simulation data have been stored in *results* in 
a timestamped folder and the experiment design in *designs/lhs* in a timestamped file.

* In the scripts folder you find R scripts for processing simulation output files and generating the necessary variables for analysis.
Each script takes only one command line argument - the timestamped folder where the simulation results we wish to analyse are saved.

* The *process_data.R* script (e.g., executed using `Rscript.exe scripts/process_data.R 20220309_152928` on Windows and
  `Rscript scripts/process_data.R 20220309_152928` on Linux) aggregates and simplifies simulation output files.
Results are saved in a timestamped directory within the *results/summary* folder.

* The *construct_output.R* script does further manipulation to produce the small set of summary outputs needed for further analysis. 
Again results are saved in the *results/summary* folder.

* *The *sensitivity.r* script conducts a sensitivity analysis to indicate the contribution of each input to the total simulation variance. 
Outputs are saved as PDF plot in the *SA_results* folder.

* The generated plot represents the sensitivity analysis with simulation model M4. Note that the experiment for M5 is not executed
as the model is not yet publicly available.

* To get the sensitivities of simulation model M3 for comparison, the experiment needs to be repeated manually with that model.
Simply navigate to the *screen_run* folder on your remote system and run `./send_jobs.sh m3`. This mimics the activity
a2.

* Wait for the jobs to finish. Then apply the analysis scripts to the result folder as before.
