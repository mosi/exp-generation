import sessl._
import sessl.mlrules._
import sessl.analysis.sensitivity._

analyze((params, objective) =>
execute {
new Experiment with Observation {



<#if modelPath??>
    model = "${modelPath}"
</#if>

<#if stopTime??>
    stopTime = ${stopTime}
</#if>


val numA = observe(count("Cell/Nuc/Bcat"))


observeAt(${observeAt})

for ((name, value) <- params)
set(name <~ value)


withReplicationsResult {
result => objective <~ result.mean(numA)
    }
}

}) using new TwoLevelFullFactorialSetup {

baseCase(
<#list baseCaseParameter as key>
    "${key}"<~ ${baseCaseParameterValues[key_index]?c}
</#list>
)


sensitivityCase(
<#list sensitivityParameter as key>
    "${key}" <~ ${sensitivityParameterValues[key_index]}<#if key_has_next>,</#if>
</#list>
)

withAnalysisResult(result => {
<#list valueParameter as key>
    val ${key} = result.individualEffect("${valueParameterValue[key_index]})
</#list>

<#list individualParameter as key>
    println(s"Individual(${key}) = ${individualParameterValue[key_index]}")
</#list>


})

}

