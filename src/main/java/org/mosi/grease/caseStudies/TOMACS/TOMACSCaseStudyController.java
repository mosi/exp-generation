package org.mosi.grease.caseStudies.TOMACS;

import org.mosi.grease.caseStudies.AbstractCaseStudyController;
import org.mosi.grease.caseStudies.CaseStudy;
import org.mosi.grease.experimentgeneration.controller.MainExperimentController;
import org.mosi.grease.experimentgeneration.model.ExperimentDesign;
import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.controller.activityGenerators.AbstractGenerator;
import org.mosi.grease.provenancefiltering.controller.triggers.AbstractTrigger;
import org.mosi.grease.provenancefiltering.controller.triggers.TriggerController;
import org.mosi.grease.provenancefiltering.model.ProvenanceNode;
import org.mosi.grease.provenancefiltering.model.entities.Data;
import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;

import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.stream.Collectors;

public class TOMACSCaseStudyController extends AbstractCaseStudyController implements Observer {
    MainExperimentController mainExperimentController;
    MainController mainController;
    GraphGeneratorHaack graphGeneratorHaack;
    GraphGeneratorAbstract graphGeneratorAbstract;
    GraphGeneratorMigration graphGeneratorMigration; //For this to work setModelUpdated must be false and BSM must trigger Analysis

    CaseStudy caseStudy;
    private static int currentCaseStudyPart = 1;

    public TOMACSCaseStudyController(MainExperimentController mainExperimentController, String caseStudy) {
        this.mainExperimentController = mainExperimentController;
        this.caseStudy = CaseStudy.valueOf(caseStudy);

        //initialize
        mainController = new MainController(mainExperimentController);
        mainController.setAbstractCaseStudyController(this);
    }

    public void runCaseStudy() {
        //edit experiment manually after it is possibly modified (false by default):
        AbstractGenerator.setManuallyEditExperiment(false);

        //turning on/off triggers and generators (These are false (= off) by default)

        //set up the case study graphs and rule set
        if(caseStudy == CaseStudy.WNT){
            System.out.println("Initializing provenance graph for case study Haack et al.");
            graphGeneratorHaack = new GraphGeneratorHaack();

            //Prepare the Lee graph without testing for triggers
            TriggerController.setTriggerAutomatically(false);
            graphGeneratorHaack.prepareCaseStudyGraphWithLeeStudy(mainController.getModelController());

            //Start Haack study with automatic support for 1 rule
            TriggerController.setTriggerAutomatically(true);
            TriggerController.setRuleEnabled("CalibratingTriggersCrossvalidation" ,true);
            graphGeneratorHaack.generateCaseStudyGraph(mainController.getModelController());
            
        } else if(caseStudy == CaseStudy.MIGRATION){
            //Automatic support during the study with 1 rule enabled
            TriggerController.setTriggerAutomatically(true);
            TriggerController.setBuildingSimulationModelTriggersAnalyzingSimulationModel(true);

            graphGeneratorMigration = new GraphGeneratorMigration();
            graphGeneratorMigration.generateCaseStudyGraphPart1(mainController.getModelController());

        } else if(caseStudy == CaseStudy.ABSTRACT){

            //Automatic support during entire simulation study with many triggers/rules enabled
            TriggerController.setTriggerAutomatically(true);
            TriggerController.setBuildingSimulationTriggersCalibratingSimulation(true);
            TriggerController.setComposingSimulationTriggersValidatingSimulation(true);
            TriggerController.setCalibratingSimulationModelTriggersValidatingSimulationModel(false);
            TriggerController.setBuildingSimulationModelTriggersAnalyzingSimulationModel(false);

            graphGeneratorAbstract = new GraphGeneratorAbstract();
            graphGeneratorAbstract.generateCaseStudyGraph_part1(mainController.getModelController());

        }

        //To manually start the trigger uncomment the following:
        //mainController.getTriggerController().triggerCalibratingSimulationModelGeneration();
        //mainController.getTriggerController().triggerValidatingSimulationModelGeneration();
    }

//    public void printCalibrationActivities(){
//        //Query for 'CALIBRATION_SIMULATION_MODEL' activities within provenance graph; The new activity should be there, now
//        Result result = mainController.getDatabaseController().getGraphDb().execute("MATCH (neighborNode)--(calibrationActivity:ACTIVITY) WHERE calibrationActivity.activityType = 'CALIBRATING_SIMULATION_ACTIVITY' return calibrationActivity,neighborNode.id");
//        System.out.println(result.resultAsString());
//    }

//    public void printValidationActivities(){
//        //Query for 'VALIDATION_SIMULATION_MODEL' activities within provenance graph; The new activity should be there, now
//        Result result = mainController.getDatabaseController().getGraphDb().execute("MATCH (neighborNode)--(validationActivity:ACTIVITY) WHERE validationActivity.activityType = 'VALIDATION_SIMULATION_MODEL' return validationActivity,neighborNode.id");
//        System.out.println();
//        System.out.println("Validation Activities:");
//        System.out.println(result.resultAsString());
//        System.out.println();
//    }

    public void findExperimentTypeOrDomain(){
        //Demonstration to find experiment domain from experiment json:
        String pathToDesExample = "case-studies/experimentGenerationGUI/exampleJsons/des-example.json";
        String pathToFemExample = "case-studies/experimentGenerationGUI/exampleJsons/fem-example.json";
        String pathToVpExample = "case-studies/experimentGenerationGUI/exampleJsons/vp-example.json";

        //Demonstration to find experiment type from experiment json:
        String pathToGsaExample = "case-studies/experimentGenerationGUI/exampleJsons/gsa-example.json";

        org.json.JSONObject whatKindOfExperimentIsThisCaseStudy = new GraphGeneratorHaack().loadCaseStudyJsonObjectFromPath(pathToGsaExample);
        ExperimentDesign resultingDomain = mainController.getExperimentJsonController().inferExperimentTypeFromExperimentJason(whatKindOfExperimentIsThisCaseStudy);
        System.out.println(resultingDomain.toString());
    }

    /*
    Continue ("manually") with the simulation study after an experiment was generated
     */
    @Override
    public void update(Observable o, Object arg) {
        currentCaseStudyPart++;

        if(caseStudy == CaseStudy.WNT){
            if(currentCaseStudyPart == 2){
                //printValidationActivities();
                System.out.println("------------------Experiment Generation terminated----------------");
            }
            else {
                mainController.getDatabaseController().stopDataBase();
            }
        }
        else if(caseStudy == CaseStudy.ABSTRACT){
            if(currentCaseStudyPart == 2){
                //get the generated simulation data and continue
                if (o instanceof AbstractTrigger) {
                    List<ProvenanceNode> generatedEntities = (List<ProvenanceNode>) arg;
                    //get SM4
                    SimulationModel sm4 = (SimulationModel) generatedEntities.stream()
                            .filter(node -> node.getName().equals("SM4"))
                            .collect(Collectors.toList()).get(0);
                    graphGeneratorAbstract.generateCaseStudyGraph_part2(mainController.getModelController(), sm4);
                }
            } else if (currentCaseStudyPart == 3){
                System.out.println("------------------Experiment Generation terminated----------------");
            }
            else {
                mainController.getDatabaseController().stopDataBase();
            }
        }
        else if(caseStudy == CaseStudy.MIGRATION){
            if(currentCaseStudyPart == 2) {
                graphGeneratorMigration.generateCaseStudyGraphPart2(mainController.getModelController());
            }
            else if(currentCaseStudyPart == 3) {
                //get the generated simulation data and continue
                if (o instanceof AbstractTrigger) {
                    List<ProvenanceNode> generatedEntities = (List<ProvenanceNode>) arg;
                    //get S3 or create dummy S3 to continue
                    Data s3 = (Data) generatedEntities.stream()
                                .filter(node -> node.getName().equals("S3"))
                                .collect(Collectors.toList()).get(0);
                    graphGeneratorMigration.generateCaseStudyGraphPart3(mainController.getModelController(), s3);
                }
            }
            else {
                mainController.getDatabaseController().stopDataBase();
            }
        }

    }
}
