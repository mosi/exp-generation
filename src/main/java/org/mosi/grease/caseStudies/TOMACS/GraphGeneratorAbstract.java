package org.mosi.grease.caseStudies.TOMACS;

import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.controller.ProvModelController;
import org.mosi.grease.provenancefiltering.model.Distribution;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.ProvenanceCommit;
import org.mosi.grease.provenancefiltering.model.ProvenanceNode;
import org.mosi.grease.provenancefiltering.model.entities.*;
import org.mosi.grease.provenancefiltering.model.entities.experiments.Experiment;
import org.mosi.grease.provenancefiltering.model.entities.experiments.ExperimentParameter;
import org.mosi.grease.provenancefiltering.model.enums.*;

public class GraphGeneratorAbstract {

    public void generateCaseStudyGraph_part1(ProvModelController provModelController) {

        System.out.println("Starting with substudy 1");

        //1. Model building:
        //Start activity m1
        ProvenanceCommit commit_bsm1 = new ProvenanceCommit();

        //Creating Research Question
        ResearchQuestion rq1 = new ResearchQuestion("Substudy 1");
        rq1.setDescription("What is the predator-prey dynamics with wolves and sheep?");
        commit_bsm1.addProvenanceNode(rq1);

        //Creating Qualitative Model
        QualitativeModel qm1 = new QualitativeModel("Substudy 1");
        ExperimentParameter parameterk1 = new ExperimentParameter("sheepGrowth", 1.0, "rate unit");
        ExperimentParameter parameterk2 = new ExperimentParameter("wolfGrowth", 0.0001, "rate unit");
        ExperimentParameter parameterk3 = new ExperimentParameter("wolfDeath", 1.0, "rate unit");
        qm1.addParameter(parameterk1);
        qm1.addParameter(parameterk2);
        qm1.addParameter(parameterk3);
        commit_bsm1.addProvenanceNode(qm1);

        //Creating Assumption
        Assumption a1 = new Assumption("Substudy 1");
        a1.setDescription("Size of the ecosystem: 1000 wolves and 1000 sheep");
        commit_bsm1.addProvenanceNode(a1);

        //Creating activity
        ProvenanceActivity bsm1 = new ProvenanceActivity("BSM1", "Substudy 1", ActivityType.CREATING_SIMULATION_MODEL);
        commit_bsm1.addProvenanceNode(bsm1);

        //Creating Simulation Model
        SimulationModel sm1 = new SimulationModel("SM1", "Substudy 1", "ML-Rules",
                "src/main/resources/case-studies/TOMACS/models/prey-predator.mlrj", "", false);
        commit_bsm1.addProvenanceNode(sm1);

        //ingoing relationships
        commit_bsm1.addProvenanceRelationship(bsm1, rq1);
        commit_bsm1.addProvenanceRelationship(bsm1, qm1);
        commit_bsm1.addProvenanceRelationship(bsm1, a1);
        //outgoing relationships
        commit_bsm1.addProvenanceRelationship(sm1, bsm1);

        //Commit activity
        provModelController.commitActivity(commit_bsm1);


        //2. Model calibrating:
        ProvenanceCommit commit_csm1 = new ProvenanceCommit();

        //Creating Data
        Data d1 = new Data("D1", "Substudy 1", DataType.UNSPECIFIED, Status.UNDEFINED);
        d1.setDescription("Wolf and sheep numbers observed near the town XYZ");
        commit_csm1.addProvenanceNode(d1);

        //Creating Activity
        ProvenanceActivity csm1 = new ProvenanceActivity("CSM1", "Substudy 1", ActivityType.CALIBRATING_SIMULATION_MODEL);
        commit_csm1.addProvenanceNode(csm1);

        //Creating Simulation Data
        Data sd1 = new Data("Substudy 1", DataType.IN_SILICO, "SE1", Status.UNDEFINED);
        commit_csm1.addProvenanceNode(sd1);

        //Creating Simulation Experiment
        Experiment se1 = new Experiment("SE1", "Substudy 1", ExperimentRole.CALIBRATION);
        se1.setSuccessful(false);
        se1.setReference("src/main/resources/case-studies/TOMACS/experiments/Example.scala");
        commit_csm1.addProvenanceNode(se1);

        //Creating Simulation Model
        SimulationModel sm2 = new SimulationModel("SM2", "Substudy 1", "ML-Rules",
                "src/main/resources/case-studies/TOMACS/models/prey-predator.mlrj", "", false);
        commit_csm1.addProvenanceNode(sm2);

        //ingoing relationships
        commit_csm1.addProvenanceRelationship(csm1, sm1);
        commit_csm1.addProvenanceRelationship(csm1, d1);

        commit_csm1.addProvenanceRelationship(se1, csm1);
        commit_csm1.addProvenanceRelationship(sd1, csm1);
        commit_csm1.addProvenanceRelationship(sm2, csm1);

        //Send commit
        provModelController.commitActivity(commit_csm1);


        //3. Model Refinement:
        ProvenanceCommit commit_bsm2 = new ProvenanceCommit();

        //Creating Data
        Data d2 = new Data("D2","Substudy 1", DataType.UNSPECIFIED, Status.UNDEFINED);
        commit_bsm2.addProvenanceNode(d2);

        //Creating Qualitative Model
        QualitativeModel qm2 = new QualitativeModel("Substudy 1");
        ExperimentParameter k2 = new ExperimentParameter("wolfGrowth", 0.0001, "rate unit", 0.0001, 0.00025);
        qm2.addParameter(k2);
        commit_bsm2.addProvenanceNode(qm2);

        //Creating Activity
        ProvenanceActivity bsm2 = new ProvenanceActivity("BSM2", "Substudy 1", ActivityType.REFINING_SIMULATION_MODEL);
        commit_bsm2.addProvenanceNode(bsm2);

        //Creating Simulation Model
        SimulationModel sm3 = new SimulationModel("SM3", "Substudy 1", "ML-Rules",
                "src/main/resources/case-studies/TOMACS/models/prey-predator.mlrj", "", false);
        commit_bsm2.addProvenanceNode(sm3);

        //ingoing relationships
        commit_bsm2.addProvenanceRelationship(bsm2, sm2);
        commit_bsm2.addProvenanceRelationship(bsm2, d2);
        commit_bsm2.addProvenanceRelationship(bsm2, qm2);
        //outgoing relationships
        commit_bsm2.addProvenanceRelationship(sm3, bsm2);

        //send to database
        provModelController.commitActivity(commit_bsm2);

        //Triggers reuse of calibration

    }

    public void generateCaseStudyGraph_part2(ProvModelController provModelController, SimulationModel sm4) {

        System.out.println("------------------------------------------------------");

        //5. Validation:
        ProvenanceCommit commit_vsm1 = new ProvenanceCommit();

        //Creating Requirement
        Requirement r1 = new Requirement("Substudy 1");
        commit_vsm1.addProvenanceNode(r1);

        //Creating Simulation Experiment
        Experiment se3 = new Experiment("SE3","Substudy 1", ExperimentRole.VALIDATION);
        se3.setExperimentType(ExperimentType.PARAMETERSCAN);
        se3.setReference("src/main/resources/case-studies/TOMACS/experiments/Example.scala");
        se3.setSuccessful(true);
        commit_vsm1.addProvenanceNode(se3);

        //Creating Simulation data
        Data sd3 = new Data("Substudy 1", DataType.IN_SILICO, "SE3", Status.SUCCESS);
        commit_vsm1.addProvenanceNode(sd3);

        //Creating activity
        ProvenanceActivity vsm1 = new ProvenanceActivity("VSM1", "Substudy 1", ActivityType.VALIDATING_SIMULATION_MODEL);
        commit_vsm1.addProvenanceNode(vsm1);

        //ingoing relationships
        commit_vsm1.addProvenanceRelationship(vsm1, r1);
        commit_vsm1.addProvenanceRelationship(vsm1, sm4);
        //outgoing relationships
        commit_vsm1.addProvenanceRelationship(se3, vsm1);
        commit_vsm1.addProvenanceRelationship(sd3, vsm1);

        //send to database
        provModelController.commitActivity(commit_vsm1);

        //////////////////////////////////////////////////////////////////
        //Starting with next substudy, counters start with 1 again
        System.out.println("------------------------------------------------------");
        System.out.println("Starting with substudy 2");

        //6. Creating Simulation Model:
        ProvenanceCommit commit_bsm1 = new ProvenanceCommit();

        //Creating Research Question
        ResearchQuestion rq1 = new ResearchQuestion("RQ1","Substudy 2");
        rq1.setDescription("how environmental conditions affect the sheep and wolf growth");
        commit_bsm1.addProvenanceNode(rq1);

        //Creating Theory
        Theory t1 = new Theory("T1", "Substudy 2");
        t1.setDescription("Food abundance varies");

        //Creating Activity
        ProvenanceActivity bsm1 = new ProvenanceActivity("BSM1", "Substudy 2", ActivityType.CREATING_SIMULATION_MODEL);
        commit_bsm1.addProvenanceNode(bsm1);

        //Creating Simulation Model
        SimulationModel sm1_2 = new SimulationModel("SM1", "Substudy 2", "ML-Rules",
                "src/main/resources/case-studies/TOMACS/models/prey-predator.mlrj",
                "", false);
        sm1_2.setDescription("model of sheep food and temperature");
        commit_bsm1.addProvenanceNode(sm1_2);

        //ingoing relationships
        commit_bsm1.addProvenanceRelationship(bsm1, rq1);
        commit_bsm1.addProvenanceRelationship(bsm1, t1);
        //outgoing relationships
        commit_bsm1.addProvenanceRelationship(sm1_2, bsm1);

        provModelController.commitActivity(commit_bsm1);

        //////////////////////////////////////////////////////////////////
        //7. Validating the 2nd model
        ProvenanceCommit commit_vsm1_2 = new ProvenanceCommit();

        //Creating Requirement
        Requirement r1_2 = new Requirement("R1", "Substudy 2");
        commit_vsm1_2.addProvenanceNode(r1_2);

        //Creating Simulation Experiment
        Experiment se1_2 = new Experiment("SE1","Substudy 2", ExperimentRole.VALIDATION);
        se1_2.setExperimentType(ExperimentType.PARAMETERSCAN);
        se1_2.setReference("src/main/resources/case-studies/TOMACS/experiments/Example.scala");
        se1_2.setSuccessful(true);
        commit_vsm1_2.addProvenanceNode(se1_2);

        //Creating Simulation data
        Data sd1_2 = new Data("Substudy 2", DataType.IN_SILICO, "SE1", Status.SUCCESS);
        commit_vsm1_2.addProvenanceNode(sd1_2);

        //Creating activity
        ProvenanceActivity vsm1_2 = new ProvenanceActivity("VSM1", "Substudy 2", ActivityType.VALIDATING_SIMULATION_MODEL);
        commit_vsm1_2.addProvenanceNode(vsm1_2);

        //ingoing relationships
        commit_vsm1_2.addProvenanceRelationship(vsm1_2, r1_2);
        commit_vsm1_2.addProvenanceRelationship(vsm1_2, sm1_2);
        //outgoing relationships
        commit_vsm1_2.addProvenanceRelationship(se1_2, vsm1_2);
        commit_vsm1_2.addProvenanceRelationship(sd1_2, vsm1_2);

        //send to database
        provModelController.commitActivity(commit_vsm1_2);

        //////////////////////////////////////////////////////////////////
        System.out.println("------------------------------------------------------");
        System.out.println("Starting with substudy 3");

        //13. Model composition
        ProvenanceCommit commit_comp = new ProvenanceCommit();

        //Creating Simulation Model
        SimulationModel sm_comp = new SimulationModel("SM1", "Substudy 3", "ML-Rules",
                "src/main/resources/case-studies/TOMACS/models/prey-predator.mlrj", "", false);
        sm_comp.setDescription("Composing the predator-prey model with the environment model");
        commit_comp.addProvenanceNode(sm_comp);

        //Creating Activity
        ProvenanceActivity mc1 = new ProvenanceActivity("BSM1", "Substudy 3", ActivityType.COMPOSING_SIMULATION_MODEL);
        commit_comp.addProvenanceNode(mc1);

        //ingoing relationships
        commit_comp.addProvenanceRelationship(mc1, sm4);
        commit_comp.addProvenanceRelationship(mc1, sm1_2);

        //outgoing relationships
        commit_comp.addProvenanceRelationship(sm_comp, mc1);

        //Send to database
        provModelController.commitActivity(commit_comp);

        //Triggers the repetition of validation experiments
    }

}
