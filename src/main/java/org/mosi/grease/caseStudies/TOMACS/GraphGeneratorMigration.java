package org.mosi.grease.caseStudies.TOMACS;

import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.controller.ProvModelController;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.ProvenanceCommit;
import org.mosi.grease.provenancefiltering.model.entities.*;
import org.mosi.grease.provenancefiltering.model.entities.experiments.Experiment;
import org.mosi.grease.provenancefiltering.model.enums.*;

import java.util.ArrayList;

public class GraphGeneratorMigration {

    //Needed for case study part 2, 3
    SimulationModel M2;
    SimulationModel M4;

    public void generateCaseStudyGraphPart1(ProvModelController provModelController) {

        //Start activity m1
        ProvenanceCommit commit_m1 = new ProvenanceCommit();

        //Creating research question RQ1
        ResearchQuestion rq1 = new ResearchQuestion("Bijak et al. 2020");
        rq1.setDescription("Does information exchange between migrants play a role in the formation of migration routes?");
        commit_m1.addProvenanceNode(rq1);

        //Creating data NSR and SR
        Data nsr = new Data("nsr", "Massey et al. 1993; Castles 2004; Alam et al. 2012; Klabunde and " +
                "Willekens 2016; Wall et al. 2017", DataType.UNSPECIFIED, Status.UNDEFINED);
        nsr.setDescription("Non-scientific reports about migration route formation");
        commit_m1.addProvenanceNode(nsr);
        Data sr = new Data("sr", "Kingsley 2016; Emmer et al. 2016", DataType.UNSPECIFIED,
                Status.UNDEFINED);
        sr.setDescription("Scientific reports about migration route formation");
        commit_m1.addProvenanceNode(sr);

        //Creating activity m1
        ProvenanceActivity m1 = new ProvenanceActivity("m1", "Bijak et al. 2020",
                ActivityType.CREATING_SIMULATION_MODEL);
        m1.setDescription("Initial model version (grid-based, discrete time)");
        commit_m1.addProvenanceNode(m1);

        //Creating the resulting simulation model M1
        SimulationModel M1 = new SimulationModel("M1", "Bijak et al. 2020", "Julia", "", "", false);
        M1.setDescription("Initial model version (grid-based, discrete time)");
        commit_m1.addProvenanceNode(M1);

        //Creating relationships for building simulation model activity m1
        //ingoing
        commit_m1.addProvenanceRelationship(m1, sr);
        commit_m1.addProvenanceRelationship(m1, nsr);
        commit_m1.addProvenanceRelationship(m1, rq1);
        //outgoing
        commit_m1.addProvenanceRelationship(M1, m1);
        //End Commit
        provModelController.commitActivity(commit_m1);


        //Start activity m2
        ProvenanceCommit commit_m2 = new ProvenanceCommit();

        //Creating building simulation model activity
        ProvenanceActivity m2 = new ProvenanceActivity("m2", "Bijak et al. 2020",
                ActivityType.REFINING_SIMULATION_MODEL);
        m2.setDescription("Creating the second model version");
        commit_m2.addProvenanceNode(m2);

        //Creating simulation model M2
        M2 = new SimulationModel("M2", "Bijak et al. 2020", "Julia",
                "", "", false);
        M2.setDescription("Second model version (graph-based, discrete time)");
        commit_m2.addProvenanceNode(M2);

        //Creating relationships for building simulation model activity m2
        //ingoing
        commit_m2.addProvenanceRelationship(m2, M1);
        commit_m2.addProvenanceRelationship(m2, rq1);
        //outgoing
        commit_m2.addProvenanceRelationship(M2, m2);
        //End Commit
        provModelController.commitActivity(commit_m2);

        //Triggers search for experiments
    }


    public void generateCaseStudyGraphPart2(ProvModelController provModelController) {
        //Start activity m2_1
        ProvenanceCommit commit_m2_1 = new ProvenanceCommit();

        //Create building simulation model activity
        ProvenanceActivity m2_1 = new ProvenanceActivity("m2_1", "Reinhardt et al. 2019",
                ActivityType.REIMPLEMENTING_SIMULATION_MODEL);
        m2_1.setDescription("Re-implementing M2 in ML3");
        commit_m2_1.addProvenanceNode(m2_1);

        //Creating simulation model M2_1
        SimulationModel M2_1 = new SimulationModel("M2_1", "Reinhardt et al. 2019", "ML3", "", "", false);
        M2_1.setDescription("Reimplementation of M2 in ML3");
        commit_m2_1.addProvenanceNode(M2_1);

        //Creating relationships for building simulation model activity m2_1
        //ingoing
        commit_m2_1.addProvenanceRelationship(m2_1, M2);
        //outgoing
        commit_m2_1.addProvenanceRelationship(M2_1, m2_1);
        //End commit
        provModelController.commitActivity(commit_m2_1);


        //Start activity m3
        ProvenanceCommit commit_m3 = new ProvenanceCommit();

        //Creating building simulation model activity m3
        ProvenanceActivity m3 = new ProvenanceActivity("m3", "Reinhardt et al. 2019", ActivityType.COMPOSING_SIMULATION_MODEL);
        m3.setDescription("Bringing M2 and M2’ into alignment");
        commit_m3.addProvenanceNode(m3);

        //Creating simulation model M3
        SimulationModel M3 = new SimulationModel("M3", "Bijak et al. 2020", "Julia",
                "../rgct_data/run.jl", "", false);
        M3.setDescription("Routes and Rumours (graph-based, discrete event)");
        commit_m3.addProvenanceNode(M3);

        //Creating simulation model M3_1
        SimulationModel M3_1 = new SimulationModel("M3_1", "Reinhardt et al. 2019", "ML3", "", "", false);
        M3_1.setDescription("ML3 Version of Routes and Rumours");
        commit_m3.addProvenanceNode(M3_1);

        //Creating relationships for building simulation model activity m3
        //ingoing
        commit_m3.addProvenanceRelationship(m3, M2_1);
        commit_m3.addProvenanceRelationship(m3, M2);
        //outgoing
        commit_m3.addProvenanceRelationship(M3_1, m3);
        commit_m3.addProvenanceRelationship(M3, m3);
        //End commit
        provModelController.commitActivity(commit_m3);


        //Start activity a1
        ProvenanceCommit commit_a1 = new ProvenanceCommit();

        //Creating methodology entity K01
        Other k01 = new Other("K01", "Kennedy and O’Hagan 2001");
        k01.setDescription("Methodology of Kennedy and O’Hagan");
        commit_a1.addProvenanceNode(k01);

        //Creating analyzing simulation model activity a1
        ProvenanceActivity a1 = new ProvenanceActivity("a1", "Bijak et al. 2020", ActivityType.ANALYZING_SIMULATION_MODEL);
        a1.setDescription("Preliminary screening of the Routes and Rumours model on all 17 Parameters");
        commit_a1.addProvenanceNode(a1);

        //Create resulting simulation experiment E1 and resulting simulation data S1
        Experiment e1 = new Experiment("E1","Bijak et al. 2020", ExperimentRole.ANALYSIS);
        e1.setIsRemote(true);
        commit_a1.addProvenanceNode(e1);
        Data s1 = new Data("S1", "Bijak et al. 2020", DataType.IN_SILICO, Status.UNDEFINED);
        commit_a1.addProvenanceNode(s1);

        //Creating relationships for analyzing simulation model activity a1
        //ingoing
        commit_a1.addProvenanceRelationship(a1, M3);
        commit_a1.addProvenanceRelationship(a1, k01);
        //outgoing
        commit_a1.addProvenanceRelationship(e1, a1);
        commit_a1.addProvenanceRelationship(s1, a1);
        //End commit
        provModelController.commitActivity(commit_a1);


        //Start activity a2
        ProvenanceCommit commit_a2 = new ProvenanceCommit();

        //Creating analyzing simulation model activity a2
        ProvenanceActivity a2 = new ProvenanceActivity("a2", "Bijak et al. 2020", ActivityType.ANALYZING_SIMULATION_MODEL);
        a2.setDescription("Uncertainty and sensitivity analysis of the Routes and Rumours model");
        commit_a2.addProvenanceNode(a2);

        //Create resulting simulation experiment E2 and resulting simulation data S2
        Experiment e2 = new Experiment("E2","Bijak et al. 2020", ExperimentRole.ANALYSIS);
        e2.setExperimentType(ExperimentType.SENSITIVITYANALYSIS);
        e2.setReference("../screen_run/send_jobs.sh");
        e2.setModelName("../rgct_data");
        e2.setIsRemote(true);
        commit_a2.addProvenanceNode(e2);
        Data s2 = new Data("S2", "Bijak et al. 2020", DataType.IN_SILICO, Status.UNDEFINED);
        s2.setDescription("Sensitivity information about the Routes and Rumours model");
        commit_a2.addProvenanceNode(s2);

        //Creating relationships for analyzing simulation model activity a2
        //ingoing
        commit_a2.addProvenanceRelationship(a2, M3);
        commit_a2.addProvenanceRelationship(a2, s1);
        commit_a2.addProvenanceRelationship(a2, k01);
        //outgoing
        commit_a2.addProvenanceRelationship(e2, a2);
        commit_a2.addProvenanceRelationship(s2, a2);
        //End commit
        provModelController.commitActivity(commit_a2);


        //Start activity g1
        ProvenanceCommit commit_g1 = new ProvenanceCommit();

        //Creating activity g1
        ProvenanceActivity g1 = new ProvenanceActivity("g1", "Bijak et al. 2020", ActivityType.CONCEPTUAL_MODELING);
        g1.setDescription("Identifying a knowledge gap in M3");
        commit_g1.addProvenanceNode(g1);

        //Creating research question RQ2
        ResearchQuestion rq2 = new ResearchQuestion("RQ2", "Bijak et al. 2020");
        rq2.setDescription("How do risk perception and risk avoidance affect the formation of migration routes?");
        commit_g1.addProvenanceNode(rq2);

        //Creating relationships for activity g1
        //ingoing
        commit_g1.addProvenanceRelationship(g1, s2);
        //outgoing
        commit_g1.addProvenanceRelationship(rq2, g1);
        //End commit
        provModelController.commitActivity(commit_g1);


        //Start activity m4
        ProvenanceCommit commit_m4 = new ProvenanceCommit();

        //Creating building simulation model activity m4
        ProvenanceActivity m4 = new ProvenanceActivity("m4", "Bijak et al. 2020", ActivityType.REFINING_SIMULATION_MODEL);
        m4.setDescription("Extending the Routes and Rumours model, including risk");
        commit_m4.addProvenanceNode(m4);

        //Creating simulation model M4
        M4 = new SimulationModel("M4", "Bijak et al. 2020", "Julia",
                "../rgct_data/run.jl", "", false);
        M4.setDescription("Risks and Rumours");
        commit_m4.addProvenanceNode(M4);

        //Creating relationships for building simulation model activity m4
        //ingoing
        commit_m4.addProvenanceRelationship(m4, rq2);
        commit_m4.addProvenanceRelationship(m4, M3);
        //outgoing
        commit_m4.addProvenanceRelationship(M4, m4);
        //End commit
        provModelController.commitActivity(commit_m4);

        //Triggers repeat analyzing simulation model

    }

    public void generateCaseStudyGraphPart3(ProvModelController provModelController, Data s3) {
        System.out.println("--------------------Continuing with Study---------------------");

        //uses generated data s3

        //Start activity g2
        ProvenanceCommit commit_g2 = new ProvenanceCommit();

        //Creating conceptual modeling activity g2
        ProvenanceActivity g2 = new ProvenanceActivity("g2", "Bijak et al. 2020", ActivityType.CONCEPTUAL_MODELING);
        g2.setDescription("Identifying a knowledge gap in M4");
        commit_g2.addProvenanceNode(g2);

        //Creating research question RQ3
        ResearchQuestion rq3 = new ResearchQuestion("rq3", "Bijak et al. 2020");
        rq3.setDescription("In a realistic scenario, can more information lead to fewer fatalities?");
        commit_g2.addProvenanceNode(rq3);

        //Creating relationships for activity g2
        //ingoing
        commit_g2.addProvenanceRelationship(g2, s3);
        //outgoing
        commit_g2.addProvenanceRelationship(rq3, g2);
        //End commit
        provModelController.commitActivity(commit_g2);


        //Start model building activity m5
        ProvenanceCommit commit_m5 = new ProvenanceCommit();

        //Creating various input data and assumptions from other part of the study
        Data osm = new Data("OSM","Alam, S. J., Geller, A. & Tsvetovat, M. (2012)", DataType.UNSPECIFIED,
                Status.UNDEFINED);
        osm.setDescription("OpenStreetMap city locations via OpenRouteService");
        commit_m5.addProvenanceNode(osm);
        Assumption id = new Assumption("ID","Alam, S. J., Geller, A. & Tsvetovat, M. (2012)");
        id.setDescription("Probability distributions representing bias and variance of data on interceptions by Libyan " +
                "and Tunisian coastguards and deaths in the Central Mediterranean");
        commit_m5.addProvenanceNode(id);
        Assumption ar = new Assumption("AR","Alam, S. J., Geller, A. & Tsvetovat, M. (2012)");
        ar.setDescription("Probability distribution representing bias and variance of data on sea arrivals in Italy");
        commit_m5.addProvenanceNode(ar);

        //Creating building simulation model activity m5
        ProvenanceActivity m5 = new ProvenanceActivity("m5", "Bijak et al. 2020", ActivityType.REFINING_SIMULATION_MODEL);
        m5.setDescription("Adding geography of and data about the Mediterranean to the Risks and Rumours model");
        commit_m5.addProvenanceNode(m5);

        //Creating simulation model M5
        SimulationModel M5 = new SimulationModel("M5", "Bijak et al. 2020", "Julia",
                "../rgct_data/run.jl", "", false);
        M5.setDescription("Risks and Rumours with Reality");
        commit_m5.addProvenanceNode(M5);

        //Creating relationships for building simulation model activity m5
        //ingoing
        commit_m5.addProvenanceRelationship(m5, M4);
        commit_m5.addProvenanceRelationship(m5, osm);
        commit_m5.addProvenanceRelationship(m5, id);
        commit_m5.addProvenanceRelationship(m5, ar);
        commit_m5.addProvenanceRelationship(m5, rq3);
        //outgoing
        commit_m5.addProvenanceRelationship(M5, m5);
        //End commit
        provModelController.commitActivity(commit_m5);

        //Triggers repeat analyzing simulation model

    }

}
