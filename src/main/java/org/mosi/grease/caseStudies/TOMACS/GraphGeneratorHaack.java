package org.mosi.grease.caseStudies.TOMACS;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;
import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.controller.ProvModelController;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.ProvenanceCommit;
import org.mosi.grease.provenancefiltering.model.entities.*;
import org.mosi.grease.provenancefiltering.model.entities.experiments.Experiment;
import org.mosi.grease.provenancefiltering.model.entities.experiments.ExperimentEntity;
import org.mosi.grease.provenancefiltering.model.entities.experiments.ExperimentParameter;
import org.mosi.grease.provenancefiltering.model.enums.ActivityType;
import org.mosi.grease.provenancefiltering.model.enums.DataType;
import org.mosi.grease.provenancefiltering.model.enums.ExperimentRole;
import org.mosi.grease.provenancefiltering.model.enums.Status;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class GraphGeneratorHaack {

    SimulationModel sm2;

    public void prepareCaseStudyGraphWithLeeStudy(ProvModelController provModelController) {

        ProvenanceCommit commitBSM1 = new ProvenanceCommit();

        //Creating research question RQ1
        ResearchQuestion rq1 = new ResearchQuestion("RQ1", "Lee et al. 2003", "Why are two " +
                "scaffold proteins, APC and axin, both necessary? Do their roles differ?");
        commitBSM1.addProvenanceNode(rq1);

        //Creating assumptions A1-A3
        Assumption a1 = new Assumption("A1", "Lee et al. 2003", "Dsh, TCF, and GSK3b are " +
                "degraded very slowly, we assume that their concentrations remain constant throughout the timecourse " +
                "of a Wnt signaling event");
        commitBSM1.addProvenanceNode(a1);
        Assumption a2 = new Assumption("A2", "Lee et al. 2003", "Unimolecular reactions are " +
                "assumed to be irreversible and are described by linear rate equations");
        commitBSM1.addProvenanceNode(a2);
        Assumption a3 = new Assumption("A3", "Lee et al. 2003", "reversible binding steps " +
                "between axin, β-catenin, APC, and TCF are very fast, such that the corresponding protein complexes " +
                "are in rapid equilibrium");
        commitBSM1.addProvenanceNode(a3);

        //Creating requirement R1
        Requirement r1 = new Requirement("R1", "Lee et al. 2003", "axin stimulates the " +
                "phosphorylation of β-catenin by GSK3β at least 24,000-fold");
        commitBSM1.addProvenanceNode(r1);

        //Creating wet lab data from other studies
        Data wd1_Lee = new Data("WD1_Lee", "Lee et al. 2001", DataType.IN_VITRO, "CSV",
                "TCF reduces the rate of β-catenin degradation", Status.UNDEFINED);
        commitBSM1.addProvenanceNode(wd1_Lee);
        Data wd1_Dajani = new Data("WD1_Dajani", "Dajani et al. 2003", DataType.IN_VITRO, "CSV",
                "axin stimulates the phosphorylation of β-catenin by GSK3b at least 24,000-fold", Status.UNDEFINED);
        commitBSM1.addProvenanceNode(wd1_Dajani);

        //Creating qualitative model QM1
        //No parameters specified yet
        ArrayList<ExperimentParameter> parameters = new ArrayList<>();
        //Model entities
        ArrayList<ExperimentEntity> entities = new ArrayList<>();
        entities.add(new ExperimentEntity("W", "W", "UniProtKB:P31285"));
        entities.add(new ExperimentEntity("Dsh", "Dsh", "UniProtKB - P51142"));
        //entities.add(new ExperimentParameter("GSK3", "", ""));
        entities.add(new ExperimentEntity("Axin", "Axin", "UniProtKB:Q9YGY0"));
        //entities.add(new ExperimentParameter("APC", "", ""));
        entities.add(new ExperimentEntity("Total_B_Catenin", "Beta-catenin", "UniProtKB:P26233"));
        //entities.add(new ExperimentParameter("TCF", "", ""));
        QualitativeModel qm1 = new QualitativeModel("QM1", "Lee et al. 2003", parameters, entities);
        qm1.setDescription("Reaction scheme for Wnt signaling pathway");
        commitBSM1.addProvenanceNode(qm1);

        //Creating building simulation model activity BSM1
        ProvenanceActivity bsm1 = new ProvenanceActivity("BSM1", "Lee et al. 2003",
                ActivityType.CREATING_SIMULATION_MODEL);
        commitBSM1.addProvenanceNode(bsm1);

        //Creating resulting simulation model SM1
        SimulationModel sm1 = new SimulationModel("SM1", "Lee et al. 2003", "Copasi", "",
                "", false);
        sm1.setDescription("Provisional reference state model");
        commitBSM1.addProvenanceNode(sm1);

        //Creating relationships for building simulation model activity BSM1
        //ingoing
        commitBSM1.addProvenanceRelationship(bsm1, rq1);
        commitBSM1.addProvenanceRelationship(bsm1, a1);
        commitBSM1.addProvenanceRelationship(bsm1, a2);
        commitBSM1.addProvenanceRelationship(bsm1, a3);
        commitBSM1.addProvenanceRelationship(bsm1, r1);
        commitBSM1.addProvenanceRelationship(bsm1, qm1);
        commitBSM1.addProvenanceRelationship(bsm1, wd1_Lee);
        commitBSM1.addProvenanceRelationship(bsm1, wd1_Dajani);
        //outgoing
        commitBSM1.addProvenanceRelationship(sm1, bsm1);
        //End Commit
        provModelController.commitActivity(commitBSM1);

        //Start CSM1
        ProvenanceCommit commitCSM1 = new ProvenanceCommit();
        //Creating own wet lab data WD1-WD5
        Data wd1 = new Data("WD1", "Lee et al. 2003", DataType.IN_VITRO, "CSV",
                "turnover of GSK3b, Dsh, and TCF is relatively slow", Status.UNDEFINED);
        commitCSM1.addProvenanceNode(wd1);
        Data wd2 = new Data("WD2", "Lee et al. 2003", DataType.IN_VITRO, "CSV",
                "total concentrations of Dsh, TCF, GSK3b, axin, \beta-catenin, and APC in Xenopus egg extract " +
                        "were determined experimentally using Western blot analysis", Status.UNDEFINED);
        commitCSM1.addProvenanceNode(wd2);
        Data wd3 = new Data("WD3", "Lee et al. 2003", DataType.IN_VITRO, "CSV",
                "phosphorylated \beta-catenin dissociates from axin more rapidly (reaction 10) than " +
                        "nonphosphorylated \beta-catenin", Status.UNDEFINED);
        commitCSM1.addProvenanceNode(wd3);
        Data wd4 = new Data("WD4", "Lee et al. 2003", DataType.IN_VITRO, "CSV",
                "steady state flux v_{11} for the degradation of \beta-catenin via the Wnt pathway and the " +
                        "flux ratio v_{13}/v_{11} describing the extent to which \beta-catenin is degraded via " +
                        "non-Wnt mechanisms", Status.UNDEFINED);
        commitCSM1.addProvenanceNode(wd4);
        Data wd5 = new Data("WD5", "Lee et al. 2003", DataType.IN_VITRO, "CSV",
                "rate of axin degradation were determined directly from experiments performed in " +
                        "Xenopus egg extracts", Status.UNDEFINED);
        commitCSM1.addProvenanceNode(wd5);

        //Creating more wet lab data from other studies
        Data wd1_Salic = new Data("WD1_Salic", "Salic et al. 2000", DataType.IN_VITRO, "CSV",
                "\beta-catenin has a 10-fold lower affinity for nonphosphorylated compared to phosphorylated " +
                        "APC, ratio K_{17} / K_{8}=10", Status.UNDEFINED);
        commitCSM1.addProvenanceNode(wd1_Salic);
        Data wd2_Salic = new Data("WD2_Salic", "Salic et al. 2000", DataType.IN_VITRO, "CSV",
                "experimentally determined rate of \beta-catenin degradation", Status.UNDEFINED);
        commitCSM1.addProvenanceNode(wd2_Salic);
        Data wd3_Salic = new Data("WD3_Salic", "Salic et al. 2000", DataType.IN_VITRO, "CSV",
                "experiments to determine the rate of APC and axin dephosphorylation", Status.UNDEFINED);
        commitCSM1.addProvenanceNode(wd3_Salic);

        //Creating calibrating simulation model activity CSM1
        ProvenanceActivity csm1 = new ProvenanceActivity("CSM1", "Lee et al. 2003",
                ActivityType.CALIBRATING_SIMULATION_MODEL);
        csm1.setDescription("Development of calibrated reference state model which includes a large number of " +
                "experimental data");
        commitCSM1.addProvenanceNode(csm1);

        //Creating the resulting simulation experiment SE1 and simulation data SD1 of CSM1
        Experiment se1 = new Experiment("SE1", "Lee et al. 2003", ExperimentRole.CALIBRATION);
        se1.setDescription("number of parameters were set such that the results of the model were in agreement " +
                "with previous experimental data, specifically with the experimentally determined rate of " +
                "\beta-catenin degradation");
        commitCSM1.addProvenanceNode(se1);
        Data sd1 = new Data("SD1", "Lee et al. 2003", DataType.IN_SILICO, "CSV",
                "Simulation results of SE1", Status.UNDEFINED);
        commitCSM1.addProvenanceNode(sd1);

        //Creating resulting simulation model SM2
        sm2 = new SimulationModel("SM2", "Lee et al. 2003", "Copasi",
                "../BIOMD0000000658/MODEL1708310000_edited.cps", "", true);
        sm2.setDescription("Calibrated reference state model");
        commitCSM1.addProvenanceNode(sm2);

        //Creating Relationships for CSM1
        //ingoing
        commitCSM1.addProvenanceRelationship(csm1, sm1);
        commitCSM1.addProvenanceRelationship(csm1, wd1);
        commitCSM1.addProvenanceRelationship(csm1, wd2);
        commitCSM1.addProvenanceRelationship(csm1, wd3);
        commitCSM1.addProvenanceRelationship(csm1, wd4);
        commitCSM1.addProvenanceRelationship(csm1, wd5);
        commitCSM1.addProvenanceRelationship(csm1, wd1_Salic);
        commitCSM1.addProvenanceRelationship(csm1, wd2_Salic);
        commitCSM1.addProvenanceRelationship(csm1, wd3_Salic);
        //outgoing
        commitCSM1.addProvenanceRelationship(se1, csm1);
        commitCSM1.addProvenanceRelationship(sd1, csm1);
        commitCSM1.addProvenanceRelationship(sm2, csm1);
        //End Commit
        provModelController.commitActivity(commitCSM1);

        //Start with VSM1
        ProvenanceCommit commitVSM1 = new ProvenanceCommit();

        //Creating more wet lab data for VSM1
        Data wd6 = new Data("WD6", "Lee et al. 2003", DataType.IN_VITRO, "CSV",
                "Preincubation of Dsh in Xenopus egg extracts abolishes the lag in Dsh activity", Status.UNDEFINED);
        commitVSM1.addProvenanceNode(wd6);
        Data wd7 = new Data("WD7", "Lee et al. 2003", DataType.IN_VITRO, "CSV",
                "Kinetics of β-catenin degradation”, “tested the results using the previously described " +
                        "biochemical system (Salic et al. 2000; Lee et al. 2001), adding purified proteins or " +
                        "compounds at t=0", Status.UNDEFINED);
        commitVSM1.addProvenanceNode(wd7);

        //Creating validating simulation model activity VSM1
        ProvenanceActivity vsm1 = new ProvenanceActivity("VSM1", "Lee et al. 2003",
                ActivityType.VALIDATING_SIMULATION_MODEL);
        vsm1.setDescription("Model validation");
        commitVSM1.addProvenanceNode(vsm1);

        //Creating the resulting simulation experiment SE2 and simulation data SD2 for VSM1
        Experiment se2 = new Experiment("SE2", "Lee et al. 2003", ExperimentRole.VALIDATION);
        se2.setDescription("we ran through a series of simulations, all of which used the same set of parameters. " +
                "From these we calculated simulated timecourses for β-catenin degradation under a range of " +
                "different conditions (increased axin concentration, increased Dsh a concentration, inhibition " +
                "of GSK3b, increased TCF concentration)");
        commitVSM1.addProvenanceNode(se2);
        Data sd2 = new Data("SD2", "Lee et al. 2003", DataType.IN_SILICO, "CSV",
                "Simulation results of SE2 (validation successful)", Status.SUCCESS);
        commitVSM1.addProvenanceNode(sd2);

        //Creating relationships for VSM1
        //ingoing
        commitVSM1.addProvenanceRelationship(vsm1, sm2);
        commitVSM1.addProvenanceRelationship(vsm1, wd6);
        commitVSM1.addProvenanceRelationship(vsm1, wd7);
        //outgoing
        commitVSM1.addProvenanceRelationship(se2, vsm1);
        commitVSM1.addProvenanceRelationship(sd2, vsm1);
        //End Commit
        provModelController.commitActivity(commitVSM1);

        //Creating analyzing simulation model activity ASM1_1 with input and output entities and relationships
        ProvenanceCommit commitASM1_1 = new ProvenanceCommit();
        //WD8
        Data wd8 = new Data("WD8", "Lee et al. 2003", DataType.IN_VITRO, "CSV",
                "we studied experimentally the dose-dependent effects of Dsh, GSK3b, and axin on " +
                        "\beta-catenin degradation", Status.UNDEFINED);
        commitASM1_1.addProvenanceNode(wd8);
        //ASM1_1
        ProvenanceActivity asm1_1 = new ProvenanceActivity("ASM1_1", "Lee et al. 2003",
                ActivityType.ANALYZING_SIMULATION_MODEL);
        commitASM1_1.addProvenanceNode(asm1_1);
        //SE3
        Experiment se3 = new Experiment("SE3", "Lee et al. 2003", ExperimentRole.ANALYSIS);
        se3.setDescription("Effect of Dsh versus Axin or GSK3β on the half-life of β-catenin in Xenopus extracts");
        commitASM1_1.addProvenanceNode(se3);
        //SD3
        Data sd3 = new Data("SD3", "Lee et al. 2003", DataType.IN_SILICO, "CSV",
                "Simulation results of SE3", Status.UNDEFINED);
        commitASM1_1.addProvenanceNode(sd3);
        //Creating relationships for ASM1_1
        //ingoing
        commitASM1_1.addProvenanceRelationship(asm1_1, sm2);
        commitASM1_1.addProvenanceRelationship(asm1_1, wd8);
        //outgoing
        commitASM1_1.addProvenanceRelationship(se3, asm1_1);
        commitASM1_1.addProvenanceRelationship(sd3, asm1_1);
        //End Commit
        provModelController.commitActivity(commitASM1_1);

        //Creating analyzing simulation model activity ASM1_2 with its output entities and relationships
        ProvenanceCommit commitASM1_2 = new ProvenanceCommit();
        //ASM1_2
        ProvenanceActivity asm1_2 = new ProvenanceActivity("ASM1_2", "Lee et al. 2003",
                ActivityType.ANALYZING_SIMULATION_MODEL);
        commitASM1_2.addProvenanceNode(asm1_2);
        //SE4
        Experiment se4 = new Experiment("SE4", "Lee et al. 2003", ExperimentRole.ANALYSIS);
        se4.setDescription("theoretical effect of APC on the concentrations of both \beta-catenin and axin");
        commitASM1_2.addProvenanceNode(se4);
        //SD4
        Data sd4 = new Data("SD4", "Lee et al. 2003", DataType.IN_SILICO, "CSV",
                "Simulation results of SE4", Status.UNDEFINED);
        commitASM1_2.addProvenanceNode(sd4);
        //Creating relationships for ASM1_2
        //ingoing
        commitASM1_2.addProvenanceRelationship(asm1_2, sm2);
        //outgoing
        commitASM1_2.addProvenanceRelationship(se4, asm1_2);
        commitASM1_2.addProvenanceRelationship(sd4, asm1_2);
        //End Commit
        provModelController.commitActivity(commitASM1_2);

        //Creating analyzing simulation model activity ASM1_3 with its output entities and relationships
        ProvenanceCommit commitASM1_3 = new ProvenanceCommit();
        //ASM1_3
        ProvenanceActivity asm1_3 = new ProvenanceActivity("ASM1_3", "Lee et al. 2003",
                ActivityType.ANALYZING_SIMULATION_MODEL);
        commitASM1_3.addProvenanceNode(asm1_3);
        //SE5
        Experiment se5 = new Experiment("SE5", "Lee et al. 2003", ExperimentRole.ANALYSIS);
        se5.setDescription("simulated the effects of changes in the rate of \beta-catenin (v_{12}) and axin (v_{14}) " +
                "synthesis on both \beta-catenin and axin levels");
        commitASM1_3.addProvenanceNode(se5);
        //SD5
        Data sd5 = new Data("SD5", "Lee et al. 2003", DataType.IN_SILICO, "CSV",
                "Simulation results of SE5", Status.UNDEFINED);
        commitASM1_3.addProvenanceNode(sd5);
        //Creating relationships for ASM1_3
        //ingoing
        commitASM1_3.addProvenanceRelationship(asm1_3, sm2);
        //outgoing
        commitASM1_3.addProvenanceRelationship(se5, asm1_3);
        commitASM1_3.addProvenanceRelationship(sd5, asm1_3);
        //End Commit
        provModelController.commitActivity(commitASM1_3);

        //Creating analyzing simulation model activity ASM1_4 with its output entities and relationships
        ProvenanceCommit commitASM1_4 = new ProvenanceCommit();
        //ASM1_4
        ProvenanceActivity asm1_4 = new ProvenanceActivity("ASM1_4", "Lee et al. 2003",
                ActivityType.ANALYZING_SIMULATION_MODEL);
        commitASM1_4.addProvenanceNode(asm1_4);
        //SE6
        Experiment se6 = new Experiment("SE6", "Lee et al. 2003", ExperimentRole.ANALYSIS);
        se6.setDescription("transient Wnt stimulation by an exponential decay” with different rates of " +
                "axin synthesis and degradation");
        se6.setReference("../BIOMD0000000658/MODEL1708310000_edited.sedml");
        se6.setModelName("../BIOMD0000000658/MODEL1708310000_edited.cps");
        commitASM1_4.addProvenanceNode(se6);
        //SD6
        Data sd6 = new Data("SD6", "Lee et al. 2003", DataType.IN_SILICO, "CSV",
                "Simulation results of SE6", Status.UNDEFINED);
        commitASM1_4.addProvenanceNode(sd6);
        //Creating relationships for ASM1_4
        //ingoing
        commitASM1_4.addProvenanceRelationship(asm1_4, sm2);
        //outgoing
        commitASM1_4.addProvenanceRelationship(se6, asm1_4);
        commitASM1_4.addProvenanceRelationship(sd6, asm1_4);
        //End Commit
        provModelController.commitActivity(commitASM1_4);

        //Creating building simulation model activitiy BSM2
        ProvenanceCommit commitBSM2 = new ProvenanceCommit();
        ProvenanceActivity bsm2 = new ProvenanceActivity("BSM2", "Lee et al. 2003",
                ActivityType.REFINING_SIMULATION_MODEL);
        commitBSM2.addProvenanceNode(bsm2);

        //Creating resulting simulation model SM3
        SimulationModel sm3 = new SimulationModel("SM3", "Lee et al. 2003", "Copasi", "", "", false);
        sm3.setDescription("Model extension");
        commitBSM2.addProvenanceNode(sm3);

        //Creating relationships for BSM2
        //ingoing
        commitBSM2.addProvenanceRelationship(bsm2, sm2);
        //outgoing
        commitBSM2.addProvenanceRelationship(sm3, bsm2);
        //End Commit
        provModelController.commitActivity(commitBSM2);

        //Creating analyzing simulation model activity ASM2_1 with input and output entities and relationships
        ProvenanceCommit commitASM2_1 = new ProvenanceCommit();
        //WD9
        Data wd9 = new Data("WD9", "Lee et al. 2003", DataType.IN_VITRO, "CSV",
                "Effects of increasing axin concentration on \beta-catenin degradation", Status.UNDEFINED);
        commitASM2_1.addProvenanceNode(wd9);
        //ASM2_1
        ProvenanceActivity asm2_1 = new ProvenanceActivity("ASM2_1", "Lee et al. 2003",
                ActivityType.ANALYZING_SIMULATION_MODEL);
        commitASM2_1.addProvenanceNode(asm2_1);
        //SE7
        Experiment se7 = new Experiment("SE7", "Lee et al. 2003", ExperimentRole.ANALYSIS);
        se7.setDescription("Effects of increasing axin concentration on \beta-catenin degradation");
        commitASM2_1.addProvenanceNode(se7);
        //SD7
        Data sd7 = new Data("SD7", "Lee et al. 2003", DataType.IN_SILICO, "CSV",
                "Simulation results of SE7", Status.UNDEFINED);
        commitASM2_1.addProvenanceNode(sd7);
        //Creating relationships for ASM2_1
        //ingoing
        commitASM2_1.addProvenanceRelationship(asm2_1, sm3);
        commitASM2_1.addProvenanceRelationship(asm2_1, wd9);
        //outgoing
        commitASM2_1.addProvenanceRelationship(se7, asm2_1);
        commitASM2_1.addProvenanceRelationship(sd7, asm2_1);
        //End Commit
        provModelController.commitActivity(commitASM2_1);

        //Creating analyzing simulation model activity ASM2_2 with its output entities and relationships
        ProvenanceCommit commitASM2_2 = new ProvenanceCommit();
        //ASM2_2
        ProvenanceActivity asm2_2 = new ProvenanceActivity("ASM2_2", "Lee et al. 2003",
                ActivityType.ANALYZING_SIMULATION_MODEL);
        commitASM2_2.addProvenanceNode(asm2_2);
        //SE8
        Experiment se8 = new Experiment("SE8", "Lee et al. 2003", ExperimentRole.ANALYSIS);
        se8.setDescription("Effects of APC concentrations on \beta-catenin degradation");
        commitASM2_2.addProvenanceNode(se8);
        //SD8
        Data sd8 = new Data("SD8", "Lee et al. 2003", DataType.IN_SILICO, "CSV",
                "Simulation results of SE8", Status.UNDEFINED);
        commitASM2_2.addProvenanceNode(sd8);
        //Creating relationships for ASM2_2
        //ingoing
        commitASM2_2.addProvenanceRelationship(asm2_2, sm3);
        //outgoing
        commitASM2_2.addProvenanceRelationship(se8, asm2_2);
        commitASM2_2.addProvenanceRelationship(sd8, asm2_2);
        //End Commit
        provModelController.commitActivity(commitASM2_2);

        //Creating analyzing simulation model activity ASM2_3 with its output entities and relationships
        ProvenanceCommit commitASM2_3 = new ProvenanceCommit();
        //ASM2_3
        ProvenanceActivity asm2_3 = new ProvenanceActivity("ASM2_3", "Lee et al. 2003",
                ActivityType.ANALYZING_SIMULATION_MODEL);
        commitASM2_3.addProvenanceNode(asm2_3);
        //SE9
        Experiment se9 = new Experiment("SE9", "Lee et al. 2003", ExperimentRole.ANALYSIS);
        se9.setDescription("effect of these alternative pathways [with and without regulatory loop Eq. 5] becomes " +
                "much more prominent when the APC concentration is lowered");
        commitASM2_3.addProvenanceNode(se9);
        //SD9
        Data sd9 = new Data("SD9", "Lee et al. 2003", DataType.IN_SILICO, "CSV",
                "Simulation results of SE9", Status.UNDEFINED);
        commitASM2_3.addProvenanceNode(sd9);
        //Creating relationships for ASM2_3
        //ingoing
        commitASM2_3.addProvenanceRelationship(asm2_3, sm3);
        //outgoing
        commitASM2_3.addProvenanceRelationship(se9, asm2_3);
        commitASM2_3.addProvenanceRelationship(sd9, asm2_3);
        //End Commit
        provModelController.commitActivity(commitASM2_3);

        //Creating analyzing simulation model activity ASM2_4 with its output entities and relationships
        ProvenanceCommit commitASM2_4 = new ProvenanceCommit();
        //ASM2_4
        ProvenanceActivity asm2_4 = new ProvenanceActivity("ASM2_4", "Lee et al. 2003",
                ActivityType.ANALYZING_SIMULATION_MODEL);
        commitASM2_4.addProvenanceNode(asm2_4);
        //SE10
        Experiment se10 = new Experiment("SE10", "Lee et al. 2003", ExperimentRole.ANALYSIS);
        se10.setDescription("Sensitivity analysis (control coefficients) of model regarding reaction rate constants");
        commitASM2_4.addProvenanceNode(se10);
        //SD10
        Data sd10 = new Data("SD10", "Lee et al. 2003", DataType.IN_SILICO, "CSV",
                "Simulation results of SE10", Status.UNDEFINED);
        commitASM2_4.addProvenanceNode(sd10);
        //Creating relationships for ASM2_4
        //ingoing
        commitASM2_4.addProvenanceRelationship(asm2_4, sm3);
        //outgoing
        commitASM2_4.addProvenanceRelationship(se10, asm2_4);
        commitASM2_4.addProvenanceRelationship(sd10, asm2_4);
        //End Commit
        provModelController.commitActivity(commitASM2_4);

        //Creating analyzing simulation model activity ASM2_5 with its output entities and relationships
        ProvenanceCommit commitASM2_5 = new ProvenanceCommit();
        //ASM2_5
        ProvenanceActivity asm2_5 = new ProvenanceActivity("ASM2_5", "Lee et al. 2003",
                ActivityType.ANALYZING_SIMULATION_MODEL);
        commitASM2_5.addProvenanceNode(asm2_5);
        //SE11
        Experiment se11 = new Experiment("SE11", "Lee et al. 2003", ExperimentRole.ANALYSIS);
        se11.setDescription("Sensitivity analysis (control coefficients) of model regarding concentration");
        commitASM2_5.addProvenanceNode(se11);
        //SD11
        Data sd11 = new Data("SD11", "Lee et al. 2003", DataType.IN_SILICO, "CSV",
                "Simulation results of SE11", Status.UNDEFINED);
        commitASM2_5.addProvenanceNode(sd11);
        //Creating relationships for ASM2_5
        //ingoing
        commitASM2_5.addProvenanceRelationship(asm2_5, sm3);
        //outgoing
        commitASM2_5.addProvenanceRelationship(se11, asm2_5);
        commitASM2_5.addProvenanceRelationship(sd11, asm2_5);
        //End Commit
        provModelController.commitActivity(commitASM2_5);

    }

    public void generateCaseStudyGraph(ProvModelController provModelController){

        //Start with automatic support here

        //Start activity commit
        ProvenanceCommit commitBSM1 = new ProvenanceCommit();

        //Creating research question RQ1
        ResearchQuestion rq1 = new ResearchQuestion("RQ1","Haack et al. 2015",
                "evaluate, whether an interplay between ROS-induced and lipid raft dependent " +
                        "WNT/β-catenin signaling can explain our experimental results we apply " +
                        "computational modeling");
        commitBSM1.addProvenanceNode(rq1);

        //Creating qualitative model QM1
        ArrayList<ExperimentParameter> parameters = new ArrayList<>();
        parameters.add(new ExperimentParameter("nWnt", 325.0));
        parameters.add(new ExperimentParameter("scalingFactor", 1.0));
        ArrayList<ExperimentEntity> entities = new ArrayList<>();
        entities.add(new ExperimentEntity("Wnt", "Wnt", "UniProtKB:P31285"));
        entities.add(new ExperimentEntity("Axin", "Axin", "UniProtKB:Q9YGY0"));
        entities.add(new ExperimentEntity("Dvl", "Dvl", "UniProtKB:P51142"));
        entities.add(new ExperimentEntity("Cell/Nuc/Bcat", "bcat", "UniProtKB:P26233"));
        //entities.add(new ExperimentEntity("LRP5", ""));
        //entities.add(new ExperimentEntity("LRP6", ""));
        //entities.add(new ExperimentEntity("CK1y", ""));
        QualitativeModel qm1 = new QualitativeModel("QM1", "Haack et al. 2015", parameters, entities);
        qm1.setDescription("schematic representation of our basic WNT model");
        commitBSM1.addProvenanceNode(qm1);

        //Creating assumptions A1-A5
        Assumption a1 = new Assumption("A1", "Haack et al. 2015", "“reduce the representation " +
                "of the receptor-complex and the signalosome." +
                "Accordingly, the FZ-LRP6 receptor complex is only represented by LRP6");
        commitBSM1.addProvenanceNode(a1);
        Assumption a2 = new Assumption("A2", "Haack et al. 2015", "we consider solely the " +
                "interaction between CK1γ and LRP6, whereas a detailed representation of DVL mediated unspecific " +
                "phosphorylation of LRP6 by GSK3β is omitted");
        commitBSM1.addProvenanceNode(a2);
        Assumption a3 = new Assumption("A3", "Haack et al. 2015", "include lipid rafts as " +
                "individual compartments within the membrane");
        commitBSM1.addProvenanceNode(a3);
        Assumption a4 = new Assumption("A4", "Haack et al. 2015", "we solely consider AXIN " +
                "as a condensed representation of the destruction complex" +
                "disregarding its remaining components, like GSK3β, APC and CK1α");
        commitBSM1.addProvenanceNode(a4);
        Assumption a5 = new Assumption("A5", "Haack et al. 2015", "released WNT molecules " +
                "can directly induce the WNT/β-catenin signaling at the cell surface in an autocrine manner");
        commitBSM1.addProvenanceNode(a5);

        //Creating own wet lab data WD1-2
        Data wd1 = new Data("WD1", "Haack et al. 2015", DataType.IN_VITRO, "CSV",
                "Lipid rafts disruption: disruption by methyl-β cyclodextrin (MbCD) (shows no impact " +
                "on lateral distribution of LRP6)", Status.UNDEFINED); //Data generated by is what?
        commitBSM1.addProvenanceNode(wd1);
        Data wd2 = new Data("WD2", "Haack et al. 2015", DataType.IN_VITRO, "CSV",
                "Impact of raft disruption on temporal regulation of nuclear β-catenin concentration " +
                "after induction of differentiation in ReNCell", Status.UNDEFINED);
        commitBSM1.addProvenanceNode(wd2);

        //Creating wet lab data from other studies
        Data wd1_Bafico = new Data("WD1_Bafico", "Bafico et al. 2001", DataType.IN_VITRO, "CSV",
                "A Scatchard plot of the data for [^{125}I] Dkk-1 binding to NIH3T3 cells", Status.UNDEFINED);
        commitBSM1.addProvenanceNode(wd1_Bafico);
        Data wd1_Prior = new Data("WD1_Prior", "Prior et al. 2003", DataType.IN_VITRO, "CSV",
                "Visualizing lipid rafts using electron microscopy and spatial point pattern analysis.” -" +
                        "> “lipid raft marker displays cholesterol-dependent clustering in microdomains with a " +
                        "mean diameter of 44 nm that occupy 35% of the cell surface", Status.UNDEFINED);
        commitBSM1.addProvenanceNode(wd1_Prior);
        Data wd1_Pralle = new Data("WD1_Pralle", "Pralle et al. 2000", DataType.IN_VITRO, "CSV",
                "Summary of the local viscous drags measured for the GPI-anchored raft-markers " +
                "PLAP and YFPGLGPI and the transmembrane raft protein HA” -> “the estimated radius " +
                "of the rafts is r = 26 +- 13 nm", Status.UNDEFINED);
        commitBSM1.addProvenanceNode(wd1_Pralle);
        Data wd1_Krieghoff = new Data("WD1_Krieghoff", "Krieghoff et al. 2006", DataType.IN_VITRO, "CSV",
                "β-catenin shuttles efficiently between nucleus and cytoplasm and is mobile in both " +
                "compartments", Status.UNDEFINED);
        commitBSM1.addProvenanceNode(wd1_Krieghoff);
        Data wd1_Hannoush = new Data("WD1_Hannoush", "Hannoush 2008", DataType.IN_VITRO, "CSV",
                "Kinetics of cellular β-catenin accumulation upon stimulation with Wnt3a", Status.UNDEFINED);
        commitBSM1.addProvenanceNode(wd1_Krieghoff);

        //Creating simulation model SM7 by Mazemondet et al.
        SimulationModel sm7 = new SimulationModel("SM7", "Mazemondet et al. 2012", "", "", "", true);
        commitBSM1.addProvenanceNode(sm7);

        //Creating building simulation model activity BSM1
        ProvenanceActivity bsm1 = new ProvenanceActivity("BSM1", "Haack et al. 2015", ActivityType.REFINING_SIMULATION_MODEL);
        bsm1.setDescription("Creation of core model");
        commitBSM1.addProvenanceNode(bsm1);

        //Creating resulting simulation model SM1
        SimulationModel sm1 = new SimulationModel("SM1", "Haack et al. 2015", "ML-Rules", "", "", false);
        sm1.setDescription("uncalibrated model code of QM1");
        commitBSM1.addProvenanceNode(sm1);

        //Creating relationships for BSM1
        //ingoing
        commitBSM1.addProvenanceRelationship(bsm1, a1);
        commitBSM1.addProvenanceRelationship(bsm1, a2);
        commitBSM1.addProvenanceRelationship(bsm1, a3);
        commitBSM1.addProvenanceRelationship(bsm1, a4);
        commitBSM1.addProvenanceRelationship(bsm1, a5);
        commitBSM1.addProvenanceRelationship(bsm1, wd1_Bafico);
        commitBSM1.addProvenanceRelationship(bsm1, wd1_Krieghoff);
        commitBSM1.addProvenanceRelationship(bsm1, wd1_Pralle);
        commitBSM1.addProvenanceRelationship(bsm1, sm2);
        commitBSM1.addProvenanceRelationship(bsm1, sm7);
        commitBSM1.addProvenanceRelationship(bsm1, wd1);
        commitBSM1.addProvenanceRelationship(bsm1, wd2);
        commitBSM1.addProvenanceRelationship(bsm1, rq1);
        commitBSM1.addProvenanceRelationship(bsm1, qm1);
        //outgoing
        commitBSM1.addProvenanceRelationship(sm1, bsm1);

        //Commit Activity
        provModelController.commitActivity(commitBSM1);

        //Creating entities for calibrating simulation model activity CSM1
        //Start commit
        ProvenanceCommit commitCSM1 = new ProvenanceCommit();
        //SD1
        Data sd1 = new Data("SD1", "Haack et al. 2015", DataType.IN_SILICO, "CSV",
                "Simulation results of SE1", Status.UNDEFINED);
        commitCSM1.addProvenanceNode(sd1);
        //SE1
        Experiment se1 = new Experiment("SE1", "Haack et al. 2015", ExperimentRole.CALIBRATION);
        se1.setDescription("calibration: “fitted the remaining parameter values of the combined intracellular and " +
                "membrane model against in vitro measurements we derived from human neuronal " +
                "progenitor cells (ReNcell VM197)” (with SESSL)");
        commitCSM1.addProvenanceNode(se1);
        //CSM1
        ProvenanceActivity csm1 = new ProvenanceActivity("CSM1", "Haack et al. 2015", ActivityType.CALIBRATING_SIMULATION_MODEL);
        csm1.setDescription("Parameter fitting (to own results)");
        commitCSM1.addProvenanceNode(csm1);
        //SM2
        SimulationModel sm2 = new SimulationModel("SM2", "Haack et al. 2015", "ML-Rules",
                "src/main/resources/case-studies/TOMACS/models/M2_2.mlrj", "", false);
        sm2.setDescription("calibrated model code of QM1");
        commitCSM1.addProvenanceNode(sm2);

        //Creating relationships for CSM1
        //ingoing
        commitCSM1.addProvenanceRelationship(csm1, sm1);
        commitCSM1.addProvenanceRelationship(csm1, wd2);
        //outgoing
        commitCSM1.addProvenanceRelationship(se1, csm1);
        commitCSM1.addProvenanceRelationship(sd1, csm1);
        commitCSM1.addProvenanceRelationship(sm2, csm1);

        //Commit Activity
        provModelController.commitActivity(commitCSM1);

        //Automatically Generating Validation1 is triggered

    }


    @NotNull
    public org.json.JSONObject loadCaseStudyJsonObjectFromPath(String resource) {

        // this is the path within the jar file
        InputStream input = this.getClass().getResourceAsStream("/resources/" + resource);
        if (input == null) {
            // this is how we load file within editor (eg eclipse)
            input = this.getClass().getClassLoader().getResourceAsStream(resource);
        }

        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.getFactory().disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);
        try {
            map = mapper.readValue(input, LinkedHashMap.class);
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        org.json.JSONObject schemaJson = new org.json.JSONObject(map);
        return schemaJson;
    }

    private JSONObject loadCaseStudyJson(){
        String resource = "case-studies/experimentGenerationGUI/exampleJsons/fem-example.json";

        // this is the path within the jar file
        InputStream input = this.getClass().getResourceAsStream("/resources/" + resource);
        if (input == null) {
            // this is how we load file within editor (eg eclipse)
            input = this.getClass().getClassLoader().getResourceAsStream(resource);
        }

        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.getFactory().disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);
        try {
            map = mapper.readValue(input, LinkedHashMap.class);
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        org.json.JSONObject schemaJson = new org.json.JSONObject(map);
        return schemaJson;

    }
}
