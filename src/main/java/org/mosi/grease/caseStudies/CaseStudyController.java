package org.mosi.grease.caseStudies;

import org.mosi.grease.caseStudies.TOMACS.TOMACSCaseStudyController;
import org.mosi.grease.caseStudies.WSC2021.WSC2021CaseStudyController;
import org.mosi.grease.experimentgeneration.controller.MainExperimentController;

public class CaseStudyController {

    public void runCaseStudy(MainExperimentController mainExperimentController, String caseStudyName) {
        CaseStudy caseStudy = CaseStudy.valueOf(caseStudyName);
        if(caseStudy == CaseStudy.MIGRATION || caseStudy == CaseStudy.WNT || caseStudy == CaseStudy.ABSTRACT) {
            TOMACSCaseStudyController TOMACSCaseStudyController = new TOMACSCaseStudyController(mainExperimentController, caseStudyName);
            TOMACSCaseStudyController.runCaseStudy();
        }
        else if(caseStudy == CaseStudy.WSC2021) {
            WSC2021CaseStudyController wsc2021CaseStudyController = new WSC2021CaseStudyController(mainExperimentController, caseStudyName);
            WSC2021CaseStudyController.runCaseStudy();
        }
    }
}
