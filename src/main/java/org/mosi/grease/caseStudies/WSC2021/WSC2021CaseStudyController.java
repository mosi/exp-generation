package org.mosi.grease.caseStudies.WSC2021;

import org.mosi.grease.caseStudies.AbstractCaseStudyController;
import org.mosi.grease.caseStudies.CaseStudy;
import org.mosi.grease.experimentgeneration.controller.MainExperimentController;
import org.mosi.grease.provenancefiltering.controller.MainController;

import java.util.Observable;
import java.util.Observer;

public class WSC2021CaseStudyController extends AbstractCaseStudyController implements Observer {
    MainExperimentController mainExperimentController;
    MainController mainController;
    GraphGeneratorWnt graphGeneratorWnt;

    CaseStudy caseStudy;
    private static int currentCaseStudyPart = 1;

    public WSC2021CaseStudyController(MainExperimentController mainExperimentController, String caseStudy) {
        this.mainExperimentController = mainExperimentController;
        this.caseStudy = CaseStudy.valueOf(caseStudy);

        //initialize
        mainController = new MainController(mainExperimentController);
        mainController.setAbstractCaseStudyController(this);
    }

    public static void runCaseStudy() {

    }

    @Override
    public void update(Observable o, Object arg) {

    }
}
