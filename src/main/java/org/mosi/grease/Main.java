package org.mosi.grease;


import javafx.application.Application;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.stage.Stage;
import org.apache.commons.cli.*;
import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;
import org.mosi.grease.caseStudies.CaseStudyController;
import org.mosi.grease.experimentgeneration.controller.MainExperimentController;
import org.mosi.grease.experimentgeneration.model.ExperimentModel;
import org.mosi.grease.experimentgeneration.view.View;


import java.util.Optional;

public class Main extends Application {


    private static Stage window;
    private static MainExperimentController mainExperimentController;

    private static String MODE = "GUI";
    public static boolean INTERACTIVE = false;
    public static boolean SKIP_EXECUTION = false;

    private static String BACKEND_NAME = null;
    private static String FILE_NAME = null;
    private static String CASE_STUDY_NAME = null;

    public static void main(String[] args) {
        parseArgs(args);
        launch(new String[]{});
    }

    private static void parseArgs(String[] args) {
        CommandLine commandLine;
        Option option_g;
        option_g = Option.builder("g")
                .hasArg()
                .optionalArg(true)
                .desc("Use the GUI to manually specify or adapt an experiment")
                .longOpt("gui")
                .build();
        Option option_t = Option.builder("t")
                .numberOfArgs(2)
                .desc("Translate an experiment specification automatically for a specific backend")
                .longOpt("translate")
                .build();
        Option option_c = Option.builder("c")
                .numberOfArgs(1)
                .desc("Run a case study")
                .longOpt("case")
                .build();
        //additional option, can be added to all of the above
        Option option_s = Option.builder("s")
                .hasArg()
                .optionalArg(true)
                .desc("Skip execution")
                .longOpt("skip")
                .build();
        Options options = new Options();
        CommandLineParser parser = new DefaultParser();

        options.addOption(option_g);
        options.addOption(option_t);
        options.addOption(option_c);
        options.addOption(option_s);

        try
        {
            commandLine = parser.parse(options, args);

            boolean gSupplied = commandLine.hasOption("g");
            boolean tSupplied = commandLine.hasOption("t");
            boolean cSupplied = commandLine.hasOption("c");

            if (gSupplied && !tSupplied && !cSupplied) {
                MODE = "GUI";
                FILE_NAME = commandLine.getOptionValue("g");
            }
            else if (tSupplied && !gSupplied && !cSupplied) {
                MODE = "TRANSLATE";
                String[] optionValues = commandLine.getOptionValues("t");
                FILE_NAME = optionValues[0];
                BACKEND_NAME = optionValues[1];
            }
            else if (cSupplied && !gSupplied && !tSupplied) {
                MODE = "CASE_STUDY";
                String[] optionValues = commandLine.getOptionValues("c");
                CASE_STUDY_NAME = optionValues[0];
                // INTERACTIVE = Boolean.parseBoolean(optionValues[1]);
                // SKIP_EXECUTION = Boolean.parseBoolean(optionValues[2]);
            }
            else {
                throw new ParseException("Wrong number of options. Use either -g filename or -t <filename> <backendname> " +
                        "or -c <casestudyname>");
            }

            //skip execution ofption
            if(commandLine.hasOption("s")) {
                SKIP_EXECUTION = true;
            }
        }
        catch (ParseException exception)
        {
            System.out.print("Parse error: ");
            System.out.println(exception.getMessage());
        }
    }

    @SuppressWarnings("unused")
    @Override
    public void start(Stage primaryStage) {
        //Initiate Model, View, Controller
        ExperimentModel experimentModel = new ExperimentModel();
        View view = new View(experimentModel, primaryStage);
        mainExperimentController = new MainExperimentController(experimentModel, view);

        window = primaryStage;
        window.hide();

        // load gui
        switch (MODE) {
            case "GUI":
                window.show();
                if (!(FILE_NAME == null)) {
                    //TODO load file to gui
                    System.out.println(FILE_NAME);
                }
                break;
            // translate file automatically
            case "TRANSLATE":
                //TODO load file and translate for backend
                System.out.println(FILE_NAME);
                System.out.println(BACKEND_NAME);
                break;
            // provenance-based support - run case study
            case "CASE_STUDY":

                // with provenance gui
                if (INTERACTIVE) { //TODO display files
                    MainProvenanceViewCotroller.setUseAsViewOnly(true);
                    mainExperimentController.setMainProvenanceViewCotroller(new MainProvenanceViewCotroller());
                    mainExperimentController.getMainProvenanceViewCotroller().showView();
                }

                if(SKIP_EXECUTION) {
                    mainExperimentController.setSkipExecution(true);
                }

                CaseStudyController caseStudyController = new CaseStudyController();
                caseStudyController.runCaseStudy(mainExperimentController, CASE_STUDY_NAME);
                break;
        }

        window.setOnCloseRequest(e -> {
            e.consume();
            closeWindows();
        });

        window.setTitle("GrEASE Experiment Generator");
    }

    private void closeWindows() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Closing Window");
        alert.setHeaderText("The system will close");
        alert.setContentText("Do you want to close the system?");

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == ButtonType.OK) {
            window.close();
        } else if (result.get() == ButtonType.CANCEL) {
            alert.close();
        }
    }

    public static Stage getWindow() {
        return window;
    }

}
