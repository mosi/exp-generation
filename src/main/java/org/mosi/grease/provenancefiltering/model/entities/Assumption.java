package org.mosi.grease.provenancefiltering.model.entities;

import org.mosi.grease.provenancefiltering.model.ProvenanceEntity;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

import java.util.HashMap;

public class Assumption extends ProvenanceEntity {

    private HashMap<String, Object> ontologyTags;
    private static int counterForNameGeneration = 1;

    public Assumption(String studyName) {
        super("A" + counterForNameGeneration, studyName, ProvenanceNodeType.ASSUMPTION);
        this.ontologyTags = new HashMap<>();
        counterForNameGeneration++;
    }

    public Assumption(String id, String studyName) {
        super(id, studyName, ProvenanceNodeType.ASSUMPTION);
        this.ontologyTags = new HashMap<>();
        counterForNameGeneration++;
    }

    public Assumption(String id, String studyName, String description) {
        super(id, studyName, ProvenanceNodeType.ASSUMPTION);
        this.ontologyTags = new HashMap<>();
        super.setDescription(description);
        counterForNameGeneration++;
    }

    public void addOntologyTag(String ontologyUrl, Object value){
        ontologyTags.put(ontologyUrl, value);
    }
}
