package org.mosi.grease.provenancefiltering.model;

import java.util.ArrayList;

public class ProvenanceCommit {

    ArrayList<ProvenanceNode> provenanceNodeList;
    ArrayList<ProvenanceRelationship> provenanceRelationshipList;

    public ProvenanceCommit() {
       this.provenanceNodeList = new ArrayList<>();
       this.provenanceRelationshipList = new ArrayList<>();
    }

    public void addProvenanceNode(ProvenanceNode provenanceNode) {
        this.provenanceNodeList.add(provenanceNode);
    }

    public void addProvenanceRelationship(ProvenanceNode sourceNode, ProvenanceNode targetNode) {
        ProvenanceRelationship relationship = new ProvenanceRelationship(sourceNode, targetNode);
        this.provenanceRelationshipList.add(relationship);
    }

    public ArrayList<ProvenanceNode> getProvenanceNodeList() {
        return provenanceNodeList;
    }

    public ArrayList<ProvenanceRelationship> getProvenanceRelationshipList() {
        return provenanceRelationshipList;
    }
}
