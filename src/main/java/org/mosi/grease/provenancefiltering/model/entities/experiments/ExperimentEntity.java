package org.mosi.grease.provenancefiltering.model.entities.experiments;

import java.util.ArrayList;

public class ExperimentEntity {
    String name;
    String alias;
    String ontologyTag;
    double initialValue;
    String unit;

    public ExperimentEntity(String name, String alias, String ontologyTag) {
        this.name = name;
        this.alias = alias;
        this.ontologyTag = ontologyTag;
        this.initialValue = initialValue;
        this.unit = unit;
    }

    public ExperimentEntity(String name, String alias, String ontologyTag, double initialValue, String unit) {
        this.name = name;
        this.alias = alias;
        this.ontologyTag = ontologyTag;
        this.initialValue = initialValue;
        this.unit = unit;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOntologyTag() {
        return ontologyTag;
    }

    public void setOntologyTag(String ontologyTag) {
        this.ontologyTag = ontologyTag;
    }

    public double getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(double initialValue) {
        this.initialValue = initialValue;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }
}
