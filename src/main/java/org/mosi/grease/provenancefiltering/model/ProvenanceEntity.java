package org.mosi.grease.provenancefiltering.model;

import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

public abstract class ProvenanceEntity extends ProvenanceNode {
    public ProvenanceEntity(String name, String studyName, ProvenanceNodeType type) {
        super(name, studyName, type);
    }
    public void setDescription(String description){
        super.setDescription(description);
    }

}
