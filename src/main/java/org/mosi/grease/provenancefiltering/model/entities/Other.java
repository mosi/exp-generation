package org.mosi.grease.provenancefiltering.model.entities;

import org.mosi.grease.provenancefiltering.model.ProvenanceEntity;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

public class Other extends ProvenanceEntity {

    private static int counterForNameGeneration = 1;

    public Other(String name, String studyName) {
        super(name, studyName, ProvenanceNodeType.OTHER);
        counterForNameGeneration++;
    }

    public Other(String studyName) {
        super("O" + counterForNameGeneration, studyName, ProvenanceNodeType.OTHER);
        counterForNameGeneration++;
    }
}
