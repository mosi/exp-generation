package org.mosi.grease.provenancefiltering.model.entities;

import org.mosi.grease.provenancefiltering.model.ProvenanceEntity;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

public class ResearchQuestion extends ProvenanceEntity {

    private static int counterForNameGeneration = 1;

    public ResearchQuestion(String studyName) {
        super("RQ" + counterForNameGeneration, studyName, ProvenanceNodeType.RESEARCHQUESTION);
        counterForNameGeneration++;
    }

    public ResearchQuestion(String id, String studyName) {
        super(id, studyName, ProvenanceNodeType.RESEARCHQUESTION);
        counterForNameGeneration++;
    }

    public ResearchQuestion(String id, String studyName, String description){
        super(id, studyName, ProvenanceNodeType.RESEARCHQUESTION);
        super.setDescription(description);
        counterForNameGeneration++;
    }

}
