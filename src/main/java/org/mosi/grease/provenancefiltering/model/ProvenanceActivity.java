package org.mosi.grease.provenancefiltering.model;


import org.mosi.grease.provenancefiltering.model.enums.ActivityType;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ProvenanceActivity extends ProvenanceNode {
    private ActivityType activityType;
    private static Map<String, Integer> counterForNameGenerationBSMMap = new HashMap<>();
    //private static int counterForNameGenerationBSM = 1;
    private static Map<String, Integer> counterForNameGenerationCSMMap = new HashMap<>();
    //private static int counterForNameGenerationCSM = 1;
    private static Map<String, Integer> counterForNameGenerationVSMMap = new HashMap<>();
    //private static int counterForNameGenerationVSM = 1;
    private static Map<String, Integer> counterForNameGenerationASMMap = new HashMap<>();
    //private static int counterForNameGenerationASM = 1;

    //id is set automatically
    public ProvenanceActivity(String studyName, ActivityType activityType, String activityName) {
        super("tempName", studyName, ProvenanceNodeType.ACTIVITY);
        boolean isStandardPrefix = getIsStandardPrefix(activityName);
        switch (activityType){
            //building simulation model activities
            case REFINING_SIMULATION_MODEL:
            case CREATING_SIMULATION_MODEL:
            case REIMPLEMENTING_SIMULATION_MODEL:
            case COMPOSING_SIMULATION_MODEL: {
                Integer counter = counterForNameGenerationBSMMap.get(studyName);
                if(counter == null) {
                    //if this is the first experiment in the study
                    counter = 1;
                }
                if(isStandardPrefix) {
                    this.setId("BSM" + counter);
                } else {
                    this.setId("a" + counter);
                }
                counterForNameGenerationBSMMap.put(studyName, counter + 1);
                break;
            }
            case CALIBRATING_SIMULATION_MODEL: {
                Integer counter = counterForNameGenerationCSMMap.get(studyName);
                if(counter == null) {
                    //if this is the first experiment in the study
                    counter = 1;
                }
                if(isStandardPrefix) {
                    this.setId("CSM" + counter);
                } else {
                    this.setId("a" + counter);
                }
                counterForNameGenerationCSMMap.put(studyName, counter + 1);
                break;
            }
            case VALIDATING_SIMULATION_MODEL:{
                Integer counter = counterForNameGenerationVSMMap.get(studyName);
                if(counter == null) {
                    //if this is the first experiment in the study
                    counter = 1;
                }
                if(isStandardPrefix) {
                    this.setId("VSM" + counter);
                } else {
                    this.setId("a" + counter);
                }
                counterForNameGenerationVSMMap.put(studyName, counter + 1);
                break;
            }
            case ANALYZING_SIMULATION_MODEL:{
                Integer counter = counterForNameGenerationASMMap.get(studyName);
                if(counter == null) {
                    //if this is the first experiment in the study
                    counter = 1;
                }
                if(isStandardPrefix) {
                    this.setId("ASM" + counter);
                } else {
                    this.setId("a" + counter);
                }
                counterForNameGenerationASMMap.put(studyName, counter + 1);
                break;
            }
        }
        this.activityType = activityType;
    }

    private boolean getIsStandardPrefix(String activityName) {
        Pattern p = Pattern.compile("^[ACV]SM*");
        Matcher m = p.matcher(activityName);
        if(m.find()) {
            return true;
        } else {
            return false;
        }
    }

    //id is set manually
    public ProvenanceActivity(String id, String studyName, ActivityType activityType) {
        super(id, studyName, ProvenanceNodeType.ACTIVITY);
        switch (activityType){
            //building simulation model activities
            case REFINING_SIMULATION_MODEL:
            case CREATING_SIMULATION_MODEL:
            case REIMPLEMENTING_SIMULATION_MODEL:
            case COMPOSING_SIMULATION_MODEL: {
                Integer counter = counterForNameGenerationBSMMap.get(studyName);
                if(counter == null) {
                    //if this is the first experiment in the study
                    counter = 1;
                }
                counterForNameGenerationBSMMap.put(studyName, counter + 1);
                break;
            }
            case CALIBRATING_SIMULATION_MODEL: {
                Integer counter = counterForNameGenerationCSMMap.get(studyName);
                if(counter == null) {
                    //if this is the first experiment in the study
                    counter = 1;
                }
                counterForNameGenerationCSMMap.put(studyName, counter + 1);
                break;
            }
            case VALIDATING_SIMULATION_MODEL:{
                Integer counter = counterForNameGenerationVSMMap.get(studyName);
                if(counter == null) {
                    //if this is the first experiment in the study
                    counter = 1;
                }
                counterForNameGenerationVSMMap.put(studyName, counter + 1);
                break;
            }
            case ANALYZING_SIMULATION_MODEL:{
                Integer counter = counterForNameGenerationASMMap.get(studyName);
                if(counter == null) {
                    //if this is the first experiment in the study
                    counter = 1;
                }
                counterForNameGenerationASMMap.put(studyName, counter + 1);
                break;
            }
        }
        this.activityType = activityType;
    }

    public void decrementCounter() {
        switch (this.activityType){
            //building simulation model activities
            case REFINING_SIMULATION_MODEL:
            case CREATING_SIMULATION_MODEL:
            case REIMPLEMENTING_SIMULATION_MODEL:
            case COMPOSING_SIMULATION_MODEL: {
                Integer counter = counterForNameGenerationBSMMap.get(this.getStudyName());
                counterForNameGenerationBSMMap.put(this.getStudyName(), counter - 1);
                break;
            }
            case CALIBRATING_SIMULATION_MODEL: {
                Integer counter = counterForNameGenerationCSMMap.get(this.getStudyName());
                counterForNameGenerationCSMMap.put(this.getStudyName(), counter - 1);
                break;
            }
            case VALIDATING_SIMULATION_MODEL:{
                Integer counter = counterForNameGenerationVSMMap.get(this.getStudyName());
                counterForNameGenerationVSMMap.put(this.getStudyName(), counter - 1);
                break;
            }
            case ANALYZING_SIMULATION_MODEL:{
                Integer counter = counterForNameGenerationASMMap.get(this.getStudyName());
                counterForNameGenerationASMMap.put(this.getStudyName(), counter - 1);
                break;
            }
        }
    }

    public ActivityType getActivityType() {
        return activityType;
    }

    public void setActivityType(ActivityType activityType) {
        this.activityType = activityType;
    }
}
