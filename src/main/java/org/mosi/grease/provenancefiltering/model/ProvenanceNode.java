package org.mosi.grease.provenancefiltering.model;

import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

import java.util.ArrayList;
import java.util.Observable;

public abstract class ProvenanceNode extends Observable {
    private ArrayList<ProvenanceNode> edgesToNodes;
    private String name;
    private String studyName;
    String description;
    private long dbId;
    private long creationTime; //this counts up for all nodes
    private ProvenanceNodeType type;
    private org.grease.provenance.model.graph.ProvenanceNode nodeForDrawing;

    public ProvenanceNode(String name, String studyName, ProvenanceNodeType type){
        this.name= name;
        this.studyName = studyName;
        this.type = type;
        edgesToNodes = new ArrayList();
    }

    public ProvenanceNodeType getType(){
        return type;
    }

    public void setType(ProvenanceNodeType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setId(String id) {
        this.name = id;
    }

    public long getDbId() {
        return dbId;
    }

    public void setStudyName(String studyName) {
        this.studyName = studyName;
    }

    public String getStudyName() {
        return studyName;
    }

    public void setDbId(long dbId) {
        this.dbId = dbId;
    }

    public long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(long creationTime) {
        this.creationTime = creationTime;
    }

    public void setNodeForDrawing(org.grease.provenance.model.graph.ProvenanceNode nodeForDrawing) {
        this.nodeForDrawing = nodeForDrawing;
    }

    public org.grease.provenance.model.graph.ProvenanceNode getNodeForDrawing() {
        return nodeForDrawing;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
