package org.mosi.grease.provenancefiltering.model;

import org.mosi.grease.provenancefiltering.controller.MainController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;
import java.util.Set;

public class ProvenanceGraph {
    private HashMap<ProvenanceNode, ArrayList<ProvenanceNode>> graph;
    private HashMap<Long, ProvenanceNode> provenanceNodeLookup;

    public ProvenanceGraph(MainController mainController){
        graph = new HashMap<>();
        provenanceNodeLookup = new HashMap<>();
    }

    public void addNode(ProvenanceNode node){
        if(!graph.containsKey(node)){
            graph.put(node, new ArrayList<>());
            provenanceNodeLookup.put(node.getDbId(),node);
        }
    }

    public void addDependency(ProvenanceNode fromNode, ProvenanceNode toNode){
        if(!graph.containsKey(fromNode)){
            ArrayList<ProvenanceNode> neighbours = new ArrayList<>();
            neighbours.add(toNode);
            graph.put(fromNode,neighbours);
        } else if(!graph.get(fromNode).contains(toNode)){
            graph.get(fromNode).add(toNode);
        }
        if(!graph.containsKey(toNode))
            this.addNode(toNode);
    }

    public Set<ProvenanceNode> getNodes(){
        return graph.keySet();
    }

    public ProvenanceNode getNodeById(Long id){
        ProvenanceNode node = provenanceNodeLookup.get(id);
        return node;
    }

}
