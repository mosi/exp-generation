package org.mosi.grease.provenancefiltering.model.entities.experiments;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.mosi.grease.provenancefiltering.model.ProvenanceEntity;
import org.mosi.grease.provenancefiltering.model.enums.ExperimentRole;
import org.mosi.grease.provenancefiltering.model.enums.ExperimentType;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Experiment extends ProvenanceEntity {
    private ExperimentRole role;
    private ExperimentType experimentType;
    private String reference;
    private String tool;

    private HashMap<String,Object> attributes;
    private boolean successful;
    private String modelName;
    private String modelPath;
    private String jsonPath;
    public JSONObject jsonObject;
    private boolean isRemote;
    private static Map<String, Integer> counterForNameGenerationMap = new HashMap<>();
    //private static int counterForNameGeneration = 1;

    //id generated automatically
    public Experiment(String studyName, ExperimentRole role, String experimentName) {
        super("temp", studyName, ProvenanceNodeType.EXPERIMENT);
        boolean isStandardPrefix = getIsStandardPrefix(experimentName);
        attributes = new HashMap<>();
        this.role = role;
        this.isRemote = false;
        //set correct id for the study
        Integer counter = counterForNameGenerationMap.get(studyName);
        if(counter == null) {
            //if this is the first experiment in the study
            counter = 1;
        }
        if(isStandardPrefix) {
            this.setId("SE" + counter);
        } else {
            this.setId("E" + counter);
        }
        counterForNameGenerationMap.put(studyName, counter + 1);
    }

    private boolean getIsStandardPrefix(String experimentName) {
        Pattern p = Pattern.compile("^SE*");
        Matcher m = p.matcher(experimentName);
        if(m.find()) {
            return true;
        } else {
            return false;
        }
    }

    //id set manually
    public Experiment(String id, String studyName, ExperimentRole role) {
        super(id, studyName, ProvenanceNodeType.EXPERIMENT);
        attributes = new HashMap<>();
        this.role = role;
        this.isRemote = false;
        //update counter for name generation
        Integer counter = counterForNameGenerationMap.get(studyName);
        if(counter == null) {
            //if this is the first experiment in the study
            counter = 1;
        }
        counterForNameGenerationMap.put(studyName, counter + 1);
    }

    public void decrementCounter() {
        Integer counter = counterForNameGenerationMap.get(this.getStudyName());
        counterForNameGenerationMap.put(this.getStudyName(), counter - 1);
    }

    public ExperimentRole getRole() {
        return role;
    }

    public void setRole(ExperimentRole role) {
        this.role = role;
    }

    public ExperimentType getExperimentType() {
        return experimentType;
    }

    public void setExperimentType(ExperimentType experimentType) {
        this.experimentType = experimentType;
    }

    public HashMap<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(HashMap<String, Object> attributes) {
        this.attributes = attributes;
    }

    public void addAttribute(String attributeType, Object value){
        attributes.put(attributeType, value);
    }

    public String getJsonPath() {
        return jsonPath;
    }

    public void setJsonPath(String json) {
        this.jsonPath = json;
    }

    private JSONObject loadTestJson(){
        String resource = "case-studies/experimentGenerationGUI/exampleJsons/des-example.json";

        // this is the path within the jar file
        InputStream input = this.getClass().getResourceAsStream("/resources/" + resource);
        if (input == null) {
            // this is how we load file within editor (eg eclipse)
            input = this.getClass().getClassLoader().getResourceAsStream(resource);
        }

        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.getFactory().disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);
        try {
            map = mapper.readValue(input, LinkedHashMap.class);
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        org.json.JSONObject schemaJson = new org.json.JSONObject(map);
        return schemaJson;

    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getModelPath() {
        return modelPath;
    }

    public void setModelPath(String modelPath) {
        this.modelPath = modelPath;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public void setJsonObject(JSONObject experimentJSON) {
        this.jsonObject = experimentJSON;
    }

    public JSONObject getJsonObject() {
        return jsonObject;
    }

    public boolean isRemote() {
        return isRemote;
    }

    public void setIsRemote(boolean isRemote) {
        this.isRemote = isRemote;
    }
}
