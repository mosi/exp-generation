package org.mosi.grease.provenancefiltering.model.entities;

import org.mosi.grease.provenancefiltering.model.ProvenanceEntity;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

public class Theory extends ProvenanceEntity {

    private String reference;

    private static int counterForNameGeneration = 1;

    public Theory(String studyName) {
        super("T" + counterForNameGeneration, studyName, ProvenanceNodeType.THEORY);
        counterForNameGeneration++;
    }

    public Theory(String id, String studyName) {
        super(id, studyName, ProvenanceNodeType.THEORY);
        counterForNameGeneration++;
    }

    public Theory(String id, String studyName, String description) {
        super(id, studyName, ProvenanceNodeType.THEORY);
        super.setDescription(description);
        counterForNameGeneration++;
    }

}
