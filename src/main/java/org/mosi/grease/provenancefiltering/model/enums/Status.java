package org.mosi.grease.provenancefiltering.model.enums;

public enum Status {
    SUCCESS, FAILURE, UNDEFINED;
}
