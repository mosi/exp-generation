package org.mosi.grease.provenancefiltering.model.enums;

public enum ExperimentType {
    SENSITIVITYANALYSIS, SIMULATIONOPTIMALIZATION, STOCHASTICALMODELCHECKING, PARAMETERSCAN
}
