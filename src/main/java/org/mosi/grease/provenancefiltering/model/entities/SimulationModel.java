package org.mosi.grease.provenancefiltering.model.entities;

import org.mosi.grease.provenancefiltering.model.ProvenanceEntity;
import org.mosi.grease.provenancefiltering.model.entities.experiments.ExperimentParameter;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;
import org.mosi.grease.provenancefiltering.model.enums.SimulationModelMilestone;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

public class SimulationModel extends ProvenanceEntity {

    private String tool;
    private String reference;

    private Boolean isValidated;
    private String modelSpecification;
    private SimulationModelMilestone simulationModelMilestone;
    private ArrayList<ExperimentParameter> parameters;
    private static int counterForNameGeneration = 1;


    public SimulationModel(String studyName) {
        super("SM" + counterForNameGeneration, studyName, ProvenanceNodeType.SIMULATIONMODEL);
        this.parameters = new ArrayList<>();
        counterForNameGeneration++;
        this.simulationModelMilestone = SimulationModelMilestone.NONE;
    }

    public SimulationModel(String id, String studyName) {
        super(id, studyName, ProvenanceNodeType.SIMULATIONMODEL);
        this.parameters = new ArrayList<>();
        counterForNameGeneration++;
        this.simulationModelMilestone = SimulationModelMilestone.NONE;
    }

    public SimulationModel(String id, String studyName, String tool,
                           String reference, String modelSpecification, Boolean isValidated) {
        super(id, studyName, ProvenanceNodeType.SIMULATIONMODEL);
        counterForNameGeneration++;
        this.simulationModelMilestone = SimulationModelMilestone.NONE;
        this.tool = tool;
        this.reference = reference;
        this.modelSpecification = modelSpecification;
        this.parameters = new ArrayList<>();
        this.isValidated = isValidated;
    }

    //For dummy generation in calibration
    public SimulationModel(String studyName, String tool,
                           String reference, String modelSpecification, Boolean isValidated) {
        super("SM" + counterForNameGeneration, studyName, ProvenanceNodeType.SIMULATIONMODEL);
        counterForNameGeneration++;
        this.simulationModelMilestone = SimulationModelMilestone.NONE;
        this.tool = tool;
        this.reference = reference;
        this.modelSpecification = modelSpecification;
        this.parameters = new ArrayList<>();
        this.isValidated = isValidated;
    }

    public void saveModelSpecificationAsFile(){
        File fi = new File("src/main/resources/case-studies/TOMACS/models/" + this.getName());
        try {
            // Create a file writer
            FileWriter wr = new FileWriter(fi, false);
            // Create buffered writer to write
            BufferedWriter w = new BufferedWriter(wr);
            // Write
            w.write(modelSpecification);

            w.flush();
            w.close();
        } catch (Exception evt) {
            evt.printStackTrace();
        }
    }

    public String getModelSpecification() {
        return modelSpecification;
    }

    public void setModelSpecification(String modelSpecification) {
        this.modelSpecification = modelSpecification;
    }

    public String getApplicationDomain() {
        return tool;
    }

    public void setApplicationDomain(String applicationDomain) {
        this.tool = applicationDomain;
    }

    public String getModelId() {
        return super.getName();
    }

    public String getModelPath() {
        return reference;
    }

    public void setModelPath(String modelPath) {
        this.reference = modelPath;
    }

    public ArrayList<ExperimentParameter> getParameters() {
        return parameters;
    }

    public void addParameter(ExperimentParameter parameter){
        parameters.add(parameter);
    }

    public SimulationModelMilestone getSimulationModelMilestone() {
        return simulationModelMilestone;
    }

    public void setSimulationModelMilestone(SimulationModelMilestone simulationModelMilestone) {
        this.simulationModelMilestone = simulationModelMilestone;
    }

    public String getTool() {
        return tool;
    }

    public void setValidated(Boolean isValidated) {
        this.isValidated = isValidated;
    }

    public Boolean isValidated() {
        return isValidated;
    }
}
