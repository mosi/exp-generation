package org.mosi.grease.provenancefiltering.model.enums;

public enum  ExperimentRole {
    VALIDATION, CALIBRATION, ANALYSIS, UNKNOWN
}
