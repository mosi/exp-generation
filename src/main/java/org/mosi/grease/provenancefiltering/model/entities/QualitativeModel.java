package org.mosi.grease.provenancefiltering.model.entities;

import org.mosi.grease.provenancefiltering.model.ProvenanceNode;
import org.mosi.grease.provenancefiltering.model.entities.experiments.ExperimentEntity;
import org.mosi.grease.provenancefiltering.model.entities.experiments.ExperimentParameter;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

import java.util.ArrayList;

public class QualitativeModel extends ProvenanceNode {
    private ArrayList<ExperimentParameter> parameters;
    private ArrayList<ExperimentEntity> entities;
    private static int counterForNameGeneration = 1;

    public QualitativeModel(String studyName) {
        super("QM" + counterForNameGeneration, studyName, ProvenanceNodeType.QUALITIVEMODEL);
        this.parameters = new ArrayList<>();
        this.entities = new ArrayList<>();
    }

    public QualitativeModel(String id, String studyName) {
        super(id, studyName, ProvenanceNodeType.QUALITIVEMODEL);
        parameters = new ArrayList<>();
        entities = new ArrayList<>();
        counterForNameGeneration++;
    }
    public QualitativeModel (String id, String studyName, ArrayList<ExperimentParameter> parameters,
                             ArrayList<ExperimentEntity> entities){
        super(id, studyName, ProvenanceNodeType.QUALITIVEMODEL);
        this.parameters = parameters;
        this.entities = entities;
        counterForNameGeneration++;
    }

    public void addParameter(ExperimentParameter parameter){
        parameters.add(parameter);
    }

    public ArrayList<ExperimentParameter> getParameters() {
        return parameters;
    }

    public void addEntity(ExperimentEntity entity){
        entities.add(entity);
    }

    public ArrayList<ExperimentEntity> getEntities() {
        return entities;
    }

    public void setParameters(ArrayList<ExperimentParameter> parameters) {
        this.parameters = parameters;
    }

    public void setEntities(ArrayList<ExperimentEntity> entities) {
        this.entities = entities;
    }

}
