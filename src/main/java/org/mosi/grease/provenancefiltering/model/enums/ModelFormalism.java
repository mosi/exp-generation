package org.mosi.grease.provenancefiltering.model.enums;

public enum ModelFormalism {
    ML_RULES, ML3
}
