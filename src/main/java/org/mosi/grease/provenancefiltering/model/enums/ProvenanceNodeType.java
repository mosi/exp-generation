package org.mosi.grease.provenancefiltering.model.enums;

public enum ProvenanceNodeType {
    DATA, EXPERIMENT,  ACTIVITY, SIMULATIONMODEL, ASSUMPTION,
    QUALITIVEMODEL, REQUIREMENT, RESEARCHQUESTION, THEORY, OTHER
}
