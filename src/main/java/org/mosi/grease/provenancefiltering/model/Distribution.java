package org.mosi.grease.provenancefiltering.model;

import org.mosi.grease.provenancefiltering.model.enums.DistributionType;

import java.util.HashMap;

public class Distribution {
    private DistributionType distributionType;
    private HashMap<String, Double> parameters;
    private Double beginOfRange;
    private Double endOfRange;

    public Distribution(DistributionType distributionType) {
        this.distributionType = distributionType;
        parameters = new HashMap<>();
    }

    public Distribution(DistributionType distributionType, Double beginOfRange, Double endOfRange) {
        this.distributionType = distributionType;
        this.beginOfRange = beginOfRange;
        this.endOfRange = endOfRange;
        parameters = new HashMap<>();
    }

    public void addParameter(String name, Double value){
        parameters.put(name, value);
    }

    public Double getParameter(String name){
        return parameters.get(name);
    }
}
