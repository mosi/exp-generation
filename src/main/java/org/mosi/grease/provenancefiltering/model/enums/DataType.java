package org.mosi.grease.provenancefiltering.model.enums;

public enum DataType {
    IN_VITRO, IN_VIVO, IN_SILICO, UNSPECIFIED;
}
