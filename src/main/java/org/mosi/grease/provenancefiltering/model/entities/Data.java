package org.mosi.grease.provenancefiltering.model.entities;

import org.mosi.grease.provenancefiltering.model.ProvenanceEntity;
import org.mosi.grease.provenancefiltering.model.enums.DataType;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;
import org.mosi.grease.provenancefiltering.model.enums.Status;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Data extends ProvenanceEntity {

    private DataType dataType;
    private String format;
    private Status status;
    private String reference;
    private static Map<String, Integer> counterForNameGenerationWDMap = new HashMap<>(); // WD = invivo + invitro
    private static Map<String, Integer> counterForNameGenerationSDMap = new HashMap<>(); // SD
    private static Map<String, Integer> counterForNameGenerationDMap = new HashMap<>(); // unspecified data

    //constructor for generating simulation data automatically
    public Data(String studyName, DataType dataType, String generatedBy, Status status) {
        super("temp", studyName, ProvenanceNodeType.DATA);
        boolean isStandardPrefix = getIsStandardPrefix(generatedBy);
        this.dataType = dataType;
        this.status = status;
        if(dataType.equals(DataType.IN_SILICO)) {
            counterForNameGenerationSDMap.putIfAbsent(studyName, 1);
            if(isStandardPrefix) {
                this.setId("SD" + counterForNameGenerationSDMap.get(studyName));
            } else {
                this.setId("S" + counterForNameGenerationSDMap.get(studyName));
            }
        }
        this.incrementCounter();
    }

    private boolean getIsStandardPrefix(String experimentName) {
        Pattern p = Pattern.compile("^SE*");
        Matcher m = p.matcher(experimentName);
        if(m.find()) {
            return true;
        } else {
            return false;
        }
    }

    public Data(String name, String studyName, DataType dataType, Status status) {
        super(name, studyName, ProvenanceNodeType.DATA);
        this.dataType = dataType;
        this.status = status;
        this.incrementCounter();
    }

    public Data(String id, String studyName, DataType dataType, String format, String description, Status status) {
        super(id, studyName, ProvenanceNodeType.DATA);
        this.dataType = dataType;
        this.format = format;
        super.setDescription(description);
        this.status = status;
        this.incrementCounter();
    }

    public void incrementCounter() {
        switch (dataType) {
            case IN_VITRO: {
                Integer counter = counterForNameGenerationWDMap.get(this.getStudyName());
                if (counter == null) {
                    //if this is the first experiment in the study
                    counter = 1;
                }
                counterForNameGenerationWDMap.put(this.getStudyName(), counter + 1);
                break;
            }
            case IN_VIVO: {
                Integer counter = counterForNameGenerationWDMap.get(this.getStudyName());
                if (counter == null) {
                    //if this is the first experiment in the study
                    counter = 1;
                }
                counterForNameGenerationWDMap.put(this.getStudyName(), counter + 1);
                break;
            }
            case IN_SILICO: {
                Integer counter = counterForNameGenerationSDMap.get(this.getStudyName());
                if (counter == null) {
                    //if this is the first experiment in the study
                    counter = 1;
                }
                counterForNameGenerationSDMap.put(this.getStudyName(), counter + 1);
                break;
            }
            case UNSPECIFIED: {
                Integer counter = counterForNameGenerationDMap.get(this.getStudyName());
                if (counter == null) {
                    //if this is the first experiment in the study
                    counter = 1;
                }
                counterForNameGenerationDMap.put(this.getStudyName(), counter + 1);
                break;
            }
        }
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public Status getStatus() {
        return status;
    }

    public String getReference()  { return reference; }

    public void setReference(String reference)  { this.reference = reference; }
}
