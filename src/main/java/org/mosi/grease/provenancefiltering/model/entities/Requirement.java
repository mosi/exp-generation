package org.mosi.grease.provenancefiltering.model.entities;

import org.mosi.grease.provenancefiltering.model.ProvenanceEntity;
import org.mosi.grease.provenancefiltering.model.enums.HypothesisStatus;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;
import org.mosi.grease.provenancefiltering.model.enums.RequirementRole;

public class Requirement extends ProvenanceEntity {

    private static int counterForNameGeneration = 1;

    public Requirement(String studyName) {
        super("R"+counterForNameGeneration, studyName, ProvenanceNodeType.REQUIREMENT);
        counterForNameGeneration++;
    }

    public Requirement(String id, String studyName) {
        super(id, studyName, ProvenanceNodeType.REQUIREMENT);
        counterForNameGeneration++;
    }

    public Requirement(String id, String studyName, String description) {
        super(id, studyName, ProvenanceNodeType.REQUIREMENT);
        super.setDescription(description);
        counterForNameGeneration++;
    }

}
