package org.mosi.grease.provenancefiltering.model.entities.experiments;

import java.util.ArrayList;

public class ExperimentParameter {
    String name;
    Double value;
    Double minimum;
    Double maximum;
    String unit;
    private ArrayList<String> ontologyTags;

    public ExperimentParameter(String name, Double value) {
        this.name = name;
        this.value = value;
    }

    public ExperimentParameter(String name, Double value, String unit) {
        this.name = name;
        this.value = value;
        this.unit = unit;
    }

    public ExperimentParameter(String name, Double value, String unit, Double minimum, Double maximum) {
        this.name = name;
        this.value = value;
        this.unit = unit;
        this.minimum = minimum;
        this.maximum = maximum;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public void setMinimum(Double minimum) {
        this.minimum = minimum;
    }

    public Double getMinimum() {
        return minimum;
    }

    public void setMaximum(Double maximum) {
        this.maximum = maximum;
    }

    public Double getMaximum() {
        return maximum;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public ArrayList<String> getOntologyTags() {
        return ontologyTags;
    }

    public void addToOntologyTags(String ontologyTag){
        this.ontologyTags.add(ontologyTag);
    }
}
