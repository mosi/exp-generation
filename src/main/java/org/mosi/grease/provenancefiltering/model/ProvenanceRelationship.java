package org.mosi.grease.provenancefiltering.model;

public class ProvenanceRelationship {
    ProvenanceNode source;
    ProvenanceNode target;

    public ProvenanceRelationship(ProvenanceNode source, ProvenanceNode target) {
        this.source = source;
        this.target = target;
    }

    public ProvenanceNode getSource() {
        return source;
    }

    public ProvenanceNode getTarget() {
        return target;
    }
}
