package org.mosi.grease.provenancefiltering.model.enums;

public enum SimulationModelMilestone {
    NONE, CALIBRATED, VALIDATED
}
