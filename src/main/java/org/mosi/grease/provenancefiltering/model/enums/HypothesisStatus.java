package org.mosi.grease.provenancefiltering.model.enums;

public enum HypothesisStatus {
HYPOTHESIS_SATISFIED, HYPOTHESIS_NOT_SATISFIED;
}
