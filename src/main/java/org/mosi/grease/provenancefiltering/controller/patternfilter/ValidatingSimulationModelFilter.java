package org.mosi.grease.provenancefiltering.controller.patternfilter;

import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.controller.patternfilter.conditions.Condition;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.ProvenanceNode;
import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

import java.util.List;
import java.util.Map;

public class ValidatingSimulationModelFilter extends AbstractFilter {

    String validatingSimulationModelPattern_ToGetNewestActivity = "MATCH (d) <--(a:ACTIVITY) <--(e:EXPERIMENT) \n" +
            "WITH d,a,e \n" +
            "MATCH (:SIMULATIONMODEL)<--(a) <--(:DATA) \n " +
            "WHERE (d:DATA OR d:REQUIREMENT)" +
            "AND not (a) <--(:SIMULATIONMODEL)" +
            "return a";
    String validatingSimulationModelPattern_ToBeReused_Part1 = "MATCH (d) <--(a:ACTIVITY) <--(e:EXPERIMENT) \n";
    String validatingSimulationModelPattern_ToBeReused_Part2 =
            "WITH d,a,e \n" +
            "MATCH (:SIMULATIONMODEL)<--(a) <--(:DATA) \n " +
            "WHERE (d:DATA OR d:REQUIREMENT) \n" +
            "AND not (a) <--(:SIMULATIONMODEL)\n" +
            "return a, collect(d), collect(e)";

    public ValidatingSimulationModelFilter(MainController mainController) {
        super(mainController);
    }

    @Override
    public List<Map<ProvenanceNodeType, List<ProvenanceNode>>> matchExperiments(ProvenanceActivity triggeringActivity,
                                                                                SimulationModel simulationModelGeneratedByActivity,
                                                                                SimulationModel simulationModelUsedByActivity,
                                                                                List<Condition> modelConditions,
                                                                                List<Condition> experimentConditions) {
        pattern_ToBeReused_Part1 = validatingSimulationModelPattern_ToBeReused_Part1;
        pattern_ToBeReused_Part2 = validatingSimulationModelPattern_ToBeReused_Part2;
        return super.matchExperiments(triggeringActivity, simulationModelGeneratedByActivity, simulationModelUsedByActivity,
                modelConditions, experimentConditions);
    }

    @Override
    public ProvenanceActivity lookForNewestActivity(){
        Pattern_ToGetNewestActivity = validatingSimulationModelPattern_ToGetNewestActivity;
        return super.lookForNewestActivity();
    }

    //Validating Simulation Model does not produce a Simulation Model
    @Override
    public SimulationModel getSimulationModelGeneratedByTriggerActivity(ProvenanceActivity activity) {
        return null;
    }

}

