package org.mosi.grease.provenancefiltering.controller.patternfilter;

import org.jetbrains.annotations.Nullable;
import org.mosi.grease.provenancefiltering.controller.DatabaseController;
import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.controller.ProvModelController;
import org.mosi.grease.provenancefiltering.controller.patternfilter.conditions.Condition;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.ProvenanceNode;
import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;
import org.neo4j.graphdb.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class AbstractFilter {
    MainController mainController;
    ProvModelController provModelController;
    DatabaseController databaseController;
    GraphDatabaseService graphDB;
    String Pattern_ToGetGeneratedSimulationModel;
    String Pattern_ToGetGeneratedSimulationModel_part2;
    String Pattern_ToGetUsedSimulationModel;
    String Pattern_ToGetUsedSimulationModel_part2;
    String Pattern_ToGetNewestActivity;
    String pattern_ToBeReused_Part1;
    String pattern_ToBeReused_Part2;
    //Map<String, Object> newestPattern;

    public AbstractFilter(MainController mainController) {
        this.mainController = mainController;
        this.provModelController = mainController.getModelController();
        databaseController = mainController.getDatabaseController();
        graphDB = databaseController.getGraphDb();
    }

    public List<Map<ProvenanceNodeType, List<ProvenanceNode>>> getInputs(String activityName, String studyName) {

        //database transaction
        try (Transaction transaction = graphDB.beginTx()) {

            //find all matches for given experiment pattern
            Result result = graphDB.execute(
                    "MATCH (i)<--(a:ACTIVITY) \n" +
                            "WHERE a.id = '" + activityName + "' AND a.studyName = '" + studyName + "' \n" +
                            "return collect(i)"
            );

            //collect all the provenance nodes
            List<Map<ProvenanceNodeType, List<ProvenanceNode>>> allActivitiesWithNodesAttached = new ArrayList<>();
            //iterate over pattern occurances
            while (result.hasNext()) {
                Map<String, Object> patternOccurrence = result.next();

                //initialize map to store the nodes
                Map<ProvenanceNodeType, List<ProvenanceNode>> inputNodes = new HashMap<>();
                List<ProvenanceNode> other = new ArrayList<>();
                List<ProvenanceNode> requirements = new ArrayList<>();
                List<ProvenanceNode> data = new ArrayList<>();

                //get pattern nodes
                for (Object o : patternOccurrence.values()) {
                    if (o instanceof ArrayList) {
                        //an array of nodes
                        for (Object o2 : (ArrayList<Object>) o) {
                            Node node = (Node) o2;
                            ProvenanceNode provenanceNode = getProvenanceNodeFromNode(node);
                            //arrange by node type
                            if (node.hasLabel(Label.label(ProvenanceNodeType.OTHER.toString()))) {
                                other.add(provenanceNode);
                            } else if (node.hasLabel(Label.label(ProvenanceNodeType.REQUIREMENT.toString()))) {
                                requirements.add(provenanceNode);
                            } else if (node.hasLabel(Label.label(ProvenanceNodeType.DATA.toString()))) {
                                data.add(provenanceNode);
                            }
                        }
                    } else {
                        //a single node
                        Node node = (Node) o;
                        ProvenanceNode provenanceNode = getProvenanceNodeFromNode(node);
                        //arrange by node type
                        if (node.hasLabel(Label.label(ProvenanceNodeType.OTHER.toString()))) {
                            other.add(provenanceNode);
                        } else if (node.hasLabel(Label.label(ProvenanceNodeType.REQUIREMENT.toString()))) {
                            requirements.add(provenanceNode);
                        } else if (node.hasLabel(Label.label(ProvenanceNodeType.DATA.toString()))) {
                            data.add(provenanceNode);
                        }
                    }
                }

                //build map for current matching
                if (!other.isEmpty())
                    inputNodes.put(ProvenanceNodeType.OTHER, other);
                if (!data.isEmpty())
                    inputNodes.put(ProvenanceNodeType.DATA, data);
                if (!requirements.isEmpty())
                    inputNodes.put(ProvenanceNodeType.REQUIREMENT, requirements);

                //add this pattern match to overall result map
                allActivitiesWithNodesAttached.add(inputNodes);
                //Debug: System.out.println(provenanceNodesAttachedToOneActivity.toString());
            }

            transaction.success();
            transaction.close();

            return allActivitiesWithNodesAttached;
        }
    }

    public List<Map<ProvenanceNodeType, List<ProvenanceNode>>> matchExperiments(
            ProvenanceActivity activity, SimulationModel simulationModelGeneratedByActivity,
            SimulationModel simulationModelUsedByActivity,
            List<Condition> modelConditions, List<Condition> experimentConditions) {

        //database transaction
        try (Transaction transaction = graphDB.beginTx()) {

            //find all matches for given experiment pattern
            Result result = graphDB.execute(buildMatchingQuery(pattern_ToBeReused_Part1, pattern_ToBeReused_Part2,
                    simulationModelGeneratedByActivity, simulationModelUsedByActivity, modelConditions, experimentConditions));


            //collect all the provenance nodes for all the activities
            List<Map<ProvenanceNodeType, List<ProvenanceNode>>> allActivitiesWithNodesAttached = new ArrayList<>();
            //iterate over pattern occurances
            while (result.hasNext()) {
                Map<String, Object> patternOccurrence = result.next();

                //initialize map to store the nodes
                Map<ProvenanceNodeType, List<ProvenanceNode>> provenanceNodesAttachedToOneActivity = new HashMap<>();
                List<ProvenanceNode> simulationModels = new ArrayList<>();
                List<ProvenanceNode> requirements = new ArrayList<>();
                List<ProvenanceNode> experiments_ingoing = new ArrayList<>();
                List<ProvenanceNode> experiments_outgoing = new ArrayList<>();
                List<ProvenanceNode> data = new ArrayList<>();
                List<ProvenanceNode> activities = new ArrayList<>();

                //todo ingoing and outgoing experiments
                //get pattern nodes
                for (Object o : patternOccurrence.values()) {
                    if (o instanceof ArrayList) {
                        //an array of nodes
                        for (Object o2 : (ArrayList<Object>) o) {
                            Node node = (Node) o2;
                            ProvenanceNode provenanceNode = getProvenanceNodeFromNode(node);
                            //arrange by node type
                            if (node.hasLabel(Label.label(ProvenanceNodeType.ACTIVITY.toString()))) {
                                activities.add(provenanceNode);
                            } else if (node.hasLabel(Label.label(ProvenanceNodeType.SIMULATIONMODEL.toString()))) {
                                simulationModels.add(provenanceNode);
                            } else if (node.hasLabel(Label.label(ProvenanceNodeType.REQUIREMENT.toString()))) {
                                requirements.add(provenanceNode);
                            } else if (node.hasLabel(Label.label(ProvenanceNodeType.EXPERIMENT.toString()))) {
                                experiments_outgoing.add(provenanceNode);
                            } else if (node.hasLabel(Label.label(ProvenanceNodeType.DATA.toString()))) {
                                data.add(provenanceNode);
                            }
                        }
                    } else {
                        //a single node
                        Node node = (Node) o;
                        ProvenanceNode provenanceNode = getProvenanceNodeFromNode(node);
                        //arrange by node type
                        if (node.hasLabel(Label.label(ProvenanceNodeType.ACTIVITY.toString()))) {
                            activities.add(provenanceNode);
                        } else if (node.hasLabel(Label.label(ProvenanceNodeType.SIMULATIONMODEL.toString()))) {
                            simulationModels.add(provenanceNode);
                        } else if (node.hasLabel(Label.label(ProvenanceNodeType.REQUIREMENT.toString()))) {
                            requirements.add(provenanceNode);
                        } else if (node.hasLabel(Label.label(ProvenanceNodeType.EXPERIMENT.toString()))) {
                            experiments_outgoing.add(provenanceNode);
                        } else if (node.hasLabel(Label.label(ProvenanceNodeType.DATA.toString()))) {
                            data.add(provenanceNode);
                        }
                    }
                }

                //build map for current matching
                provenanceNodesAttachedToOneActivity.put(ProvenanceNodeType.ACTIVITY, activities);
                if (!simulationModels.isEmpty())
                    provenanceNodesAttachedToOneActivity.put(ProvenanceNodeType.SIMULATIONMODEL, simulationModels);
                if (!data.isEmpty())
                    provenanceNodesAttachedToOneActivity.put(ProvenanceNodeType.DATA, data);
                if (!data.isEmpty())
                    provenanceNodesAttachedToOneActivity.put(ProvenanceNodeType.REQUIREMENT, requirements);
                if (!experiments_outgoing.isEmpty())
                    provenanceNodesAttachedToOneActivity.put(ProvenanceNodeType.EXPERIMENT, experiments_outgoing);

                //add this pattern match to overall result map
                allActivitiesWithNodesAttached.add(provenanceNodesAttachedToOneActivity);
                //Debug: System.out.println(provenanceNodesAttachedToOneActivity.toString());
            }

            transaction.success();
            transaction.close();

            return allActivitiesWithNodesAttached;
        }
    }


    public SimulationModel getSimulationModelGeneratedByTriggerActivity(ProvenanceActivity activity){
        try (Transaction transaction = graphDB.beginTx()){
            String query = Pattern_ToGetGeneratedSimulationModel + activity.getName() + "' " +
                    Pattern_ToGetGeneratedSimulationModel_part2 + activity.getStudyName() + "' " + "return outs";
            //debug:
            //System.out.println(query);
            Result result = graphDB.execute(query);
            if(result.hasNext()){
                Node simulationModelNode = (Node) result.next().values().toArray()[0]; //there is always exactly one simulation model
                transaction.success();
                transaction.close();
                return (SimulationModel) getProvenanceNodeFromNode(simulationModelNode);
            } else {
                transaction.success();
                transaction.close();
                return null;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public SimulationModel getSimulationModelUsedByTriggerActivity(ProvenanceActivity activity){
        try (Transaction transaction = graphDB.beginTx()){
            String query = Pattern_ToGetUsedSimulationModel + activity.getName() + "' " +
                    Pattern_ToGetUsedSimulationModel_part2 + activity.getStudyName() + "' " + "return outs";
            //debug:
            //System.out.println(query);
            Result result = graphDB.execute(query);
            if(result.hasNext()){
                Node simulationModelNode = (Node) result.next().values().toArray()[0]; //there is always exactly one simulation model
                transaction.success();
                transaction.close();
                return (SimulationModel) getProvenanceNodeFromNode(simulationModelNode);
            } else {
                transaction.success();
                transaction.close();
                return null;
            }
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    ProvenanceNode getProvenanceNodeFromNode(Node node){
        return provModelController.getGraph().getNodeById(node.getId());
    }

    public ProvenanceActivity lookForNewestActivity(){
        try(Transaction transaction = graphDB.beginTx()){
            List<String> validationId = new ArrayList();
            Result result = graphDB.execute(Pattern_ToGetNewestActivity);

            ProvenanceActivity latestActivity = getLatestProvenanceActivity(validationId, result);
            transaction.success();
            return latestActivity;

        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Nullable
    ProvenanceActivity getLatestProvenanceActivity(List<String> validationId, Result result) {
        long numberOfLatestActivity = -1;
        ProvenanceActivity latestActivity = null;
        while (result.hasNext()) {
            boolean updatedNewestActivity = false;
            Map<String, Object> patternOccurrence = result.next();
            for (Object o : patternOccurrence.values()) {
                try {
                    Node node = (Node) o;
                    String nodeId = node.getProperty("id").toString();
                    if (!validationId.contains(nodeId) && node.hasLabel(Label.label(ProvenanceNodeType.ACTIVITY.toString()))) {
                        ProvenanceActivity provenanceNode = (ProvenanceActivity) getProvenanceNodeFromNode(node);
                        validationId.add(nodeId);
                        if(provenanceNode.getCreationTime()> numberOfLatestActivity){
                            latestActivity = provenanceNode;
                            updatedNewestActivity = true;
                        }
                    }
                }catch (ClassCastException e){
                    // ignore Lists
                }
            }
            if(updatedNewestActivity){
//                newestPattern = patternOccurrence;
            }
        }
        return latestActivity;
    }

    String buildMatchingQuery(String pattern_part_1, String pattern_part_2,
                              SimulationModel modelGeneratedByActivity,
                              SimulationModel modelUsedByActivity,
                              List<Condition> modelConditions,
                              List<Condition> experimentConditions) {

        StringBuilder builder = new StringBuilder();
        builder.append(pattern_part_1);

        //experiment conditions
        int i = 0;
        for(Condition c : experimentConditions) {
            builder.append(" AND ");
            builder.append(c.getQueryString(modelUsedByActivity));
        }
        builder.append("\n");

        //model conditions
        i = 0;
        for(Condition c : modelConditions) {
            builder.append(" AND ");
            builder.append(c.getQueryString(modelUsedByActivity));
        }
        builder.append("\n");

        builder.append(pattern_part_2);

        //debug
        //System.out.println(builder.toString());

        return builder.toString();
    }

    List<ProvenanceActivity> applyConditionFilter(Result result) {
        List<ProvenanceActivity> activityList = new ArrayList();
        while (result.hasNext()) {
            //get the next match
            Map<String, Object> patternOccurrence = result.next();
            for (Object o : patternOccurrence.values()) {
                try {
                    Node node = (Node) o;
                    String nodeId = node.getProperty("id").toString();
                    //add activity IDs
                    if (node.hasLabel(Label.label(ProvenanceNodeType.ACTIVITY.toString()))) {
                        ProvenanceActivity provenanceNode = (ProvenanceActivity) getProvenanceNodeFromNode(node);
                        if (activityList.contains(provenanceNode)) {
                            //activityList.add(nodeId);
                        }
                    }
                }catch (ClassCastException e){
                    // ignore Lists
                }
            }
        }
        return activityList;
    }
}
