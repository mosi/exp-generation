package org.mosi.grease.provenancefiltering.controller;

import org.json.JSONArray;
import org.json.JSONObject;
import org.mosi.grease.provenancefiltering.model.entities.QualitativeModel;
import org.mosi.grease.provenancefiltering.model.entities.experiments.ExperimentEntity;
import org.mosi.grease.provenancefiltering.model.entities.experiments.ExperimentParameter;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Result;
import org.neo4j.graphdb.Transaction;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.stream.Collectors;

public class AdaptionController {
    ProvModelController provModelController;
    MainController mainController;

    public AdaptionController(MainController mainController) {
        this.mainController = mainController;
        this.provModelController = mainController.getModelController();
    }

    public QualitativeModel filterForLatestQualitativeModelInStudy(String studyName) {
        GraphDatabaseService graphDb = mainController.getDatabaseController().graphDb;
        try(Transaction transaction = graphDb.beginTx()){
            Result allQualitativeModels = graphDb.execute("MATCH (qm:QUALITIVEMODEL) RETURN qm");
            long numberOfLatestQualitativeModel = -1;
            QualitativeModel latestQualitativeModel = null;
            while (allQualitativeModels.hasNext()) {
                for (Object o : allQualitativeModels.next().values()) {
                    Node node = (Node) o;

                    QualitativeModel provenanceNode = (QualitativeModel) provModelController.getGraph().getNodeById(node.getId());

                    if(provenanceNode.getStudyName().equals(studyName) &&
                            provenanceNode.getCreationTime() > numberOfLatestQualitativeModel){
                        latestQualitativeModel = provenanceNode;
                        numberOfLatestQualitativeModel = provenanceNode.getCreationTime();
                    }
                }
            }
            transaction.success();
            return latestQualitativeModel;
        } catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public void updateModel(JSONObject experimentJson, String modelPath, String nameOfLatestStudy) {
        //set new model path
        JSONObject modelObject = experimentJson.getJSONObject("model");
        JSONObject modelFileObject = modelObject.getJSONObject("modelFile");
        modelFileObject.put("reference", "models" + "/" + modelPath);

        //update parameter names
        QualitativeModel latestQualitativeModel = filterForLatestQualitativeModelInStudy(nameOfLatestStudy);
        ArrayList<ExperimentParameter> parameters = latestQualitativeModel.getParameters();
        if(parameters != null) {
            JSONObject configurationObject = new JSONObject();
            JSONArray parameterNamesArray = new JSONArray();
            JSONArray parameterValuesArray = new JSONArray();
            for(ExperimentParameter parameter : parameters) {
                parameterNamesArray.put(parameter.getName());
                parameterValuesArray.put(parameter.getValue());
            }
            configurationObject.put("parameterName", parameterNamesArray);
            configurationObject.put("parameterValue", parameterValuesArray);
            modelObject.put("configuration", configurationObject);
        }

    }

    public void updateObservation(JSONObject experimentJSON, String nameOfOldStudy, String nameOfLatestStudy) {
        JSONObject observationObject = experimentJSON.getJSONObject("observation");

        //update output variables based on qualitative model (ontology)
        JSONObject observables = observationObject.getJSONObject("observables");
        JSONArray expressionArray = observables.getJSONArray("observationExpression");
        JSONArray aliaArray = observables.getJSONArray("observationAlias");
        QualitativeModel oldQualitativeModel = filterForLatestQualitativeModelInStudy(nameOfOldStudy);
        QualitativeModel latestQualitativeModel = filterForLatestQualitativeModelInStudy(nameOfLatestStudy);
        ArrayList<ExperimentEntity> oldEntities = oldQualitativeModel.getEntities();
        ArrayList<ExperimentEntity> latestEntities = latestQualitativeModel.getEntities();
        Iterator<Object> expressionIterator = expressionArray.iterator();
        int index = 0;
        while(expressionIterator.hasNext()) {
            String expression = (String) expressionIterator.next();
            //get ontology tag of old entity
            String tagOfOldEntity = oldEntities.stream().filter(e ->
                    e.getName().equals(expression)).collect(Collectors.toList()).get(0).getOntologyTag();
            //match tag with latest entities
            ExperimentEntity newEntity = latestEntities.stream().filter(e ->
                    e.getOntologyTag().equals(tagOfOldEntity)).collect(Collectors.toList()).get(0);
            //put name and alias of matching latest entity into arrays
            expressionArray.put(index, newEntity.getName());
            aliaArray.put(index, newEntity.getAlias());
            index++;
        }

        //update observation times
        JSONObject observationTime = observationObject.getJSONObject("observationTime");
        observationTime.put("observationRangeStart", 0);
        observationTime.put("observationRangeEnd", 960);
        observationTime.put("observationRangeInterval", 6);

        //update output format
        observationObject.put("outputFormat", "CSV");

    }

    public void updateSimulation(JSONObject experimentJSON) {
        //set default replication number
        JSONObject simulationObject = experimentJSON.getJSONObject("simulation");
        simulationObject.put("replications", 10);

        //rename simulator
        simulationObject.put("simulator", "SSA");

        //transform stop time
        JSONObject stopConditionObject = simulationObject.getJSONObject("stopCondition");
        double stopTime = stopConditionObject.getDouble("stopTime");
        stopConditionObject.put("stopTime", 960);
    }
}
