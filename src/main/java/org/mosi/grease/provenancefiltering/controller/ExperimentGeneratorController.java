package org.mosi.grease.provenancefiltering.controller;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONObject;
import org.mosi.grease.experimentgeneration.model.Domain;
import org.mosi.grease.experimentgeneration.model.ExperimentDesign;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;
import org.mosi.grease.provenancefiltering.model.entities.experiments.Experiment;
import org.mosi.grease.provenancefiltering.model.enums.ExperimentRole;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

public class ExperimentGeneratorController {
    MainController mainController;
    ProvModelController provModelController;
    AdaptionController adaptionController;

    public ExperimentGeneratorController(MainController mainController) {
        this.mainController = mainController;
        this.provModelController = mainController.getModelController();
        this.adaptionController = mainController.getAdaptionController();
    }

    public void editExperimentsWithGUI(List<Experiment> inputExperiment, ProvenanceActivity createdActivity){
        EditJsonExperimentWithGuiController editJsonExperimentWithGuiController = mainController.getEditJsonExperimentController();
        List<JSONObject> inputExperimentsJsons = new ArrayList<>();
        List<Domain> inputExperimentDomains= new ArrayList<>();
        List<ExperimentDesign> inputExperimentTypes = new ArrayList<>();
        for(Experiment experiment: inputExperiment){
            JSONObject experimentJson = new JSONObject(experiment.getJsonPath());
            inputExperimentsJsons.add(experimentJson);
            Domain experimentDomain = mainController.getExperimentJsonController().inferExperimentDomainFromExperimentJason(experimentJson);
            inputExperimentDomains.add(experimentDomain);
            ExperimentDesign experimentType = mainController.getExperimentJsonController().inferExperimentTypeFromExperimentJason(experimentJson);
            inputExperimentTypes.add(experimentType);
        }
        editJsonExperimentWithGuiController.setCreatedActivity(createdActivity);
        editJsonExperimentWithGuiController.startExperimentEditor(inputExperimentsJsons, inputExperimentDomains, inputExperimentTypes);
    }

    public Experiment generateNewExperimentEntity(Experiment oldExperiment, SimulationModel model,
                                                   ProvenanceActivity newActivity) {
        //get experiment role
        ExperimentRole role = ExperimentRole.UNKNOWN;
        switch (newActivity.getActivityType()) {
            case CALIBRATING_SIMULATION_MODEL: {
                role = ExperimentRole.CALIBRATION;
                break;
            }
            case VALIDATING_SIMULATION_MODEL: {
                role = ExperimentRole.VALIDATION;
                break;
            }
            case ANALYZING_SIMULATION_MODEL: {
                role = ExperimentRole.ANALYSIS;
                break;
            }
        }

        //set general info, id is generated
        Experiment generatedExperiment = new Experiment(model.getStudyName(), role, oldExperiment.getName());
        generatedExperiment.setExperimentType(oldExperiment.getExperimentType());
        generatedExperiment.setReference(oldExperiment.getReference());
        generatedExperiment.setDescription(oldExperiment.getName() + " reused");
        generatedExperiment.setAttributes(oldExperiment.getAttributes());
        generatedExperiment.setModelPath(model.getModelPath());
        generatedExperiment.setIsRemote(oldExperiment.isRemote());

        return generatedExperiment;
    }

    public Experiment updateExperimentBasedOnModel(Experiment generatedExperiment, Experiment oldExperiment, SimulationModel model,
                                                   ProvenanceActivity newActivity) {
        //parse JSON
        JSONObject experimentJSON = null;
        if(oldExperiment.getJsonPath() != null) {
            //json already available
            experimentJSON = new JSONObject(oldExperiment.getJsonPath());
        } else if(oldExperiment.getReference() != null) {
            //json needs to be parsed from tool-specific specification
            experimentJSON = mainController.getExperimentJsonController().parseSpecification(oldExperiment.getReference());
        }

        //adapt, validate and save json
        if(experimentJSON != null) {

            //update model
            adaptionController.updateModel(experimentJSON, FilenameUtils.getName(model.getModelPath()),
                    generatedExperiment.getStudyName());

            //update simulation
            adaptionController.updateSimulation(experimentJSON);

            //update observation
            adaptionController.updateObservation(experimentJSON, oldExperiment.getStudyName(),
                    generatedExperiment.getStudyName());

            //validate, save and return
            JSONObject schema = mainController.getExperimentJsonController().getJsonForDomain(Domain.STOCHASTIC_DISCRETE_EVENT);
            if(mainController.getMainExperimentController().getParsingController().schemaCheckExperimentParameter(
                    schema, experimentJSON)) {
                String jsonPath = "src/main/resources/case-studies/TOMACS/experiments/" + generatedExperiment.getName() + ".json";
                generatedExperiment.setJsonPath(jsonPath);
                generatedExperiment.setJsonObject(experimentJSON);
                File jsonFile = new File(jsonPath);
                mainController.getExperimentJsonController().saveJsonObject(experimentJSON, jsonFile);
                return generatedExperiment;
            }
        } else {
            //json conversion not necessary
        }

        return generatedExperiment;
    }

    public void saveToFile(File file, String content) {
        try {
            // Create a file writer
            FileWriter wr = new FileWriter(file, false);
            // Create buffered writer to write
            BufferedWriter w = new BufferedWriter(wr);
            // Write
            w.write(content);

            w.flush();
            w.close();
        } catch (Exception evt) {
            evt.printStackTrace();
        }
    }
}
