package org.mosi.grease.provenancefiltering.controller.patternfilter.conditions;

import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;

public class AreStudiesNotEqualCondition extends Condition {

    //query if the model is validated
    String subquery = "not m.studyName = '";
    String subquery2 = "'";

    @Override
    public String getQueryString(SimulationModel modelUsedByActivity) {
        return subquery + modelUsedByActivity.getStudyName() + subquery2;
    }
}
