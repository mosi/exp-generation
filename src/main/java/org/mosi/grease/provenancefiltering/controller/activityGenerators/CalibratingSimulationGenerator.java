package org.mosi.grease.provenancefiltering.controller.activityGenerators;

import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.ProvenanceCommit;
import org.mosi.grease.provenancefiltering.model.ProvenanceNode;
import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;
import org.mosi.grease.provenancefiltering.model.enums.ActivityType;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

import java.util.List;
import java.util.Map;

public class CalibratingSimulationGenerator extends AbstractGenerator{
    public CalibratingSimulationGenerator(MainController mainController) {
        super(mainController);
    }

    @Override
    public boolean generate(ProvenanceCommit pc, Map<ProvenanceNodeType, List<ProvenanceNode>> pattern, ProvenanceNode provenanceModelNode,
                            List<Map<ProvenanceNodeType, List<ProvenanceNode>>> inputList) {
        SimulationModel simulationModel = (SimulationModel) provenanceModelNode;

        //create and add new activity:
        ProvenanceActivity calibratingModelActivity = new ProvenanceActivity(simulationModel.getStudyName(), ActivityType.CALIBRATING_SIMULATION_MODEL,
                pattern.get(ProvenanceNodeType.ACTIVITY).get(0).getName());

        pc.addProvenanceNode(calibratingModelActivity);
        pc.addProvenanceRelationship(calibratingModelActivity, simulationModel);

        //create a new (fitted) simulation model
        SimulationModel newDummyModel = new SimulationModel(simulationModel.getStudyName(), "", simulationModel.getModelPath(), "", false);
        pc.addProvenanceNode(newDummyModel);
        pc.addProvenanceRelationship(newDummyModel, calibratingModelActivity);

        //done by abstract generator anyway
        //reuse Data:
        //for(ProvenanceNode data: pattern.get(ProvenanceNodeType.DATA)){
        //    pc.addProvenanceRelationship(calibratingModelActivity, data);
        //}

        //done by abstract generator anyway
        //reuse Requirement:
        //if(pattern.containsKey(ProvenanceNodeType.REQUIREMENT)){
        //    for(ProvenanceNode requirement: pattern.get(ProvenanceNodeType.REQUIREMENT)){
        //        pc.addProvenanceRelationship(calibratingModelActivity, requirement);
        //    }
        //}

        //reuse Experiment:
        return reuseExperiment(pc, pattern, calibratingModelActivity, simulationModel, inputList);
    }

}
