package org.mosi.grease.provenancefiltering.controller;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.entities.*;
import org.mosi.grease.provenancefiltering.model.entities.experiments.Experiment;
import org.mosi.grease.provenancefiltering.model.entities.experiments.ExperimentParameter;
import org.mosi.grease.provenancefiltering.model.enums.*;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.ResourceIterable;
import org.neo4j.graphdb.Transaction;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class JsonCaseStudyController {
    private MainController mainController;
    private ProvModelController provModelController;
    private Gson gson;
    private LinkedHashMap jsonMap;

    public JsonCaseStudyController(MainController controller) {
        this.mainController = controller;
        this.gson = new Gson();
    }

    public MainController getMainController() {
        return mainController;
    }

    public LinkedHashMap getJsonMap() {
        return jsonMap;
    }

    private LinkedHashMap getJsonFile(String path) {
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(path));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        LinkedHashMap json = gson.fromJson(bufferedReader,  LinkedHashMap.class);
        return json;
    }

    public void createGraphFromJson() {
        this.provModelController = mainController.getModelController();
        LinkedHashMap jsonMap = getJsonMap();
        for (Object jsonObject : jsonMap.keySet()) {
            LinkedTreeMap objectMap = (LinkedTreeMap) jsonMap.get(jsonObject.toString());
            switch (objectMap.get("type").toString()) {
                case "SimulationModel":
                    createSimulationModel(objectMap);
                    break;
                case "Assumption":
                    createAssumption(objectMap);
                    break;
                case "Data":
                    createData(objectMap);
                    break;
                case "QualitativeModel":
                    createQualitativeModel(objectMap);
                    break;
                case "Requirement":
                    createRequirement(objectMap);
                    break;
                case "ResearchQuestion":
                    createResearchQuestion(objectMap);
                    break;
                case "SensitivityAnalysis":
                    createExperiment(objectMap, ExperimentType.SENSITIVITYANALYSIS);
                    break;
                case "SimulationoptimizationExperiment":
                    createExperiment(objectMap, ExperimentType.SIMULATIONOPTIMALIZATION);
                    break;
                case "StochasticalModelCheckingExperiment":
                    createExperiment(objectMap, ExperimentType.STOCHASTICALMODELCHECKING);
                    break;
                case "Activity":
                    createProvenanceActivity(objectMap);
                    break;
                case "Relationship":
                    createRelationship(objectMap);
                    break;

            }

        }
    }

    private void createProvenanceActivity(LinkedTreeMap objectMap) {
        String id = "";
        String studyName = "";
        ActivityType activityType = ActivityType.VALIDATING_SIMULATION_MODEL;

        for (Object keys : objectMap.keySet()) {
            String key = keys.toString();
            Object result = objectMap.get(key);

            switch (key) {
                case "id": {
                    id = result.toString();
                    break;
                }
                case "studyName": {
                    studyName = result.toString();
                    break;
                }
                case "activityType": {
                    activityType = ActivityType.valueOf(result.toString());
                    break;
                }
            }
        }
        ProvenanceActivity activity = new ProvenanceActivity(id, studyName, activityType);
        provModelController.addProvenanceNodeToGraphAndDB(activity);
    }

    private void createExperiment(LinkedTreeMap objectMap, ExperimentType type) {
        Experiment experiment = new Experiment("id", "studyName", ExperimentRole.VALIDATION); //get Information about role?
        experiment.setExperimentType(type);

        for (Object o : objectMap.keySet()) {
            String key = o.toString();
            Object result = objectMap.get(key);
            switch (key) {
                case "id": {
                    String id = result.toString();
                    experiment.setId(id);
                    break;
                }
                case "studyName": {
                    String studyName = result.toString();
                    experiment.setStudyName(studyName);
                    break;
                }
                default: {
                    experiment.addAttribute(key,result);
                }
            }
        }

        provModelController.addProvenanceNodeToGraphAndDB(experiment);
    }

    private void createResearchQuestion(LinkedTreeMap objectMap) {
        String id = "";
        String studyName = "";
        String researchQuestionQuestion = "";
        for (Object o : objectMap.keySet()) {
            String key = o.toString();
            Object result = objectMap.get(key);
            switch (key) {
                case "id": {
                    id = result.toString();
                    break;
                }
                case "studyName": {
                    studyName = result.toString();
                    break;
                }
                case "description": {
                    researchQuestionQuestion = result.toString();
                    break;
                }
            }
        }
        ResearchQuestion researchQuestion = new ResearchQuestion(id,studyName);
        researchQuestion.setDescription(researchQuestionQuestion);
        provModelController.addProvenanceNodeToGraphAndDB(researchQuestion);
    }

    private void createRequirement(LinkedTreeMap objectMap) {
        String id = "";
        String studyName = "";
        String description = "";
        for (Object o : objectMap.keySet()) {
            String key = o.toString();
            Object result = objectMap.get(key);
            switch (key) {
                case "id": {
                    id = result.toString();
                    break;
                }
                case "studyName": {
                    studyName = result.toString();
                    break;
                }
                case "description": {
                    description = result.toString();
                    break;
                }
            }
        }
        Requirement requirement = new Requirement(id,studyName);
        if(!description.isEmpty()){
            requirement.setDescription(description);
        }
        provModelController.addProvenanceNodeToGraphAndDB(requirement);
    }

    private void createQualitativeModel(LinkedTreeMap objectMap) {
        String id = "";
        String studyName = "";
        List<ExperimentParameter> parameters = new ArrayList<>();

        for (Object o : objectMap.keySet()) {
            String key = o.toString();
            Object result = objectMap.get(key);
            switch (key) {
                case "id": {
                    id = result.toString();
                    break;
                }
                case "studyName": {
                    studyName = result.toString();
                    break;
                }
                case "parameter": {
                    for (Object value : (ArrayList) objectMap.get(key)) {
                        ExperimentParameter parameter = new ExperimentParameter("id", (Double)value, "unit"); //todo: parameter name and id
                        parameters.add(parameter);

                    }
                    break;
                }
            }
        }
        QualitativeModel qualitativeModel = new QualitativeModel(id,studyName);
        if(!parameters.isEmpty()){
            for(ExperimentParameter parameter: parameters){
                qualitativeModel.getParameters().add(parameter);
            }
        }
        provModelController.addProvenanceNodeToGraphAndDB(qualitativeModel);
    }

    private void createData(LinkedTreeMap objectMap) {
        String id = "";
        String studyName = "";
        //data generated by a simulation experiment
        DataType dataType = DataType.IN_SILICO;
        String generatedBy = "";
        String format = "";
        String description = "";
        for (Object property : objectMap.keySet()) {
            String key = property.toString();
            Object result = objectMap.get(key);
            switch (key) {
                case "id": {
                    id = result.toString();
                    break;
                }
                case "studyName": {
                    studyName = result.toString();
                    break;
                }
                case "format": {
                    format = result.toString();
                    break;
                }
                case "description": {
                    description = result.toString();
                    break;
                }
                case "generatedBy": {
                    generatedBy = result.toString();
                    break;
                }
            }
        }
        Data data = new Data(id, studyName, dataType, Status.UNDEFINED);
        if(!description.isEmpty()){
            data.setDescription(description);
        }
        if(!format.isEmpty()){
            data.setFormat(format);
        }
        provModelController.addProvenanceNodeToGraphAndDB(data);
    }

    private void createRelationship(LinkedTreeMap objectMap) {
        Object from = objectMap.get("from");
        Node sourceNode = null;
        Node targetNode;
        Transaction transaction = mainController.getDatabaseController().getGraphDb().beginTx();
        ResourceIterable<Node> allNodes = mainController.getDatabaseController().getGraphDb().getAllNodes();
        for (Node node : allNodes) {
            if (node.getProperty("id").equals(from.toString())) {
                sourceNode = node;
            }
        }
        for (Object target : ((List) objectMap.get("to"))) {
            for (Node node : allNodes) {
                if (node.getProperty("id").equals(target.toString())) {
                    targetNode = node;
                    sourceNode.createRelationshipTo(targetNode, RelationshipType.withName("asd"));

                }
            }
        }
        transaction.success();
        transaction.close();

    }

    private void createAssumption(LinkedTreeMap objectMap) {
        String id = "";
        String description = "";
        String studyName = "";
        for (Object property : objectMap.keySet()) {
            String key = property.toString();
            Object result = objectMap.get(key);
            switch (property.toString()) {
                case "id": {
                    id = result.toString();
                    break;
                }
                case "studyName": {
                    studyName = result.toString();
                    break;
                }
                case "description": {
                    description = result.toString();
                    break;
                }
            }
        }
        Assumption assumption = new Assumption(id, studyName, description);
        provModelController.addProvenanceNodeToGraphAndDB(assumption);
    }

    private void createSimulationModel(LinkedTreeMap objectMap) {
        String id = "";
        String studyName = "";
        String appplicationDomain = "";
        String modelPath = "";
        Boolean isValidated = null;
        String modelSpecification = "";
        for (Object property : objectMap.keySet()) {
            String key = property.toString();
            Object result = objectMap.get(key);
            switch (property.toString()) {
                case "id": {
                    id = result.toString();
                    break;
                }
                case "studyName": {
                    studyName = result.toString();
                    break;
                }
                case "applicationDomain": {
                    appplicationDomain = result.toString();
                    break;
                }
                case "modelPath": {
                    modelPath = result.toString();
                    break;
                }
                case "isValidated": {
                    isValidated = Boolean.getBoolean(property.toString());
                    break;
                }
                case "modelSpecification": {
                    modelSpecification = result.toString();
                    break;
                }
            }
        }
        SimulationModel simulationModel = new SimulationModel(id, studyName);
        if(!modelSpecification.isEmpty()){
            simulationModel.setModelSpecification(modelSpecification);
        }
        if(!modelPath.isEmpty()){
            simulationModel.setModelPath(modelPath);
        }
        if(!appplicationDomain.isEmpty()){
            simulationModel.setApplicationDomain(appplicationDomain);
        }
        if(isValidated!=null){
            simulationModel.setValidated(isValidated);
        }
        provModelController.addProvenanceNodeToGraphAndDB(simulationModel);
    }

}
