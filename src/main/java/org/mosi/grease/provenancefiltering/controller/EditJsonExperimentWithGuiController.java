package org.mosi.grease.provenancefiltering.controller;

import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.util.Pair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mosi.grease.Main;
import org.mosi.grease.experimentgeneration.controller.MainExperimentController;
import org.mosi.grease.experimentgeneration.model.Domain;

import org.mosi.grease.experimentgeneration.model.ExperimentDesign;
import org.mosi.grease.experimentgeneration.model.ExperimentModel;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.entities.experiments.Experiment;

import java.util.*;

public class EditJsonExperimentWithGuiController extends Observable{
    MainExperimentController mainExperimentController;
    MainController mainController;
    List<org.json.JSONObject> inputExperimentsJsons;
    List<Domain> inputExperimentDomains;
    List<ExperimentDesign> inputExperimentTypes;
    ProvenanceActivity createdActivity;

    private static int jsonExperimentBookmark = 0;
    private static boolean lastExperimentToEdit = false;

    public EditJsonExperimentWithGuiController(MainExperimentController mainExperimentController, MainController mainController) {
        this.mainExperimentController = mainExperimentController;
        this.mainController = mainController;
    }

    public void openJsonExperiment(){

        mainExperimentController.setEditingExperiment(true);
        mainExperimentController.setEditJsonExperimentController(this);

        mainExperimentController.getView().getExperimentTab().setDomainValue(inputExperimentDomains.get(jsonExperimentBookmark));
        ExperimentDesign experimentType = inputExperimentTypes.get(jsonExperimentBookmark);
        if(experimentType!=null){
            mainExperimentController.getView().getExperimentTab().setExperimentDesign(experimentType);
        }
        LinkedHashMap<String, Object> generatedGuiElements = mainExperimentController.getExperimentModel().getGeneratedGuiElements();

        JSONObject experimentJson = inputExperimentsJsons.get(jsonExperimentBookmark);
        jsonExperimentBookmark ++;
        Main.getWindow().show();
        displayContentOfJson(experimentJson, generatedGuiElements);
    }

    //called, when experiment is finished being manually edited
    public void insertExperimentFromJsonIntoProvenanceGraph(JSONObject experimentJson){
        ProvModelController provModelController = mainController.getModelController();
        String studyName = provModelController.getNameOfCurrentStudy();
        Experiment experiment = mainController.getExperimentJsonController().
                generateExperimentFromJson(experimentJson, studyName);
        mainController.getExperimentJsonController().saveExperimentJsonAsFile(experiment);
        provModelController.addProvenanceNodeToGraphAndDB(experiment);
        provModelController.createRelationship(experiment,createdActivity);
        if(lastExperimentToEdit){
            this.addObserver(mainController.getAbstractCaseStudyController());
            setChanged();
            notifyObservers();
        }
    }

    public void stopEditingCurrentExperiment(ExperimentModel experimentModel) {
        JSONObject jsonInput = experimentModel.getJsonInput();

        if(EditJsonExperimentWithGuiController.isLastExperimentToEdit()){
            Main.getWindow().hide();
            insertExperimentFromJsonIntoProvenanceGraph(jsonInput);
        } else {
            insertExperimentFromJsonIntoProvenanceGraph(jsonInput);

            int jsonExperimentBookmark = getJsonExperimentBookmark();
            List<JSONObject> jsonExperiments = getInputExperimentsJsons();
            if(jsonExperiments.size() == jsonExperimentBookmark +1 ){
                setLastExperimentToEdit(true);
            }
            openJsonExperiment();
        }
    }

    private void displayContentOfJson(JSONObject jsonExperiment, LinkedHashMap<String, Object> generatedGuiElements) {

        for (Object keyObject : jsonExperiment.keySet()){
            String key = (String) keyObject;
            Object guiElement = generatedGuiElements.get(key);

            if(jsonExperiment.get(key) instanceof String || jsonExperiment.get(key) instanceof Integer || jsonExperiment.get(key) instanceof Long){
                String value = String.valueOf(jsonExperiment.get(key));

                if(guiElement instanceof TextField){
                    TextField textField = (TextField) guiElement;
                    if(textField.isDisabled()){
                        handleOneOfDisable(generatedGuiElements, key);
                    }
                    textField.setText(value);
                } else if(guiElement instanceof ComboBox){
                    ComboBox<String> comboBox = (ComboBox<String>) guiElement;
                    if(comboBox.isDisabled()){
                        handleOneOfDisable(generatedGuiElements, key);
                    }
                    comboBox.getSelectionModel().select(value);
                }
            } else if(jsonExperiment.get(key) instanceof JSONArray) {
                JSONArray entries = jsonExperiment.getJSONArray(key);
                if(guiElement instanceof ArrayList){
                    ArrayList<TextField> listOfTextfields = (ArrayList<TextField>) guiElement;
                    for (int i = 0; i < entries.length(); i++) {
                        //if entry needs new dynamic text field, add new text field via add-button:
                        if (listOfTextfields.size() <= i){
                            ArrayList<javafx.scene.control.Button> addButtons = (ArrayList) generatedGuiElements.get("addButtons" + keyObject);
                            if(addButtons.get(0).isDisable()){
                                handleOneOfDisable(generatedGuiElements, key);
                            }
                            addButtons.get(0).fire();
                        }
                        TextField textField = listOfTextfields.get(i);
                        if(textField.isDisabled()){
                            handleOneOfDisable(generatedGuiElements, key);
                        }
                        textField.setText(String.valueOf(entries.get(i)));
                    }
                }

            }else if(jsonExperiment.get(key) instanceof JSONObject){
                displayContentOfJson((JSONObject) jsonExperiment.get(key), generatedGuiElements);
            }
        }
    }

    private void handleOneOfDisable(LinkedHashMap<String, Object> generatedGuiElements, String textFieldKey) {
        HashMap<String,List<Object>> oneOfLookup = (HashMap<String,List<Object>>) generatedGuiElements.get("oneOfLookup");
        if(oneOfLookup.containsKey(textFieldKey)){
            List<Object> comboboxes = oneOfLookup.get(textFieldKey);
            for(Object comboboxObject : comboboxes){
                Pair<ComboBox, String> comboBoxPair = (Pair<ComboBox, String>) comboboxObject;
                ComboBox comboBox = comboBoxPair.getKey();
                comboBox.getSelectionModel().select(comboBoxPair.getValue());
                mainExperimentController.getExperimentModel().updateEdibility(generatedGuiElements, comboBox);
            }
        }
    }

    public void startExperimentEditor(List<org.json.JSONObject> inputExperimentsJsons, List<Domain> inputExperimentDomains, List<ExperimentDesign> inputExperimentTypes) {
        this.inputExperimentsJsons = inputExperimentsJsons;
        this.inputExperimentDomains = inputExperimentDomains;
        this.inputExperimentTypes = inputExperimentTypes;
        jsonExperimentBookmark = 0;
        lastExperimentToEdit = false;

        if(inputExperimentsJsons.size() == 1) {
            lastExperimentToEdit = true;
        }
        openJsonExperiment();
    }

    //------------------------------------------------getter and setter:------------------------------------------------

    public static boolean isLastExperimentToEdit() {
        return lastExperimentToEdit;
    }

    public static int getJsonExperimentBookmark() {
        return jsonExperimentBookmark;
    }

    public static void setJsonExperimentBookmark(int jsonExperimentBookmark) {
        EditJsonExperimentWithGuiController.jsonExperimentBookmark = jsonExperimentBookmark;
    }

    public static void setLastExperimentToEdit(boolean lastExperimentToEdit) {
        EditJsonExperimentWithGuiController.lastExperimentToEdit = lastExperimentToEdit;
    }

    public void setCreatedActivity(ProvenanceActivity createdActivity) {
        this.createdActivity = createdActivity;
    }

    public List<JSONObject> getInputExperimentsJsons() {
        return inputExperimentsJsons;
    }
}
