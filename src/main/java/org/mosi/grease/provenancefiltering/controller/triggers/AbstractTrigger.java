package org.mosi.grease.provenancefiltering.controller.triggers;

import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.controller.activityGenerators.AbstractGenerator;
import org.mosi.grease.provenancefiltering.controller.patternfilter.AbstractFilter;
import org.mosi.grease.provenancefiltering.controller.patternfilter.conditions.Condition;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.ProvenanceCommit;
import org.mosi.grease.provenancefiltering.model.ProvenanceNode;
import org.mosi.grease.provenancefiltering.model.entities.Other;
import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Observable;


public abstract class AbstractTrigger extends Observable {
    MainController mainController;
    AbstractFilter triggerFilter; //This filter will be used to check whether trigger applies

    public AbstractTrigger(MainController mainController) {
        this.mainController = mainController;
    }

    public void trigger(ProvenanceActivity activity, AbstractFilter filterForPatternToBeReused, AbstractGenerator generatorToGenerateOrReusePattern,
                        List<Condition> modelConditions, List<Condition> experimentConditions){

        //activity only triggers if it modified the simulation model
        SimulationModel simulationModelGeneratedByActivity = triggerFilter.getSimulationModelGeneratedByTriggerActivity(activity);
        if(simulationModelGeneratedByActivity != null) {

            //get simulation model used by trigger activity
            SimulationModel simulationModelUsedByActivity = triggerFilter.getSimulationModelUsedByTriggerActivity(activity);
            //debug
//            System.out.println(activity.getName() + " " + activity.getStudyName());
//            System.out.println(simulationModelUsedByActivity.getModelId() + " " + simulationModelUsedByActivity.getStudyName());
//            System.out.println(simulationModelGeneratedByActivity.getModelId() + " " + simulationModelGeneratedByActivity.getStudyName());

            //find all matches of filter where conditions apply
            List<Map<ProvenanceNodeType, List<ProvenanceNode>>> filterResultList = filterForPatternToBeReused.matchExperiments(
                    activity, simulationModelGeneratedByActivity, simulationModelUsedByActivity, modelConditions, experimentConditions);

            List<ProvenanceNode> generatedEntities = new ArrayList<>();

            //if matches not empty, loop over matches and for each generate an experiment
            if(filterResultList.isEmpty()){
                System.out.println("No experiments to reuse.");
                System.out.println("----------------------------------------------------------");
            }
            else {

                for(Map<ProvenanceNodeType, List<ProvenanceNode>> match : filterResultList) {

                    System.out.println("--------------------------------------------------------------------");
                    System.out.print("Found activity to reuse: ");
                    printFilteredActivity(match);

                    //find all inputs to this activity
                    List<Map<ProvenanceNodeType, List<ProvenanceNode>>> inputList = filterForPatternToBeReused.getInputs(
                            match.get(ProvenanceNodeType.ACTIVITY).get(0).getName(),
                            match.get(ProvenanceNodeType.ACTIVITY).get(0).getStudyName());

                    //create new commit for this activity
                    ProvenanceCommit pc = new ProvenanceCommit();

                    //Generate a new activity using the filtering results
                    System.out.println("Starting experiment reuse");
                    boolean isGenerationSuccessful = generatorToGenerateOrReusePattern.generate(
                            pc, match, simulationModelGeneratedByActivity, inputList);

                    //commit the experiment if it could be generated
                    if(isGenerationSuccessful) {
                        //collect the generated nodes for later
                        generatedEntities.addAll(pc.getProvenanceNodeList());
                        System.out.println("Generating provenance");
                        mainController.getModelController().commitActivity(pc);
                    }
                }
            }
            //notify case study observer to continue after this reuse round (even if no experiments were found)
            if(!AbstractGenerator.isManuallyEditExperiment()){
                this.addObserver(mainController.getAbstractCaseStudyController());
                setChanged();
                //Pass on the generated entities to the case study
                notifyObservers(generatedEntities);
            }
        }
    }

    public void trigger(AbstractFilter filterForPatternToBeReused, AbstractGenerator generatorToGenerateOrReusePattern,
                        List<Condition> modelConditions, List<Condition> experimentConditions){
        ProvenanceActivity activity = triggerFilter.lookForNewestActivity();
        this.trigger(activity, filterForPatternToBeReused, generatorToGenerateOrReusePattern, modelConditions, experimentConditions);
    }

    public void printFilteredActivity(Map<ProvenanceNodeType, List<ProvenanceNode>> filterResult) {
        System.out.println(filterResult.get(ProvenanceNodeType.ACTIVITY).get(0).getName() + " in " +
                filterResult.get(ProvenanceNodeType.ACTIVITY).get(0).getStudyName());
    }
}
