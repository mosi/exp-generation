package org.mosi.grease.provenancefiltering.controller.patternfilter.conditions;

import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;

public class IsValidatedCondition extends Condition {

    //query if the model is validated
    String subquery = "m.isValidated = true";

    @Override
    public String getQueryString(SimulationModel modelUsedByActivity) {
        return subquery;
    }
}
