package org.mosi.grease.provenancefiltering.controller.patternfilter.conditions;

import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;

public abstract class Condition {

    public abstract String getQueryString(SimulationModel modelUsedByActivity);

}
