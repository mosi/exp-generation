package org.mosi.grease.provenancefiltering.controller.activityGenerators;

import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.ProvenanceCommit;
import org.mosi.grease.provenancefiltering.model.ProvenanceNode;
import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;
import org.mosi.grease.provenancefiltering.model.enums.ActivityType;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

import java.util.List;
import java.util.Map;

public class AnalysisGenerator extends AbstractGenerator{
    public AnalysisGenerator(MainController mainController) {
        super(mainController);
    }

    @Override
    public boolean generate(ProvenanceCommit pc, Map<ProvenanceNodeType, List<ProvenanceNode>> pattern, ProvenanceNode provenanceModelNode,
                            List<Map<ProvenanceNodeType, List<ProvenanceNode>>> inputList) {
        SimulationModel simulationModel = (SimulationModel) provenanceModelNode;

        //create and add new activity:
        ProvenanceActivity analyzingModelActivity = new ProvenanceActivity(simulationModel.getStudyName(), ActivityType.ANALYZING_SIMULATION_MODEL,
                pattern.get(ProvenanceNodeType.ACTIVITY).get(0).getName());

        pc.addProvenanceNode(analyzingModelActivity);
        pc.addProvenanceRelationship(analyzingModelActivity, simulationModel);

        //reuse Experiment:
        return reuseExperiment(pc, pattern, analyzingModelActivity, simulationModel, inputList);
    }
}
