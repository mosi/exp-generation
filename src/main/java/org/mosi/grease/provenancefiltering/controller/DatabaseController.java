package org.mosi.grease.provenancefiltering.controller;

import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.ProvenanceNode;
import org.mosi.grease.provenancefiltering.model.entities.experiments.Experiment;
import org.mosi.grease.provenancefiltering.model.entities.*;
import org.neo4j.graphdb.*;
import org.neo4j.graphdb.factory.GraphDatabaseFactory;
import org.neo4j.kernel.configuration.BoltConnector;

import java.io.File;


public class DatabaseController {
    GraphDatabaseService graphDb;
    private final MainController mainController;

    public DatabaseController(MainController mainController) {
        this.mainController = mainController;
        startDatabase();
        clearDatabase();
        System.out.println("Database started");
    }

    private void startDatabase() {
        File graphDatabaseFile = new File("src/main/resources/database/graph.db");

        BoltConnector bolt = new BoltConnector("0");
        graphDb = new GraphDatabaseFactory().newEmbeddedDatabaseBuilder(graphDatabaseFile)
                                            .setConfig(bolt.type, "BOLT")
                                            .setConfig(bolt.enabled, "true")
                                            .setConfig(bolt.listen_address, "localhost:7688")
                                            .newGraphDatabase();
    }

    public void stopDataBase() {
        graphDb.shutdown();
    }

    private void clearDatabase() {
        graphDb.execute("Match (n) DETACH DELETE n");
    }

    public void addRelationship(Node sourceNode, Node targetNode) {
        Transaction transaction = graphDb.beginTx();
        sourceNode.createRelationshipTo(targetNode, RelationshipType.withName("asd"));
        transaction.success();
        transaction.close();
    }

    public void addNode(ProvenanceNode provenanceNode){
        try(Transaction transaction = graphDb.beginTx()){
            Node node = graphDb.createNode(Label.label(provenanceNode.getType().toString()));
            node.setProperty("studyName", provenanceNode.getStudyName());
            node.setProperty("id", provenanceNode.getName());

            addClassSpecificProperties(provenanceNode, node);

            provenanceNode.setDbId(node.getId());
            System.out.println("added " + provenanceNode.getType() + " " + provenanceNode.getName());
            transaction.success();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void addClassSpecificProperties(ProvenanceNode provenanceNode, Node node){
        //set queryable properties
        Class<? extends ProvenanceNode> provenanceNodeClass = provenanceNode.getClass();
        if(provenanceNodeClass == Experiment.class){
            addExperiment(provenanceNode, node);
        } else if(provenanceNodeClass == SimulationModel.class){
            addSimulationModel(provenanceNode, node);
        } else if(provenanceNodeClass == Data.class){
            addData(provenanceNode, node);
        } else if (provenanceNodeClass == Assumption.class){
            addAssumption(provenanceNode, node);
        } else if (provenanceNodeClass == Requirement.class){
            addRequirement(provenanceNode, node);
        } else if (provenanceNodeClass == ResearchQuestion.class){
            addResearchQuestion(provenanceNode, node);
        } else if (provenanceNodeClass == ProvenanceActivity.class){
            addActivity(provenanceNode, node);
        }
    }

    private void addExperiment(ProvenanceNode provenanceNode, Node node) {
        Experiment experiment = (Experiment) provenanceNode;
        node.setProperty("experimentRole", experiment.getRole().toString());
        if(experiment.getExperimentType() != null) {
            node.setProperty("type", experiment.getExperimentType().toString());
        }
    }

    private void addSimulationModel(ProvenanceNode provenanceNode, Node node) {
        SimulationModel model = (SimulationModel) provenanceNode;
        node.setProperty("isValidated", model.isValidated());
    }

    private void addData(ProvenanceNode provenanceNode, Node node) {
        Data data = (Data) provenanceNode;
        node.setProperty("type", data.getType().toString());
        node.setProperty("status", data.getStatus().toString());
    }

    private void addAssumption(ProvenanceNode provenanceNode, Node node) {
        Assumption assumption = (Assumption) provenanceNode;
        if(assumption.getDescription() != null){
            node.setProperty("description", assumption.getDescription());
        }
    }

    private void addRequirement(ProvenanceNode provenanceNode, Node node) {
        Requirement requirement = (Requirement) provenanceNode;
        if(requirement.getDescription() != null){
            node.setProperty("description", requirement.getDescription());
        }
    }


    private void addResearchQuestion(ProvenanceNode provenanceNode, Node node) {
        ResearchQuestion researchQuestion = (ResearchQuestion) provenanceNode;
        if(researchQuestion.getDescription() != null){
            node.setProperty("description", researchQuestion.getDescription());
        }
    }

    private void addActivity(ProvenanceNode provenanceNode, Node node) {
        ProvenanceActivity provenanceActivity = (ProvenanceActivity) provenanceNode;
        node.setProperty("activityType", provenanceActivity.getActivityType().toString());
    }

    public GraphDatabaseService getGraphDb() {
        return graphDb;
    }
}
