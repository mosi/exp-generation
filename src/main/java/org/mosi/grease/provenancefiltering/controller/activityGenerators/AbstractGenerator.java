package org.mosi.grease.provenancefiltering.controller.activityGenerators;

import org.apache.commons.io.FileUtils;
import org.mosi.grease.backend.BackendController;
import org.mosi.grease.experimentgeneration.controller.generation.controller.GenerationController;
import org.mosi.grease.experimentgeneration.model.BackendType;
import org.mosi.grease.provenancefiltering.controller.DatabaseController;
import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.controller.ProvModelController;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.ProvenanceCommit;
import org.mosi.grease.provenancefiltering.model.ProvenanceNode;
import org.mosi.grease.provenancefiltering.model.entities.Data;
import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;
import org.mosi.grease.provenancefiltering.model.entities.experiments.Experiment;
import org.mosi.grease.provenancefiltering.model.enums.DataType;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;
import org.mosi.grease.provenancefiltering.model.enums.Status;
import org.neo4j.graphdb.GraphDatabaseService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public abstract class AbstractGenerator {
    MainController mainController;
    DatabaseController databaseController;
    GraphDatabaseService graphDB;
    ProvModelController provModelController;
    BackendController backendController;
    GenerationController generationController;
    static boolean manuallyEditExperiment = false;

    public AbstractGenerator(MainController mainController) {
        this.mainController = mainController;
        databaseController = mainController.getDatabaseController();
        graphDB = databaseController.getGraphDb();
        provModelController = mainController.getModelController();
        backendController = mainController.getBackendController();
        generationController = mainController.getMainExperimentController().getGenerationController();
    }

    public abstract boolean generate(ProvenanceCommit pc, Map<ProvenanceNodeType, List<ProvenanceNode>> pattern, ProvenanceNode provenanceNode,
                                     List<Map<ProvenanceNodeType, List<ProvenanceNode>>> inputList);

    public static void setManuallyEditExperiment(boolean manuallyEditExperiment) {
        ValidatingSimulationGenerator.manuallyEditExperiment = manuallyEditExperiment;
    }

    boolean reuseExperiment(ProvenanceCommit pc, Map<ProvenanceNodeType, List<ProvenanceNode>> pattern,
                            ProvenanceActivity generatedActivity, SimulationModel model,
                            List<Map<ProvenanceNodeType, List<ProvenanceNode>>> inputList) {
        List<Experiment> experimentList = new ArrayList<>();

        //TODO only use outgoing experiments
        //Get the experiment node, each pattern should only contain 1 experiment to reuse
        ProvenanceNode experiment = pattern.get(ProvenanceNodeType.EXPERIMENT).get(0);
        Experiment oldExperiment = (Experiment) experiment;
        //link old experiment to new activity
        pc.addProvenanceRelationship(generatedActivity, oldExperiment);

        //Add data entities that were used as input
        if(inputList.get(0).containsKey(ProvenanceNodeType.DATA)) {
            for (ProvenanceNode input : inputList.get(0).get(ProvenanceNodeType.DATA)) {
                pc.addProvenanceRelationship(generatedActivity, input);
            }
        }
        //Add requirement entities that were used as input
        if(inputList.get(0).containsKey(ProvenanceNodeType.REQUIREMENT)) {
            for (ProvenanceNode input : inputList.get(0).get(ProvenanceNodeType.REQUIREMENT)) {
                pc.addProvenanceRelationship(generatedActivity, input);
            }
        }
        if(inputList.get(0).containsKey(ProvenanceNodeType.OTHER)) {
        //Add other entities that were used as input
            for (ProvenanceNode input : inputList.get(0).get(ProvenanceNodeType.OTHER)) {
                pc.addProvenanceRelationship(generatedActivity, input);
            }
        }

        //Create the new experiment entity
        Experiment generatedExperiment = mainController.getExperimentGeneratorController().
                generateNewExperimentEntity(oldExperiment, model, generatedActivity);

        //Generate and adapt the new experiment based on context information (incl. parsing and saving JSON specification)
        if(oldExperiment.getReference() != null || oldExperiment.getJsonPath() != null) {
            System.out.println("Adapting experiment specification");
             generatedExperiment = mainController.getExperimentGeneratorController().
                    updateExperimentBasedOnModel(generatedExperiment, oldExperiment, model, generatedActivity);
        }

        //connect the new experiment to trigger activity
        pc.addProvenanceNode(generatedExperiment);
        pc.addProvenanceRelationship(generatedExperiment, generatedActivity);

        if(manuallyEditExperiment){
            //display the experiment
            experimentList.add(generatedExperiment);
        } else { //automatic execution (if possible)
            try {
                //get backend type based on model type
                BackendType backendType = backendController.inferExperimentType(model);

                //get model prov name
                String modelProvName = model.getModelId().toLowerCase();

                //execute if file paths are available
                if((generatedExperiment.getJsonPath() == null && generatedExperiment.getReference() == null) || generatedExperiment.getModelPath() == null) {
                    System.out.println("Files not found. Continue.");
                } else {
                    File modelFile = new File(generatedExperiment.getModelPath());

                    //skip execution (for demonstration purposes)
                    if(mainController.getMainExperimentController().getSkipExecution()) {
                        System.out.println("Skipping execution");
                        //create dummy data
                        Data simulationData = new Data(generatedExperiment.getStudyName(), DataType.IN_SILICO,
                               generatedExperiment.getName(), Status.UNDEFINED);
                        pc.addProvenanceNode(simulationData);
                        pc.addProvenanceRelationship(simulationData, generatedActivity);
                        return true;
                    } else {
                        //execution
                        System.out.println("Running experiment");
                        generationController.updateExperimentModel(generatedExperiment.getName(), backendType);
                        //initalize to old experiment file
                        File backendSpecificExperimentFile = new File(generatedExperiment.getReference());
                        //the inputs were changed, thus generated and save backend-specific specification
                        if(generatedExperiment.getJsonObject() != null) {
                            backendSpecificExperimentFile = generationController.transformJsonToBackendSpecification(
                                    generatedExperiment.getJsonObject());
                            generatedExperiment.setReference(backendSpecificExperimentFile.getPath());
                        }
                        String result = backendController.executeExperiment(backendSpecificExperimentFile,
                               modelFile, modelProvName, backendType, generatedExperiment.isRemote());
                        if(result != null) {
                            File resultDir = new File(result);
                            if (resultDir.isDirectory()) {
                                System.out.println("Collecting output data");
                                String pathToExperimentResults = "src" + File.separator + "main" + File.separator + "resources" + File.separator
                                        + "case-studies" + File.separator + "TOMACS" + File.separator + "dataGeneratedByExperiments" + File.separator;
                                Path targetForResult = Paths.get(pathToExperimentResults + resultDir.getName());
                                //We do not have to copy and delete the results from the experiment directory
                                if(backendType.equals(BackendType.SESSL)) {
                                    FileUtils.copyDirectory(resultDir, targetForResult.toFile());
                                    FileUtils.deleteDirectory(resultDir);
                                }
                                //fill data entity
                                Data simulationData = new Data(generatedExperiment.getStudyName(), DataType.IN_SILICO,
                                        generatedExperiment.getName(), Status.UNDEFINED);
                                simulationData.setReference(targetForResult.toString());
                                pc.addProvenanceNode(simulationData);
                                pc.addProvenanceRelationship(simulationData, generatedActivity);
                            }
                        } else {
                            //create dummy data
                            Data simulationData = new Data(generatedExperiment.getStudyName(), DataType.IN_SILICO,
                                    generatedExperiment.getName(), Status.UNDEFINED);
                            pc.addProvenanceNode(simulationData);
                            pc.addProvenanceRelationship(simulationData, generatedActivity);
                        }
                        return true;
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        //display the experiment in the GUI if in interactive mode
        if(manuallyEditExperiment){
            mainController.getExperimentGeneratorController().editExperimentsWithGUI(experimentList, generatedActivity);
            //manual generation complete (activity + experiment)
            return true;
        }

        //decrement experiment counter
        generatedExperiment.decrementCounter();
        //decrement activity counter
        generatedActivity.decrementCounter();

        return false;
    }

    public static boolean isManuallyEditExperiment() {
        return manuallyEditExperiment;
    }
}
