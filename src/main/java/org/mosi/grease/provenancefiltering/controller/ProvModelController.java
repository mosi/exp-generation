package org.mosi.grease.provenancefiltering.controller;

import org.grease.provenance.editor.ProvenanceEditorController;
import org.grease.provenance.model.graph.ProvenanceDependency;
import org.mosi.grease.Main;
import org.mosi.grease.provenancefiltering.model.ProvenanceCommit;
import org.mosi.grease.provenancefiltering.model.ProvenanceGraph;
import org.mosi.grease.provenancefiltering.model.ProvenanceNode;
import org.mosi.grease.provenancefiltering.model.ProvenanceRelationship;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;

import java.util.HashSet;
import java.util.Observable;


public class ProvModelController extends Observable {
    MainController mainController;
    DatabaseController databaseController;
    JsonCaseStudyController jsonCaseStudyController;
    ProvenanceGraph graph;
    long numberOfNodes;
    String nameOfCurrentStudy;

    public ProvModelController(MainController mainController) {
        this.mainController = mainController;
        databaseController = mainController.getDatabaseController();
        jsonCaseStudyController = mainController.getJsonController();
        graph = new ProvenanceGraph(mainController);
        this.addObserver(mainController.getTriggerController());
        numberOfNodes = 0;
    }

    public void commitActivity(ProvenanceCommit provenanceCommit) {

        HashSet<ProvenanceNode> activitySet = new HashSet<>();

        for (ProvenanceNode node : provenanceCommit.getProvenanceNodeList()) {
            addProvenanceNodeToGraphAndDB(node);
        }
        for (ProvenanceRelationship relationship : provenanceCommit.getProvenanceRelationshipList()) {
            ProvenanceNode source = relationship.getSource();
            ProvenanceNode target = relationship.getTarget();
            createRelationship(source, target);
            //collect activities that were committed
            if(source.getType() == ProvenanceNodeType.ACTIVITY){
                activitySet.add(source);
            } else if(target.getType() == ProvenanceNodeType.ACTIVITY){
                activitySet.add(target);
            }
        }

        //state of database changed
        setChanged();
        for (ProvenanceNode activity : activitySet) {
            notifyObservers(activity);
        }

    }

    public void createRelationship(ProvenanceNode sourceNode, ProvenanceNode targetNode){
        graph.addDependency(sourceNode,targetNode);
        databaseController.addRelationship(provenanceNodeToNeo4jNode(sourceNode),provenanceNodeToNeo4jNode(targetNode));
        String dependencyType = "used";
        if(targetNode.getType() == ProvenanceNodeType.ACTIVITY) {
            dependencyType = "wasGeneratedBy";
        }
        System.out.println("added RELATIONSHIP " + sourceNode.getName() + " " + dependencyType + " " + targetNode.getName());
        if(Main.INTERACTIVE){
            org.grease.provenance.model.graph.ProvenanceNode sourceNodeForGui = sourceNode.getNodeForDrawing();
            org.grease.provenance.model.graph.ProvenanceNode targetNodeForGui = targetNode.getNodeForDrawing();
            ProvenanceDependency provenanceDependency = mainController.getMainExperimentController().getMainProvenanceViewCotroller().getGuiGeneratingController().
                    addDependencyBetween(sourceNodeForGui,targetNodeForGui); //I don't understand, why this is called on the GeneratingController not the GUIGeneratingController
            mainController.getMainExperimentController().getMainProvenanceViewCotroller().getGraphDrawingController().drawProvenanceDependency(provenanceDependency);
        }
    }

    public Node provenanceNodeToNeo4jNode(ProvenanceNode provenanceNode){
        Transaction tx = databaseController.graphDb.beginTx();
        Node neo4jNode = databaseController.graphDb.getNodeById(provenanceNode.getDbId());
        tx.success();
        tx.close();
        return neo4jNode;
    }

    public void addProvenanceNodeToGraphAndDB(ProvenanceNode provenanceNode){
        provenanceNode.setCreationTime(numberOfNodes);
        numberOfNodes++;
        databaseController.addNode(provenanceNode);
        graph.addNode(provenanceNode);
        nameOfCurrentStudy = provenanceNode.getStudyName();

        if(Main.INTERACTIVE){
            ProvenanceEditorController provenanceEditorController = mainController.getMainExperimentController().getMainProvenanceViewCotroller().getProvenanceEditorController();
            org.grease.provenance.model.graph.ProvenanceNodeType provenanceNodeType;
            if(provenanceNode.getType() == ProvenanceNodeType.ACTIVITY){
                provenanceNodeType = org.grease.provenance.model.graph.ProvenanceNodeType.ACTIVITY;
            } else {
                provenanceNodeType = org.grease.provenance.model.graph.ProvenanceNodeType.ENTITY;
            }
            org.grease.provenance.model.graph.ProvenanceNode provenanceNodeForGui = new org.grease.provenance.model.graph.ProvenanceNode(provenanceNode.getName(),provenanceNodeType);
            if(provenanceNode.getDescription() != null){
                provenanceNodeForGui.setDescription(provenanceNode.getDescription());
            }
            provenanceNode.setNodeForDrawing(provenanceNodeForGui);
            mainController.getMainExperimentController().getMainProvenanceViewCotroller().getGuiGeneratingController().addNode(provenanceNodeForGui);
        }
    }

    public ProvenanceGraph getGraph() {
        return graph;
    }

    public String getNameOfCurrentStudy() {
        return nameOfCurrentStudy;
    }
}
