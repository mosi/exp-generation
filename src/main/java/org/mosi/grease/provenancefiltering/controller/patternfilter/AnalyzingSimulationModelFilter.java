package org.mosi.grease.provenancefiltering.controller.patternfilter;

import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.controller.patternfilter.conditions.Condition;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.ProvenanceNode;
import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

import java.util.List;
import java.util.Map;

public class AnalyzingSimulationModelFilter extends AbstractFilter {
    String analyzingSimulationModelPatternActivities_ToGetNewestActivity = "MATCH (:SIMULATIONMODEL) <--(a:ACTIVITY) <--(:EXPERIMENT) \n" +
            "WHERE not (a)<--(:SIMULATIONMODEL) AND not (:DATA)<--(a) AND not (:REQUIREMENT)<--(a)" +
            "WITH a " +
            "MATCH (a) <--(:DATA)" +
            "return a";

    //Analysis Pattern first part
    String analyzingSimulationModelPattern_ToBeReused_Part1 = "MATCH (m:SIMULATIONMODEL)<--(a:ACTIVITY)<--(e:EXPERIMENT) \n" +
            "WHERE not (a)<--(:SIMULATIONMODEL) ";

    //Analysis Pattern after model and experiment conditions
    String analyzingSimulationModelPattern_ToBeReused_Part2 = "MATCH (a)<--(d:DATA) \n" +
            "WHERE d.status = 'UNDEFINED' \n" +
            "return a, collect(e), collect(d)";

    String analyzingSimulationModelPattern_ToGetUsedSimulationModel = "";
    String analyzingSimulationModelPattern_ToGetUsedSimulationModel_part2 = "";


    public AnalyzingSimulationModelFilter(MainController mainController) {
        super(mainController);
    }

    @Override
    public List<Map<ProvenanceNodeType, List<ProvenanceNode>>> matchExperiments(ProvenanceActivity activity,
                                                                                SimulationModel simulationModelGeneratedByActivity,
                                                                                SimulationModel simulationModelUsedByActivity,
                                                                                List<Condition> modelConditions,
                                                                                List<Condition> experimentConditions) {
        pattern_ToBeReused_Part1 = analyzingSimulationModelPattern_ToBeReused_Part1;
        pattern_ToBeReused_Part2 = analyzingSimulationModelPattern_ToBeReused_Part2;
        return super.matchExperiments(activity, simulationModelGeneratedByActivity, simulationModelUsedByActivity,
                modelConditions, experimentConditions);
    }


    @Override
    public SimulationModel getSimulationModelUsedByTriggerActivity(ProvenanceActivity activity) {
        Pattern_ToGetUsedSimulationModel = analyzingSimulationModelPattern_ToGetUsedSimulationModel;
        Pattern_ToGetUsedSimulationModel_part2 = analyzingSimulationModelPattern_ToGetUsedSimulationModel_part2;
        return super.getSimulationModelUsedByTriggerActivity(activity);
    }

    @Override
    public ProvenanceActivity lookForNewestActivity() {
        Pattern_ToGetNewestActivity = analyzingSimulationModelPatternActivities_ToGetNewestActivity;
        return super.lookForNewestActivity();
    }
}
