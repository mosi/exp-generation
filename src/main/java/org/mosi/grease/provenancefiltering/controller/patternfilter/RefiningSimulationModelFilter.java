package org.mosi.grease.provenancefiltering.controller.patternfilter;

import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.controller.patternfilter.conditions.Condition;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.ProvenanceNode;
import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

import java.util.List;
import java.util.Map;

public class RefiningSimulationModelFilter extends AbstractFilter {

    //Refining simulation model pattern: uses a simulation model plus some other artifacts, and produces a simulation model
    String refiningSimulationModelPattern_ToGetNewestActivity = "MATCH (:SIMULATIONMODEL) <--(a:ACTIVITY) <--(:SIMULATIONMODEL) \n" +
            "WHERE not (a) <--(:DATA) AND not (a) <--(:EXPERIMENT) \n" +
            "WITH a \n" +
            "MATCH (d) <--(a) \n" +
            "WHERE (d:SIMULATIONMODEL OR d:RESEARCHQUESTION OR d:DATA OR d:ASSUMPTION OR d:QUALITATIVEMODEL OR d:OTHER) \n" +
            "return a";

    String refiningSimulationModelPattern_ToBeReused_Part1 = "MATCH (d) <--(a:ACTIVITY) <--(s:SIMULATIONMODEL) \n" +
            "WHERE (d:SIMULATIONMODEL OR d:RESEARCHQUESTION OR d:DATA OR d:ASSUMPTION OR d:QUALITATIVEMODEL OR d:OTHER) \n" +
            "AND not (a) <--(:DATA) AND not (a) <--(:EXPERIMENT)" +
            "WITH a,d,s \n" +
            "MATCH (a)-[*]-(b) \n" +
            "WHERE b.id= '";
    String refiningSimulationModelPattern_ToBeReused_Part2 = "' return collect(d), a, s";

    String refiningSimulationModelPattern_ToGetUsedSimulationModel = "MATCH (outs:SIMULATIONMODEL) <--(a:ACTIVITY) <--(:SIMULATIONMODEL) \n" +
            "WHERE not (a) <--(:DATA) AND not (a) <--(:EXPERIMENT) AND a.id = '";
    String refiningSimulationModelPattern_ToGetUsedSimulationModel_part2 = "AND a.studyName = '";

    String refiningSimulationModelPattern_ToGetGeneratedSimulationModel = "MATCH (d) <--(a:ACTIVITY) <--(outs:SIMULATIONMODEL) \n" +
            "WHERE (d:SIMULATIONMODEL OR d:RESEARCHQUESTION OR d:DATA OR d:ASSUMPTION OR d:QUALITATIVEMODEL OR d:OTHER) \n" +
            "AND not (a) <--(:DATA) AND not (a) <--(:EXPERIMENT) AND a.id = '";
    String refiningSimulationModelPattern_ToGetGeneratedSimulationModel_part2 = "AND a.studyName = '";

    public RefiningSimulationModelFilter(MainController mainController) {
        super(mainController);
    }

    @Override
    public SimulationModel getSimulationModelGeneratedByTriggerActivity(ProvenanceActivity activity) {
        Pattern_ToGetGeneratedSimulationModel = refiningSimulationModelPattern_ToGetGeneratedSimulationModel;
        Pattern_ToGetGeneratedSimulationModel_part2 = refiningSimulationModelPattern_ToGetGeneratedSimulationModel_part2;
        return super.getSimulationModelGeneratedByTriggerActivity(activity);
    }

    @Override
    public SimulationModel getSimulationModelUsedByTriggerActivity(ProvenanceActivity activity) {
        Pattern_ToGetUsedSimulationModel = refiningSimulationModelPattern_ToGetUsedSimulationModel;
        Pattern_ToGetUsedSimulationModel_part2 = refiningSimulationModelPattern_ToGetUsedSimulationModel_part2;
        return super.getSimulationModelUsedByTriggerActivity(activity);
    }

    @Override
    public ProvenanceActivity lookForNewestActivity() {
        Pattern_ToGetNewestActivity = refiningSimulationModelPattern_ToGetNewestActivity;
        return super.lookForNewestActivity();
    }

    //Building Simulation Model is not tested
    @Override
    public List<Map<ProvenanceNodeType, List<ProvenanceNode>>> matchExperiments(ProvenanceActivity triggeringActivity,
                                                                                SimulationModel simulationModelGeneratedByActivity,
                                                                                SimulationModel simulationModelUsedByActivity,
                                                                                List<Condition> modelConditions,
                                                                                List<Condition> experimentConditions) {
        pattern_ToBeReused_Part1 = refiningSimulationModelPattern_ToBeReused_Part1;
        pattern_ToBeReused_Part2 = refiningSimulationModelPattern_ToBeReused_Part2;
        return super.matchExperiments(triggeringActivity, simulationModelGeneratedByActivity, simulationModelUsedByActivity, modelConditions, experimentConditions);
    }

}
