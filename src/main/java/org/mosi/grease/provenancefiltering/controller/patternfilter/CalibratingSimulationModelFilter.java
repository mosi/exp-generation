package org.mosi.grease.provenancefiltering.controller.patternfilter;

import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.controller.patternfilter.conditions.Condition;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.ProvenanceNode;
import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;
import org.mosi.grease.provenancefiltering.model.enums.ProvenanceNodeType;

import java.util.List;
import java.util.Map;

public class CalibratingSimulationModelFilter extends AbstractFilter {

    //query a calibration activity that
    String calibratingSimulationModelPattern_ToGetNewestActivity = "MATCH (d) <--(a:ACTIVITY) <--(e:EXPERIMENT) \n" +
            "WHERE (d:DATA OR d:REQUIREMENT) \n" +
            "WITH d,a,e \n" +
            "MATCH (:SIMULATIONMODEL) <--(a) <--(f:DATA) \n " +
            "WITH d,a,e,f " +
            "MATCH (a) <--(:SIMULATIONMODEL)" +
            "return a";

    String calibratingSimulationModelPattern_ToBeReused_Part1 = "MATCH (d) <--(a:ACTIVITY) <--(e:EXPERIMENT) \n" +
            "WHERE (d:DATA OR d:REQUIREMENT) \n";

    String calibratingSimulationModelPattern_ToBeReused_Part2 =
            "WITH d,a,e \n" +
            "MATCH (:SIMULATIONMODEL) <--(a) <--(:DATA) \n " +
            "WITH d,a,e \n" +
            "MATCH (a) <--(s:SIMULATIONMODEL) \n" +
            "return a, collect(d), collect(e), s \n";

    String calibratingSimulationModelPattern_ToGetGeneratedSimulationModel = "MATCH (d) <--(a:ACTIVITY) <--(e:EXPERIMENT) \n" +
            "WHERE (d:DATA OR d:REQUIREMENT) \n" +
            "WITH d,a,e \n" +
            "MATCH (s:SIMULATIONMODEL) <--(a) <--(:DATA) \n " +
            "WITH d,a,e,s \n" +
            "MATCH (a) <--(outs:SIMULATIONMODEL)" + "WHERE a.id = '";
    String calibratingSimulationModelPattern_ToGetGeneratedSimulationModel_part2 = "AND a.studyName = '";

    String calibratingSimulationModelPattern_ToGetUsedSimulationModel = "MATCH (d) <--(a:ACTIVITY) <--(e:EXPERIMENT) \n" +
            "WHERE (d:DATA OR d:REQUIREMENT) \n" +
            "WITH d,a,e \n" +
            "MATCH (outs:SIMULATIONMODEL) <--(a) <--(:DATA) \n " +
            "WITH d,a,e,outs \n" +
            "MATCH (a) <--(s:SIMULATIONMODEL)" + "WHERE a.id = '";
    String calibratingSimulationModelPattern_ToGetUsedSimulationModel_part2 = "AND a.studyName = '";

    public CalibratingSimulationModelFilter(MainController mainController) {
        super(mainController);
    }

    @Override
    public List<Map<ProvenanceNodeType, List<ProvenanceNode>>> matchExperiments(ProvenanceActivity triggeringActivity,
                                                                                SimulationModel simulationModelGeneratedByActivity,
                                                                                SimulationModel simulationModelUsedByActivity,
                                                                                List<Condition> modelConditions,
                                                                                List<Condition> experimentConditions) {
        pattern_ToBeReused_Part1 = calibratingSimulationModelPattern_ToBeReused_Part1;
        pattern_ToBeReused_Part2 = calibratingSimulationModelPattern_ToBeReused_Part2;
        return super.matchExperiments(triggeringActivity, simulationModelGeneratedByActivity, simulationModelUsedByActivity,
                modelConditions, experimentConditions);
    }

    @Override
    public SimulationModel getSimulationModelGeneratedByTriggerActivity(ProvenanceActivity activity) {
        Pattern_ToGetGeneratedSimulationModel = calibratingSimulationModelPattern_ToGetGeneratedSimulationModel;
        Pattern_ToGetGeneratedSimulationModel_part2 = calibratingSimulationModelPattern_ToGetGeneratedSimulationModel_part2;
        return super.getSimulationModelGeneratedByTriggerActivity(activity);
    }

    @Override
    public SimulationModel getSimulationModelUsedByTriggerActivity(ProvenanceActivity activity) {
        Pattern_ToGetUsedSimulationModel = calibratingSimulationModelPattern_ToGetUsedSimulationModel;
        Pattern_ToGetUsedSimulationModel_part2 = calibratingSimulationModelPattern_ToGetUsedSimulationModel_part2;
        return super.getSimulationModelUsedByTriggerActivity(activity);
    }

    @Override
    public ProvenanceActivity lookForNewestActivity() {
        Pattern_ToGetNewestActivity = calibratingSimulationModelPattern_ToGetNewestActivity;
        return super.lookForNewestActivity();
    }
}
