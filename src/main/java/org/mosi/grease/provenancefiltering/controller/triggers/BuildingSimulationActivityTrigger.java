package org.mosi.grease.provenancefiltering.controller.triggers;

import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.controller.activityGenerators.AbstractGenerator;
import org.mosi.grease.provenancefiltering.controller.patternfilter.AbstractFilter;
import org.mosi.grease.provenancefiltering.controller.patternfilter.RefiningSimulationModelFilter;
import org.mosi.grease.provenancefiltering.controller.patternfilter.conditions.Condition;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;

import java.util.List;

public class BuildingSimulationActivityTrigger extends AbstractTrigger{

    public BuildingSimulationActivityTrigger(MainController mainController) {
        super(mainController);
    }

    @Override
    public void trigger(ProvenanceActivity triggerActivity, AbstractFilter filterForPatternToBeReused,
                        AbstractGenerator generatorToGenerateOrReusePattern, List<Condition> modelConditions,
                        List<Condition> experimentConditions) {
        triggerFilter = new RefiningSimulationModelFilter(mainController);
        super.trigger(triggerActivity, filterForPatternToBeReused, generatorToGenerateOrReusePattern, modelConditions, experimentConditions);
    }

    @Override
    //used when triggered manually
    public void trigger(AbstractFilter filterForPatternToBeReused, AbstractGenerator generatorToGenerateOrReusePattern,
                        List<Condition> modelConditions, List<Condition> experimentConditions) {
        triggerFilter = new RefiningSimulationModelFilter(mainController);
        super.trigger(filterForPatternToBeReused, generatorToGenerateOrReusePattern, modelConditions, experimentConditions);
    }

}
