package org.mosi.grease.provenancefiltering.controller.patternfilter.conditions;

import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;

public class IsBasedOnCondition extends Condition{

    //query if the activities are based on the same model or models are equal
    String subquery = "((m.id = '";
    String subquery2 = "') OR ((m)<-[*1..5]-({id: '";
    String subquery3 = "'})))";

    @Override
    public String getQueryString(SimulationModel modelUsedByActivity) {
        return subquery + modelUsedByActivity.getModelId() + subquery2 + modelUsedByActivity.getModelId() + subquery3;
    }
}
