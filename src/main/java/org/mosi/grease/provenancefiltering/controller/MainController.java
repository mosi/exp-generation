package org.mosi.grease.provenancefiltering.controller;

import org.mosi.grease.backend.BackendController;
import org.mosi.grease.caseStudies.AbstractCaseStudyController;
import org.mosi.grease.caseStudies.CaseStudyController;
import org.mosi.grease.experimentgeneration.controller.MainExperimentController;
import org.mosi.grease.provenancefiltering.controller.jsonSubcontroller.ExperimentJsonController;
import org.mosi.grease.provenancefiltering.controller.triggers.TriggerController;

public class MainController {
    private DatabaseController databaseController;
    private JsonCaseStudyController jsonCaseStudyController;
    private ProvModelController provModelController;
    private ExperimentGeneratorController experimentGeneratorController;
    private ExperimentJsonController experimentJsonController;
    private TriggerController triggerController;
    private EditJsonExperimentWithGuiController editJsonExperimentWithGuiController;
    private MainExperimentController mainExperimentController;
    private CaseStudyController caseStudyController;
    private AbstractCaseStudyController abstractCaseStudyController;
    private BackendController backendController;
    private AdaptionController adaptionController;

    public MainController(MainExperimentController mainExperimentController) {
        this.jsonCaseStudyController = new JsonCaseStudyController(this);
        this.triggerController = new TriggerController(this);
        this.databaseController = new DatabaseController(this);
        this.provModelController = new ProvModelController(this);
        this.adaptionController = new AdaptionController(this);
        this.experimentGeneratorController = new ExperimentGeneratorController(this);
        this.experimentJsonController = new ExperimentJsonController(this);
        this.mainExperimentController = mainExperimentController;
        this.backendController = mainExperimentController.getBackendController();
        backendController.setMainController(this);
        this.editJsonExperimentWithGuiController = new EditJsonExperimentWithGuiController(mainExperimentController, this);
    }

    public JsonCaseStudyController getJsonController() {
        return jsonCaseStudyController;
    }

    public DatabaseController getDatabaseController() {
        return databaseController;
    }

    public ProvModelController getModelController() {
        return provModelController;
    }

    public ExperimentGeneratorController getExperimentGeneratorController() {
        return experimentGeneratorController;
    }

    public AdaptionController getAdaptionController() {
        return adaptionController;
    }

    public ExperimentJsonController getExperimentJsonController() {
        return experimentJsonController;
    }

    public TriggerController getTriggerController() {
        return triggerController;
    }

    public MainExperimentController getMainExperimentController() {
        return mainExperimentController;
    }

    public EditJsonExperimentWithGuiController getEditJsonExperimentController() {
        return editJsonExperimentWithGuiController;
    }

    public CaseStudyController getCaseStudyController() {
        return caseStudyController;
    }

    public void setCaseStudyController(CaseStudyController caseStudyController) {
        this.caseStudyController = caseStudyController;
    }

    public AbstractCaseStudyController getAbstractCaseStudyController() {
        return abstractCaseStudyController;
    }

    public void setAbstractCaseStudyController(AbstractCaseStudyController abstractCaseStudyController) {
        this.abstractCaseStudyController = abstractCaseStudyController;
    }

    public BackendController getBackendController() {
        return backendController;
    }

    //to debug via Neo4J desktop app:
    public static void emptyLoopToDebug(){
        while (true){
        }
    }
}
