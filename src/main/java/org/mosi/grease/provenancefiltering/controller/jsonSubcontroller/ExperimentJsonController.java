package org.mosi.grease.provenancefiltering.controller.jsonSubcontroller;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FilenameUtils;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.jetbrains.annotations.NotNull;
import org.jlibsedml.Libsedml;
import org.jlibsedml.SEDMLDocument;
import org.jlibsedml.UniformTimeCourse;
import org.jlibsedml.XMLException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mosi.grease.experimentgeneration.model.Domain;
import org.mosi.grease.experimentgeneration.model.ExperimentDesign;
import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.controller.ProvModelController;
import org.mosi.grease.provenancefiltering.model.entities.experiments.Experiment;
import org.mosi.grease.provenancefiltering.model.enums.ExperimentRole;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;


public class ExperimentJsonController {
    MainController mainController;
    ProvModelController provModelController;

    public ExperimentJsonController(MainController mainController) {
        this.mainController = mainController;
    }

    public ExperimentDesign inferExperimentTypeFromExperimentJason(org.json.JSONObject experimentJson){
        //gsa:
        String gsaSchemaPath = "schemas/Types/gsa-schema.json";
        org.json.JSONObject desSchema = loadJsonObjectFromPath(gsaSchemaPath);
        Schema schema = SchemaLoader.load(desSchema);
        try{
            schema.validate(experimentJson);
            return ExperimentDesign.GLOBAL_SENSITIVITY_ANALYSIS;
        }catch (ValidationException e){
        }
        return ExperimentDesign.NONE;
    }

    public Domain inferExperimentDomainFromExperimentJason(org.json.JSONObject experimentJson){

        //des:
        String desSchemaPath = "schemas/des-schema.json";
        org.json.JSONObject desSchema = loadJsonObjectFromPath(desSchemaPath);
        Schema schema = SchemaLoader.load(desSchema);
        try{
            schema.validate(experimentJson);
            return Domain.STOCHASTIC_DISCRETE_EVENT;
        }catch (ValidationException e){
        }

        //Virtual Prototyping of Heterogeneous Systems:
        String vpSchemaPath = "schemas/vp-schema.json";
        org.json.JSONObject vpSchema = loadJsonObjectFromPath(vpSchemaPath);
        schema = SchemaLoader.load(vpSchema);
        try{
            schema.validate(experimentJson);
            return Domain.HETEROGENEOUS_SYSTEMS;
        }catch (ValidationException e){
        }

        //Finite element analysis:
        String femSchemaPath = "schemas/fem-schema.json";
        org.json.JSONObject femSchema = loadJsonObjectFromPath(femSchemaPath);
        schema = SchemaLoader.load(femSchema);
        try{
            schema.validate(experimentJson);
            return Domain.FINITE_ELEMENT;
        }catch (ValidationException e){
        }

        return null;
    }

    @NotNull
    private org.json.JSONObject loadJsonObjectFromPath(String resource) {
        // this is the path within the jar file
        InputStream input = this.getClass().getResourceAsStream("/resources/" + resource);
        if (input == null) {
            // this is how we load file within editor (eg eclipse)
            input = this.getClass().getClassLoader().getResourceAsStream(resource);
        }

        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.getFactory().disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);
        try {
            map = mapper.readValue(input, LinkedHashMap.class);
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        org.json.JSONObject schemaJson = new org.json.JSONObject(map);
        return schemaJson;
    }

    public JSONObject buildJsonFromExperimentAttibutes(HashMap<String, Object> attributes){

        JSONObject experimentJson = new JSONObject();

        JSONObject modelObject = new JSONObject();
        experimentJson.put("model", modelObject);
        //JSONObject configurationObject = new JSONObject();
        JSONObject fileObject = new JSONObject();
        modelObject.put("modelFile", fileObject);
        fileObject.put("reference", attributes.get("reference"));
        //experimentJson.put("configuration", configurationObject);
        //fillJsonFromExperimentAttributes(configurationObject, attributes);

        JSONObject simulationObject = new JSONObject();
        experimentJson.put("simulation", simulationObject);
        simulationObject.put("simulator", attributes.get("simulator"));
        simulationObject.put("replications", 1);
        JSONObject stopConditionObject = new JSONObject();
        simulationObject.put("stopCondition", stopConditionObject);
        stopConditionObject.put("stopTime", attributes.get("stopTime"));

        JSONObject observationObject = new JSONObject();
        experimentJson.put("observation", observationObject);
        JSONObject observables = new JSONObject();
        observationObject.put("observables", observables);
        JSONArray expressionArray = new JSONArray();
        expressionArray.put(attributes.get("observationExpression"));
        observables.put("observationExpression", expressionArray);
        JSONArray aliasArray = new JSONArray();
        aliasArray.put(attributes.get("observationAlias"));
        observables.put("observationAlias", aliasArray);
        JSONObject observationTime = new JSONObject();
        observationObject.put("observationTime", observationTime);
        observationTime.put("observationRangeStart", attributes.get("observationRangeStart"));
        observationTime.put("observationRangeEnd", attributes.get("observationRangeEnd"));
        observationTime.put("observationRangeInterval", attributes.get("observationRangeInterval"));
        observationObject.put("outputFormat", attributes.get("outputFormat"));

        return experimentJson;
    }

    private void fillJsonFromExperimentAttributes(JSONObject modelObject, HashMap<String, Object> attributes) {

        for (String key : attributes.keySet()){
            Object object = attributes.get(key);
            if(object.getClass() == String.class){
                modelObject.put(key, object);
            } else if(object.getClass() == ArrayList.class){
                JSONArray jsonArray = new JSONArray();
                jsonArray.put((ArrayList) object);
                modelObject.put(key,jsonArray);
            } else {
                HashMap experimentMap = (HashMap) object;
                JSONObject newJsonObject = new JSONObject();
                modelObject.put(key, newJsonObject);
                fillJsonFromExperimentAttributes(newJsonObject, experimentMap);
            }
        }
    }

    public Experiment generateExperimentFromJson(JSONObject experimentJson, String studyName){
        this.provModelController = mainController.getModelController();

        Experiment experiment = null;
        if(experimentJson.has("experimentName")){
            String experimentName = (String) experimentJson.get("experimentName");
            experiment = new Experiment(experimentName, studyName, ExperimentRole.VALIDATION);
        }else {
            experiment = new Experiment(studyName, ExperimentRole.VALIDATION, "SEx");
        }
        //experiment.setJson(experimentJson);

        fillAttributesFromJson(experimentJson, experiment);

        return experiment;
    }

    private void fillAttributesFromJson(JSONObject experimentJson, Experiment experiment){
        HashMap<String, Object> attributes = experiment.getAttributes();
        for(String key : experimentJson.keySet()){
            Object object = experimentJson.get(key);
            if(object.getClass() == String.class || object.getClass() == Long.class || object.getClass() == Integer.class){
                if(key.equals("modelPath")){
                    experiment.setModelPath(experimentJson.getString(key));
                }
            } else if(object.getClass() == JSONArray.class){
                ArrayList<Object> newList = new ArrayList<>();
                newList.add(object);
                attributes.put(key, newList);
            } else {
                if (key.equals("model") || key.equals("configuration")) {
                    JSONObject jObject = (JSONObject) object;
                    HashMap<String, Object> newAttributes = new HashMap<>();
                    fillAttributesFromJson(jObject, experiment);
                }
            }
        }
    }

    public void saveExperimentJsonAsFile(Experiment experiment){
        JSONObject experimentJson = new JSONObject(experiment.getJsonPath());
        File file = new File(experiment.getReference());
        saveJsonObject(experimentJson, file);
    }

    public void saveJsonObject(JSONObject experimentJson, File file) {
        try {
            // Create a file writer
            FileWriter wr = new FileWriter(file, false);
            // Create buffered writer to write
            BufferedWriter w = new BufferedWriter(wr);
            // Write
            w.write(experimentJson.toString(2));

            w.flush();
            w.close();
        } catch (Exception evt) {
            evt.printStackTrace();
        }
    }

    public JSONObject parseSpecification(String experimentPath) {
        JSONObject experiment = null;
        File file = new File(experimentPath);
        String extension = FilenameUtils.getExtension(file.getName());
        if (extension.equals("sedml")) {
            experiment = parseSEDMLSpecification(experimentPath, file);
        } else if(extension.equals("sh")) {
            parseBashSpecification(experimentPath, file);
        }
        //default return null
        return experiment;
    }

    private JSONObject parseBashSpecification(String experimentPath, File file) {
        return null;
    }

    private JSONObject parseSEDMLSpecification(String experimentPath, File file) {
        try {
            SEDMLDocument doc = Libsedml.readDocument(file);

            HashMap<String, Object> inputs = new HashMap<>();
            String modelName = doc.getSedMLModel().getModels().get(0).getSource();
            inputs.put("reference", modelName);
            double initialTime = ((UniformTimeCourse) doc.getSedMLModel().getSimulations().get(0)).getInitialTime();
            inputs.put("startTime", initialTime);
            double outputStartTime = ((UniformTimeCourse) doc.getSedMLModel().getSimulations().get(0)).getOutputStartTime();
            inputs.put("observationRangeStart", outputStartTime);
            double outputEndTime = ((UniformTimeCourse) doc.getSedMLModel().getSimulations().get(0)).getOutputEndTime();
            inputs.put("observationRangeEnd", outputEndTime);
            inputs.put("stopTime", outputEndTime);
            double numberOfPoints = ((UniformTimeCourse) doc.getSedMLModel().getSimulations().get(0)).getNumberOfPoints();
            inputs.put("observationRangeInterval", numberOfPoints);
            String algorithmID = ((UniformTimeCourse) doc.getSedMLModel().getSimulations().get(0)).getAlgorithm().getKisaoID();
            inputs.put("simulator", algorithmID);
            String observedVariable = doc.getSedMLModel().getDataGenerators().get(2).getListOfVariables().get(0).getName();
            inputs.put("observationExpression", observedVariable);
            String alias = doc.getSedMLModel().getDataGenerators().get(2).getName();
            inputs.put("observationAlias", alias);

            //build json
            return buildJsonFromExperimentAttibutes(inputs);

        } catch (XMLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject getJsonForDomain(Domain d) {

        String resource = "";
        if(d.equals(Domain.STOCHASTIC_DISCRETE_EVENT)) {
            resource = "schemas/des-schema.json";
        } else if(d.equals(Domain.HETEROGENEOUS_SYSTEMS)) {
            resource = "schemas/vp-schema.json"; //Virtual Prototyping of Heterogeneous Systems
        } else if(d.equals(Domain.FINITE_ELEMENT)) {
            resource = "schemas/fem-schema.json";
        }

        // this is the path within the jar file
        InputStream input = this.getClass().getResourceAsStream("/resources/" + resource);
        if (input == null) {
            // this is how we load file within editor (eg eclipse)
            input = this.getClass().getClassLoader().getResourceAsStream(resource);
        }

        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.getFactory().disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);
        try {
            map = mapper.readValue(input, LinkedHashMap.class);
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        JSONObject jsonObject = new JSONObject(map);

        return jsonObject;
    }
}
