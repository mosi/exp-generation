package org.mosi.grease.provenancefiltering.controller.triggers;

import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.controller.activityGenerators.AnalysisGenerator;
import org.mosi.grease.provenancefiltering.controller.activityGenerators.CalibratingSimulationGenerator;
import org.mosi.grease.provenancefiltering.controller.activityGenerators.ValidatingSimulationGenerator;
import org.mosi.grease.provenancefiltering.controller.patternfilter.AnalyzingSimulationModelFilter;
import org.mosi.grease.provenancefiltering.controller.patternfilter.CalibratingSimulationModelFilter;
import org.mosi.grease.provenancefiltering.controller.patternfilter.ValidatingSimulationModelFilter;
import org.mosi.grease.provenancefiltering.controller.patternfilter.conditions.*;
import org.mosi.grease.provenancefiltering.model.ProvenanceActivity;
import org.mosi.grease.provenancefiltering.model.enums.ActivityType;

import java.util.*;

public class TriggerController implements Observer {
    MainController mainController;

    static boolean triggerAutomaticallyWhenInserted = false;

    static boolean ruleRefiningCalibratingEnabled = false;
    static boolean ruleRefiningAnalysisEnabled = false;
    static boolean ruleCalibratingValidatingEnabled = false;
    static boolean ruleCalibratingCrossValidatingEnabled = false;
    static boolean ruleComposingValidatingEnabled = false;

    public TriggerController(MainController mainController) {
        this.mainController = mainController;
    }

    //Use this to trigger manually
    public void triggerCalibratingSimulationModelGeneration() {
        //implement the rules in one place
        //When triggered, a CSM Activity will be generated/reused:
        CalibratingSimulationModelFilter calibratingSimulationModelFilter = new CalibratingSimulationModelFilter(mainController);
        CalibratingSimulationGenerator calibratingSimulationGenerator = new CalibratingSimulationGenerator(mainController);

        List<Condition> modelConditions = new ArrayList<>();
        List<Condition> experimentConditions = new ArrayList<>();

        BuildingSimulationActivityTrigger buildingSimulationActivityTrigger = new BuildingSimulationActivityTrigger(mainController);
        buildingSimulationActivityTrigger.trigger(calibratingSimulationModelFilter, calibratingSimulationGenerator,
                                                modelConditions, experimentConditions);
    }

    //Use this to trigger manually
    public void triggerValidatingSimulationModelGeneration() {
        //implement the rules in one place
        //When triggered, a VSM Activity will be generated/reused:
        ValidatingSimulationModelFilter validatingSimulationModelFilter = new ValidatingSimulationModelFilter(mainController);
        ValidatingSimulationGenerator validatingSimulationGenerator = new ValidatingSimulationGenerator(mainController);

        List<Condition> modelConditions = new ArrayList<>();
        List<Condition> experimentConditions = new ArrayList<>();

        CalibratingSimulationActivityTrigger calibratingSimulationActivityTrigger = new CalibratingSimulationActivityTrigger(mainController);
        calibratingSimulationActivityTrigger.trigger(validatingSimulationModelFilter, validatingSimulationGenerator,
                                                    modelConditions, experimentConditions);

        //This is necessary to continue the CaseStudy, because experiments are not executed automatically:
        calibratingSimulationActivityTrigger.addObserver(mainController.getAbstractCaseStudyController());
    }

    //this method is called, when a provenance dependency containing an activity is inserted into the provenance graph:
    @Override
    public void update(Observable o, Object arg) {
        if(arg.getClass() == ProvenanceActivity.class){
            if (triggerAutomaticallyWhenInserted) {
                ProvenanceActivity activity = (ProvenanceActivity) arg;
                if (activity.getActivityType() == ActivityType.REFINING_SIMULATION_MODEL) {
                    if (ruleRefiningCalibratingEnabled) {
                        System.out.println("-------------------Automatic Experiment Generation started-----------------");
                        System.out.println("Trigger activity detected: Refining Simulation Model " + activity.getName());
                        //When triggered, a CSM Activity will be generated/reused:
                        CalibratingSimulationModelFilter calibratingSimulationModelFilter = new CalibratingSimulationModelFilter(mainController);
                        CalibratingSimulationGenerator calibratingSimulationModelGenerator = new CalibratingSimulationGenerator(mainController);

                        //Conditions
                        List<Condition> modelConditions = new ArrayList<>();
                        List<Condition> experimentConditions = new ArrayList<>();

                        BuildingSimulationActivityTrigger buildingSimulationActivityTrigger = new BuildingSimulationActivityTrigger(mainController);
                        buildingSimulationActivityTrigger.trigger(activity, calibratingSimulationModelFilter, calibratingSimulationModelGenerator,
                                                                    modelConditions, experimentConditions);

                        //This is necessary to continue the CaseStudy, because experiments are not executed automatically:
                        buildingSimulationActivityTrigger.addObserver(mainController.getAbstractCaseStudyController());
                    }
                    if(ruleRefiningAnalysisEnabled){
                        System.out.println("-------------------Automatic Experiment Generation started-----------------");
                        System.out.println("Trigger activity detected: Refining Simulation Model " + activity.getName());
                        AnalyzingSimulationModelFilter analyzingSimulationModelFilter = new AnalyzingSimulationModelFilter(mainController);
                        AnalysisGenerator analyzingSimulationModelGenerator = new AnalysisGenerator(mainController);

                        //Conditions
                        List<Condition> modelConditions = new ArrayList<>();
                        AreEqualCondition c1 = new AreEqualCondition();
                        modelConditions.add(c1);
                        List<Condition> experimentConditions = new ArrayList<>();
                        HasExperimentTypeCondition c2 = new HasExperimentTypeCondition("SENSITIVITYANALYSIS");
                        experimentConditions.add(c2);

                        //Trigger the rule
                        BuildingSimulationActivityTrigger buildingSimulationActivityTrigger = new BuildingSimulationActivityTrigger(mainController);
                        buildingSimulationActivityTrigger.trigger(activity, analyzingSimulationModelFilter, analyzingSimulationModelGenerator,
                                                                modelConditions, experimentConditions);

                        //This is necessary to continue the CaseStudy, because experiments are not executed automatically:
                        buildingSimulationActivityTrigger.addObserver(mainController.getAbstractCaseStudyController());
                    }
                } else if (activity.getActivityType() == ActivityType.CALIBRATING_SIMULATION_MODEL) {
                    if (ruleCalibratingValidatingEnabled) {
                        System.out.println("-------------------Automatic Experiment Generation started-----------------");
                        System.out.println("Trigger activity detected: Calibrating Simulation Model " + activity.getName());
                        //When triggered, a VSM Activity will be generated/reused:
                        ValidatingSimulationModelFilter validatingSimulationModelFilter = new ValidatingSimulationModelFilter(mainController);
                        ValidatingSimulationGenerator validatingSimulationGenerator = new ValidatingSimulationGenerator(mainController);

                        List<Condition> modelConditions = new ArrayList<>();
                        List<Condition> experimentConditions = new ArrayList<>();

                        CalibratingSimulationActivityTrigger calibratingSimulationActivityTrigger = new CalibratingSimulationActivityTrigger(mainController);
                        calibratingSimulationActivityTrigger.trigger(activity, validatingSimulationModelFilter, validatingSimulationGenerator,
                                                                        modelConditions, experimentConditions);

                        //This is necessary to continue the CaseStudy, because experiments are not executed automatically:
                        calibratingSimulationActivityTrigger.addObserver(mainController.getAbstractCaseStudyController());
                    }
                    else if (ruleCalibratingCrossValidatingEnabled) {
                        System.out.println("--------------Automatic Experiment Generation started---------------");
                        System.out.println("Trigger activity detected: Calibrating Simulation Model " + activity.getName()
                                + " in " + activity.getStudyName());

                        //When triggered, an ASM activity will be reused
                        AnalyzingSimulationModelFilter analyzingSimulationModelFilter = new AnalyzingSimulationModelFilter(mainController);
                        //And a VSM activity will be generated
                        ValidatingSimulationGenerator validatingSimulationGenerator = new ValidatingSimulationGenerator(mainController);
                        //Based on the following conditions
                        List<Condition> modelConditions = new ArrayList<>();
                        IsBasedOnCondition c1 = new IsBasedOnCondition();
                        modelConditions.add(c1);
                        AreStudiesNotEqualCondition c2 = new AreStudiesNotEqualCondition();
                        modelConditions.add(c2);
                        IsValidatedCondition c3 = new IsValidatedCondition();
                        modelConditions.add(c3);
                        List<Condition> experimentConditions = new ArrayList<>();

                        //Trigger the rule
                        CalibratingSimulationActivityTrigger calibratingSimulationActivityTrigger = new CalibratingSimulationActivityTrigger(mainController);
                        calibratingSimulationActivityTrigger.trigger(activity, analyzingSimulationModelFilter, validatingSimulationGenerator,
                                modelConditions, experimentConditions);

                        //This is necessary to continue the Case Study because experiments are not executed automatically
                        calibratingSimulationActivityTrigger.addObserver(mainController.getAbstractCaseStudyController());
                    }
                } else if (activity.getActivityType() == ActivityType.COMPOSING_SIMULATION_MODEL) {
                    if (ruleComposingValidatingEnabled) {
                        System.out.println("--------------Automatic Experiment Generation started---------------");
                        System.out.println("Trigger activity detected: Composing Simulation Model " + activity.getName()
                                + " in " + activity.getStudyName());

                        ValidatingSimulationModelFilter validatingSimulationModelFilter = new ValidatingSimulationModelFilter(mainController);
                        ValidatingSimulationGenerator validatingSimulationModelGenerator = new ValidatingSimulationGenerator(mainController);

                        //Conditions
                        List<Condition> modelConditions = new ArrayList<>();
                        List<Condition> experimentConditions = new ArrayList<>();

                        ComposingSimulationActivityTrigger composingSimulationActivityTrigger = new ComposingSimulationActivityTrigger(mainController);
                        composingSimulationActivityTrigger.trigger(activity, validatingSimulationModelFilter, validatingSimulationModelGenerator,
                                modelConditions, experimentConditions);

                        //This is necessary to continue the CaseStudy, because experiments are not executed automatically:
                        composingSimulationActivityTrigger.addObserver(mainController.getAbstractCaseStudyController());
                    }
                }
            }
        }
    }

    //---------------------------------setters:-------------------------------------------------------------------------

    public static void setTriggerAutomatically(boolean triggerAutomatically) {
        TriggerController.triggerAutomaticallyWhenInserted = triggerAutomatically;
    }

    public static void setRuleEnabled(String ruleName, boolean ruleEnabled) {
        if(ruleName.equals("BuildingTriggersCalibrating")) {
            setBuildingSimulationTriggersCalibratingSimulation(ruleEnabled);
        } else if(ruleName.equals("CalibratingTriggersCrossvalidation")) {
            setCalibratingSimulationModelTriggersCrossValidatingSimulationModel(ruleEnabled);
        } else if(ruleName.equals("CalibratingTriggersValidating")) {
            setCalibratingSimulationModelTriggersValidatingSimulationModel(ruleEnabled);
        } else if(ruleName.equals("BuildingTriggersAnalyzing")) {
            setBuildingSimulationModelTriggersAnalyzingSimulationModel(ruleEnabled);
        } else if(ruleName.equals("ComposingTriggersValidating")) {
            setComposingSimulationTriggersValidatingSimulation(ruleEnabled);
        }
    }

    public static void setBuildingSimulationTriggersCalibratingSimulation(boolean enabled) {
        TriggerController.ruleRefiningCalibratingEnabled = enabled;
    }

    public static void setCalibratingSimulationModelTriggersValidatingSimulationModel(boolean enabled) {
        TriggerController.ruleCalibratingValidatingEnabled = enabled;
    }

    public static void setCalibratingSimulationModelTriggersCrossValidatingSimulationModel(boolean enabled) {
        TriggerController.ruleCalibratingCrossValidatingEnabled = enabled;
    }

    public static void setBuildingSimulationModelTriggersAnalyzingSimulationModel(boolean enabled) {
        TriggerController.ruleRefiningAnalysisEnabled = enabled;
    }

    public static void setComposingSimulationTriggersValidatingSimulation(boolean enabled) {
        TriggerController.ruleComposingValidatingEnabled = enabled;
    }
}
