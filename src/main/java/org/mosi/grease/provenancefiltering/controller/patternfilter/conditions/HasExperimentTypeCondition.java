package org.mosi.grease.provenancefiltering.controller.patternfilter.conditions;

import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;

public class HasExperimentTypeCondition extends Condition {

    String fixquery = "e.type = '";

    String subquery = "";

    public HasExperimentTypeCondition(String experimentType) {
        super();
        subquery = fixquery + experimentType + "'";
    }

    @Override
    public String getQueryString(SimulationModel modelUsedByActivity) {
        return subquery;
    }
}
