package org.mosi.grease.backend;

import org.mosi.grease.experimentgeneration.controller.MainExperimentController;

import freemarker.template.Template;

import org.mosi.grease.experimentgeneration.experiment.Experiment;
import org.mosi.grease.experimentgeneration.model.ExperimentModel;
import org.json.JSONObject;
import org.mosi.grease.provenancefiltering.controller.MainController;

import java.util.HashMap;

/**
 * Created by Marcus on 17.12.2018.
 */
public class PythonBackendController {
    private HashMap experimentParameter;
    private BackendController backendController;

    public PythonBackendController(BackendController backendController) {
        this.backendController = backendController;
    }

    public HashMap getExperimentParameter() {
        return experimentParameter;
    }

}
