package org.mosi.grease.backend;

import org.apache.commons.io.IOUtils;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;

public class RJuliaBackendController {

    private BackendController backendController;
    private final String dataFolder = "results";
    private final String designFolder = "designs";
    private final String passwordFile = "pwd.config";
    private String password;

    public RJuliaBackendController(BackendController backendController) {
        this.backendController = backendController;
        if(backendController.isWindows()) {
            try {
                this.password = new String(Files.readAllBytes(Paths.get(passwordFile)), StandardCharsets.UTF_8);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String executeRJuliaExperimentAndGetResult(File experimentFile, File modelFile, String modelProvName, boolean isRemote) {
        if(isRemote) {
            if(modelProvName.equals("m3") || modelProvName.equals("m4")) {
                return remoteRJuliaExecution(experimentFile, modelFile, modelProvName);
            } else {
                return null;
            }
        }
        else {
            //Local execution of Rjulia scripts not supported.
            return null;
        }
    }

    private String remoteRJuliaExecution(File experimentFile, File modelFile, String modelProvName) {
        //unpack properties
        Properties remoteProperties = backendController.getRemoteProperties();
        String server = remoteProperties.getProperty("server");
        String sshport = remoteProperties.getProperty("sshport");
        String scpport = remoteProperties.getProperty("scpport");
        String user = remoteProperties.getProperty("user");
        //String password = remoteProperties.getProperty("password");
        String workspace = remoteProperties.getProperty("workspace");
        // get relevant folders
        String experimentFolder = experimentFile.getParent();
        String modelFolder = modelFile.getParent();
        // prepare folder in workspace with experiment ID
        String experimentFolderName = experimentFile.getParentFile().getName();
        String experimentFileName = experimentFile.getName();
        String experimentName = experimentFileName.replaceFirst("[.][^.]+$", "");
        createFolder(experimentName, server, sshport, user, workspace);
        // send experiment folder
        sendFolder(experimentFolder, experimentName, server, scpport, user, workspace);
        // send model folder
        sendFolder(modelFolder, experimentName, server, scpport, user, workspace);
        // run experiment
        buildAndRunExecScript(experimentFileName, experimentName, experimentFolderName,
                server, sshport, user, workspace, modelProvName);
        String resultsPath = getResultFromExecution(experimentFolder, experimentFolderName, experimentName, server, scpport, user, workspace);
        // do not clear. might want to use for manual experiments.
        //clearRemoteDirectory(experimentName, server, sshport, user, workspace);
        return resultsPath;
    }

    private void clearRemoteDirectory(String experimentName, String server, String port, String user, String workspace) {
        String command = "";
        if(backendController.isUnix()) {
            // sshpass -f <passwordFile> ssh <user>@<server> "rm -rf <workspace>/<experimentName>"
            command = "sshpass -f " + passwordFile + " ssh -p " + port + " " + user + "@" + server +
                    " \"rm -rf " + workspace + "/" + experimentName + "\"";
        } else if(backendController.isWindows()) {
            // plink -batch -ssh <user>@<server> -P 22 -pw <password> "rm -rf <workspace>/<experimentName>"
            command = "plink -batch -ssh " + user + "@" + server + " -P " + port + " -pw " + password +
                    " \"rm -rf " + workspace + "/" + experimentName + "\"";
        }
        waitForProcess(startProcess(command));
    }

    private void createFolder(String experimentName, String server, String port, String user, String workspace) {
        String command = "";
        if(backendController.isUnix()) {
            // sshpass -f <passwordFile> ssh <user>@<server> "mkdir <workspace>/<experimentName>"
            command = "sshpass -f " + passwordFile + " ssh -p " + port + " " + user + "@" + server +
                    " \"mkdir -p " + workspace + "/" + experimentName + "\"";
        } else if(backendController.isWindows()) {
            // plink -batch -ssh <user>@<server> -P 22 -pw <password> "mkdir <workspace>/<experimentName>"
            command = "plink -batch -ssh " + user + "@" + server + " -P " + port + " -pw " + password +
                    " \"mkdir -p " + workspace + "/" + experimentName + "\"";
        }
        waitForProcess(startProcess(command));
    }

    private Process startProcess(String command) {
        try {
            ProcessBuilder pb;
            if(backendController.isWindows()) {
                pb = new ProcessBuilder("cmd", "/C", command);
            } else {
                pb = new ProcessBuilder("/bin/bash", "-c", command);
            }
            pb.inheritIO();
            return pb.start();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void sendFolder(String folder, String experimentName, String server,
                                    String port, String user, String workspace) {
        String command = "";
        if(backendController.isUnix()) {
            // sshpass -f <passwordFile> scp -P <port> -rp <folder> <user>@<server>:<workspace>/<folderName>
            command = "sshpass -f " + passwordFile + " scp -P " + port + " -rp " + folder + " " +
                    user + "@" + server + ":" +  workspace + "/" + experimentName;
        } else if(backendController.isWindows()) {
            // pscp -r -batch -scp -P <port> -pw <password> <folder> <user>@<server>:<workspace>/<folderName>
            command = "pscp -r -batch -scp -P " + port + " -pw " + password + " " +
                    folder + " " +
                    user + "@" + server + ":" +  workspace + "/" + experimentName;
        }
        waitForProcess(startProcess(command));
    }

    private void waitForProcess(Process process) {
        try {
            process.waitFor();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void buildAndRunExecScript(String experimentFileName, String experimentName, String experimentFolderName,
                                         String server, String port, String user, String workspace, String modelProvName) {
        String sendCommand = "";
        String runCommand = "";
        if(backendController.isUnix()) {
            // execution script exec wrapps the experiment
            // makes sure there is no finished file with same name, start experiment execution, create new finished file,
            // write everything to exec file, make executable
            sendCommand = "sshpass -f " + passwordFile + " ssh -p " + port + " " + user + "@" + server + " 'echo \" " +
                    " if test -f finished; then \n rm finished \n fi \n cd " + experimentFolderName + " \n chmod a+x " + experimentName +
                    ".sh \n ./" + experimentName + ".sh " + modelProvName + " \n cd .. \n" +
                    "bash \n" +
                    "ID=$(qstat -f | grep -B 1 'Job_Name = run_model.pbs' | grep 'Job Id' | awk '{print $3}'); \n" +
                    "while qstat -c $ID &> /dev/null; do \n" +
                    "   sleep 1800; \n" +
                    "done; \n" +
                    "touch finished \n" +
                    " \" " +
                    "> " + workspace + "/" + experimentName + "/exec.sh ; chmod a+x " + workspace + "/" + experimentName + "/exec.sh'";
            // run exec file
            // sshpass -f <password> ssh -p <port> <user>@<server> "cd <workspace>/<experimentName> && bash -l -c ./exec.sh >/dev/null 2>&1 & disown -h"
            runCommand = "sshpass -f " + passwordFile + " ssh -p " + port + " " + user + "@" + server + " 'cd " + workspace + "/" + experimentName
                    + " && chmod a+x exec.sh && bash -l -c \"./exec.sh >/dev/null 2>&1 & disown -h\"'";
        } else if(backendController.isWindows()) {
            sendCommand = "plink -batch -ssh " + user + "@" + server + " -P " + port + " -pw " + password + " \"echo ' " +
                    "if test -f finished; then \\n rm finished \\n fi \\n" +
                    "cd " + experimentFolderName + " \\n chmod a+x " + experimentName +
                    ".sh \\n ./" + experimentName + ".sh " + modelProvName + " \\n cd .. \\n " +
                    "bash \\n" +
                    "ID=$(qstat -f | grep -B 1 'Job_Name = run_model.pbs' | grep 'Job Id' | awk '{print $3}'); \\n" +
                    "while qstat -c $ID &> /dev/null; do \\n" +
                    "   sleep 1800; \\n" +
                    "done; \\n" +
                    "touch finished \\n" +
                    "' " +
                    "> " + workspace + "/" + experimentName + "/exec.sh ; chmod a+x " + workspace + "/" + experimentName + "/exec.sh\"";
            // plink -batch -ssh <user>@<server> -P <port> -pw <password> 'cd <workspace>/<experimentName> && bash -l -c
            // "./exec.sh >/dev/null 2>&1 & disown -h"'
            runCommand = "plink -batch -ssh " + user + "@" + server + " -P " + port + " -pw " + password + " \"cd " +
                    workspace + "/" + experimentName + " && chmod a+x exec.sh && bash -l -c './exec.sh >/dev/null 2>&1 & disown -h'\"";
        }
        waitForProcess(startProcess(sendCommand));
        waitForProcess(startProcess(runCommand));
    }

    private String getResultFromExecution(String experimentFolder, String experimentFolderName, String experimentName, String server, String port,
                                        String user, String workspace) {

        //command for checking if file exists remotely
        String checkCommand = "";
        //command for retrieving result folder
        String getCommand = "";
        //command for retrieving designs folder
        String getDesignCommand = "";
        if(backendController.isUnix()) {
            //sshpass -f <passwordFile> ssh -p <port> <user>@<server> test -e <workspace>/<experimentName>/finished
            checkCommand = "sshpass -f " + passwordFile + " ssh -p " + port + " " + user + "@" + server + " \" test -e " + workspace
                    + "/" + experimentName + "/" + "finished \"";
            //sshpass -f <passwordFile> scp -P <port> -rp <user>@<server>:<workspace>/<experimentName>/<dataFolder> <experimentFolder>/<dataFolder>
            getCommand = "sshpass -f " + passwordFile + " scp -P " + port + " -rp " + user + "@" + server + ":"
                    + workspace + "/" + experimentName + "/" + experimentFolderName + "/" + dataFolder + "/ " + experimentFolder + "/" + dataFolder;
            getDesignCommand = "sshpass -f " + passwordFile + " scp -P " + port + " -rp " + user + "@" + server + ":"
                    + workspace + "/" + experimentName + "/" + experimentFolderName + "/" + designFolder + "/ " + experimentFolder + "/" + designFolder;
        } else if(backendController.isWindows()) {
            //plink -batch -ssh <user>@<server> -P <port> -pw <password> test -e <workspace>/<experimentName>/finished
            checkCommand = "plink -batch -ssh " + user + "@" + server + " -P " + port + " -pw " + password + " \" test -e " + workspace
                    + "/" + experimentName + "/" + "finished \"";
            // pscp -r -batch -scp -P <port> -pw <password> <user>@<server>:<workspace>/<experimentName>/<dataFolder> <experimentFolder>/<dataFolder>
            getCommand = "pscp -r -batch -scp -P " + port + " -pw " + password + " " + user + "@" + server + ":" +
                    workspace + "/" + experimentName + "/" + experimentFolderName + "/" + dataFolder + "/ " + experimentFolder + "/" + dataFolder;
            getDesignCommand = "pscp -r -batch -scp -P " + port + " -pw " + password + " " + user + "@" + server + ":" +
                    workspace + "/" + experimentName + "/" + experimentFolderName + "/" + designFolder + "/ " + experimentFolder + "/" + designFolder;
        }
        //check for finished file every 10 minutes, max. 72:00 h
        int counter = 0;
        int interval = 600000;
        while(counter * interval < 259200000) {
            Process process = startProcess(checkCommand);
            if(process != null) {
                System.out.println("Waiting for simulation results");
                waitForProcess(process);
                if(process.exitValue() == 0) {
                    waitForProcess(startProcess(getDesignCommand));
                    waitForProcess(startProcess(getCommand));
                    System.out.println("Retrieving simulation results");
                    return experimentFolder + "/" + dataFolder;
                }
            }
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            counter++;
        }
        return experimentFolder + "/" + dataFolder;
    }
}