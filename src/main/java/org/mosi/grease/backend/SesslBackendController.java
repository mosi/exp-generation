package org.mosi.grease.backend;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.mosi.grease.experimentgeneration.controller.MainExperimentController;
import org.mosi.grease.experimentgeneration.model.ExperimentModel;
import org.json.JSONObject;
import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.model.entities.experiments.Experiment;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

/**
 * Created by Marcus on 17.12.2018.
 */
public class SesslBackendController{
    BackendController backendController;

    public SesslBackendController(BackendController backendController) {
        this.backendController = backendController;
    }

    protected boolean checkFileExtension(String filePath) {
        File file = new File(filePath);
        String extension = FilenameUtils.getExtension(file.getName());
        if (extension.equals("mlrj") || extension.equals("scala")) {
            //System.out.println("File Extension : " + extension + " valid");
            return true;
        } else {
            System.out.println("Invalid File Extension : " + extension);
            return false;
        }
    }

    public String executeSESSLExperimentAndGetResult(File experimentFile, File modelFile, boolean isRemote){
        if(isRemote) {
            //Remote execution of SESSL scripts not supported.
            return null;
        }
        else {
            return localSESSLExecution(experimentFile, modelFile);
        }
    }

    private String localSESSLExecution(File experimentFile, File modelFile) {
        try {
            Process process = executeSESSLExperiment(experimentFile, modelFile);
            process.waitFor();
            return getResultFromExecution();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Process executeSESSLExperiment(File experimentFile, File modelFile) throws IOException {
        if(checkFileExtension(experimentFile.getPath()) && checkFileExtension(modelFile.getPath())) {
            copyModelFileToBindingsPath(modelFile.toPath());
            experimentFile = copyExperimentFileToBindingsPath(experimentFile.toPath());
            adjustPomFile(experimentFile);
            if (backendController.isWindows()) {
                return executeExperimentBatch();
            } else if (backendController.isUnix()) {
                return executeExperimentShell();
            }
        }
        return null;
    }

    public String getResultFromExecution() throws InterruptedException {
        String pathQuickstartDict = "src" + File.separator + "main" + File.separator + "resources" + File.separator +
                "bindings" + File.separator + "sessl-quickstart" + File.separator;
        File quickstartDictionaryPath = new File(pathQuickstartDict);
        String contents[] = quickstartDictionaryPath.list();

        LocalDate date = LocalDate.now();
        int month = date.getMonthValue();
        String monthString;
        if(month<10)
            monthString = "0" + month;
        else
            monthString = String.valueOf(month);
        int day = date.getDayOfMonth();
        String dayString;
        if(day<10)
            dayString = "0" + day;
        else
            dayString = String.valueOf(day);

        //get result folder that start with current date
        List<String> contentsList = new java.util.ArrayList<>(Arrays.asList(contents));
        String prefix = "results-"+ date.getYear() + "-" + monthString + "-" + dayString;
        contentsList.removeIf(c -> !c.startsWith(prefix));
        //sort results by name and return last item
        Collections.sort(contentsList);
        String content = contentsList.get(contentsList.size() - 1);
        return new File(quickstartDictionaryPath.getPath() + File.separator + content).toPath().toString();

        //return null;
    }

    public Process executeExperimentBatch() throws IOException {
        ProcessBuilder processBuilder = new ProcessBuilder("cmd.exe", "/C", "run.bat");
        File dir = new File(backendController.BINDINGS_PATH + "sessl-quickstart" + File.separator);
        if (dir.exists()){
            System.out.println("Backend binding found.");
        }
        processBuilder.directory(dir);

        return processBuilder.start();
    }

    public File copyFileToBindingsPath(Path filePath, File bindingsPath){

        File targetAtBindingsPath = new File(bindingsPath + File.separator + filePath.getFileName());
        File originalFile = filePath.toFile();
        if(! (targetAtBindingsPath.getPath().contains(originalFile.getPath()) || originalFile.getPath()
                .contains(targetAtBindingsPath.getPath()))){
            try {
                FileUtils.copyFile(originalFile, targetAtBindingsPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return targetAtBindingsPath;
        }else {
            return filePath.toFile();
        }
    }

    public File copyModelFileToBindingsPath(Path modelPath){
        File bindingsPath = new File(backendController.BINDINGS_PATH + "sessl-quickstart"
                + File.separator + "models");
        return copyFileToBindingsPath(modelPath, bindingsPath);
    }

    public File copyExperimentFileToBindingsPath(Path experimentPath){
        File bindingsPath = new File(backendController.BINDINGS_PATH + "sessl-quickstart"
                + File.separator + "experiments");
        return copyFileToBindingsPath(experimentPath, bindingsPath);
    }

    public Process executeExperimentShell() {
        try {
            ProcessBuilder pb = new ProcessBuilder("sh", "-c", "./run.sh");
            pb.redirectErrorStream();
            pb.directory(new File(backendController.BINDINGS_PATH + "sessl-quickstart"));
            return pb.start();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void adjustPomFile(File experimentFile) throws IOException {
        String pomString = new String(Files.readAllBytes(Paths.get(backendController.BINDINGS_PATH + "sessl-quickstart/pom.xml")));
        String[] split = pomString.split("\n");
        for (int i = 0; i < split.length; i++) {
            if (split[i].contains("scriptFile")) {
                split[i] = "\t\t\t\t\t<scriptFile>" + "experiments/" + experimentFile.getName() + "</scriptFile>";
            }
        }
        List<String> wordList = Arrays.asList(split);
        String joined = StringUtils.join(wordList, "\n");
        File pomFile = new File(backendController.BINDINGS_PATH + "sessl-quickstart/pom.xml");
        BufferedWriter writer = new BufferedWriter(new FileWriter(pomFile));
        writer.write(joined);
        writer.close();
    }

}
