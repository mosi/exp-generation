package org.mosi.grease.backend;


import org.mosi.grease.experimentgeneration.controller.MainExperimentController;
import org.mosi.grease.experimentgeneration.model.BackendType;
import org.mosi.grease.experimentgeneration.model.ExperimentModel;
import org.mosi.grease.provenancefiltering.controller.MainController;
import org.mosi.grease.provenancefiltering.model.entities.SimulationModel;
import org.mosi.grease.provenancefiltering.model.entities.experiments.Experiment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Properties;

/**
 * Created by Marcus on 20.02.2019.
 */
public class BackendController{
    private final String OS = System.getProperty("os.name").toLowerCase();
    MainExperimentController mainExperimentController;
    MainController mainController;
    private PythonBackendController pythonBackendController;
    private SesslBackendController sesslBackendController;
    private YamlBackendController yamlBackendController;
    private RJuliaBackendController rjuliaBackendController;
    public final static String BINDINGS_PATH = "."+File.separator+"src"+File.separator+"main"+File.separator+"resources"+File.separator+"bindings"+File.separator;

    public BackendController(MainExperimentController mainExperimentController) {
        this.mainExperimentController = mainExperimentController;
        pythonBackendController = new PythonBackendController(this);
        sesslBackendController = new SesslBackendController(this);
        yamlBackendController = new YamlBackendController(this);
        rjuliaBackendController = new RJuliaBackendController(this);
    }

    public String executeExperiment(File experiment, File experimentModel, String modelProvName, BackendType backendType, boolean isRemote) throws IOException {
        boolean experimentFileExists = checkFileExistence(experiment.getPath());
        boolean modelFileExists = checkFileExistence(experimentModel.getPath());
        if(experimentFileExists && modelFileExists) {
            switch (backendType) {
                case SESSL: {
                    return sesslBackendController.executeSESSLExperimentAndGetResult(experiment, experimentModel, isRemote);
                }
                case PYTHON:
                case YAML: {
                    break;
                }
                case R_JULIA: {
                    return rjuliaBackendController.executeRJuliaExperimentAndGetResult(experiment, experimentModel, modelProvName, isRemote);
                }
            }
        }
        return null;
    }

    protected boolean checkFileExistence(String filePath) {
        try {
            File file = new File((filePath));
            boolean exists = file.getAbsoluteFile().exists();
            if (exists) {
                //System.out.println("File Existence checked");
                return true;
            } else {
                System.out.println("Incorrect File Path, File does not Exist");
                return false;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void adjustModelPath() {
        adaptModelPath(mainExperimentController.getExperimentModel());
    }

    //sessl controller
    protected void adaptModelPath(ExperimentModel experimentModel) {
        String modelPath = experimentModel.getExperimentSpecificationInJSON().get("modelPath").toString();
        String[] quickstarts = modelPath.split("quickstart");
        experimentModel.getExperimentSpecificationInJSON().put("modelPath", "." + quickstarts[1]);
    }

    public BackendType inferExperimentType(SimulationModel model) {
        String modelingTool = model.getTool();
        switch(modelingTool) {
            case "ML-Rules": {
                return BackendType.SESSL;
            }
            case "Julia": {
                return BackendType.R_JULIA;
            }
            default: {
                System.out.println("No backend binding available");
                return null;
            }
        }
    }

    public boolean isWindows() {
        return (OS.contains("win"));
    }

    public boolean isUnix() {
        return (OS.contains("nix") || OS.contains("nux") || OS.indexOf("aix") > 0);
    }

    public PythonBackendController getPythonBackendController() {
        return pythonBackendController;
    }

    public SesslBackendController getSesslBackendController() {
        return sesslBackendController;
    }

    public YamlBackendController getYamlBackendController() {
        return yamlBackendController;
    }

    public void setMainController(MainController mainController) {
        this.mainController = mainController;
    }

    public Properties getRemoteProperties() {
        Properties prop = new Properties();
        String fileName = "remote.config";
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            prop.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop;
    }
}
