package org.mosi.grease.backend;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.mosi.grease.experimentgeneration.controller.MainExperimentController;
import org.mosi.grease.experimentgeneration.model.ExperimentModel;
import org.json.JSONObject;
import org.mosi.grease.provenancefiltering.controller.MainController;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;

public class YamlBackendController{

    private BackendController backendController;

    public YamlBackendController(BackendController backendController) {
        this.backendController = backendController;
    }

}
