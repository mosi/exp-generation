package org.mosi.grease.experimentgeneration.view.templates;


import org.mosi.grease.experimentgeneration.controller.TemplateController;
import org.mosi.grease.experimentgeneration.extraction.ExtractionController;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.net.URISyntaxException;

/**
 * Created by Marcus on 07.05.2017.
 */
public class TemplateEditor extends JPanel {
    private TemplateController controller;
    private ExtractionController extractionController;
    private JTextArea resultTextArea;
    private TemplateTabPane templateTabPane;
    private JFileChooser chooser;
    private JButton evalDocumenationBtn;
    private boolean templateSelected;
    private boolean documentationSelected;

    public TemplateEditor(TemplateController controller, ExtractionController extractionController, JTextArea resultTextArea) {
        this.controller = controller;
        this.extractionController = extractionController;
        this.resultTextArea = resultTextArea;
        this.templateTabPane = new TemplateTabPane();
        this.evalDocumenationBtn = new JButton();
        this.templateSelected = false;
        this.documentationSelected = false;
        this.setup();
    }

    public JTextArea getResultTextArea() {
        return resultTextArea;
    }

    private void setup() {
        // Set Templateeditor layout
        this.setLayout(new BorderLayout());

        // Setup EditorPanel
        JPanel editorPanel = new JPanel();
        editorPanel.setLayout(new GridBagLayout());
        editorPanel.setBorder(BorderFactory.createTitledBorder("Editor"));

        // Setup tree
        DefaultMutableTreeNode templateTreeRoot = controller.buildTemplateTree();
        JTree templateTree = new JTree(templateTreeRoot);
        templateTree.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) templateTree.getLastSelectedPathComponent();
                    if (node == null) {
                        return;
                    }
                    System.out.println("Selected node: " + node.toString());
                    controller.openTemplate(node);
                    templateSelected = true;
                    if(templateSelected && documentationSelected) {
                    	evalDocumenationBtn.setEnabled(true);
                    }
                }
            }
        });

        // Setup treescrollpane
        JScrollPane treeScrollPane = new JScrollPane(templateTree);
        treeScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
        treeScrollPane.setBorder(BorderFactory.createTitledBorder("Templates"));

        //Setup template editor bar
        JPanel templateEditorBar = new JPanel();
        templateEditorBar.setLayout(new FlowLayout());
        
        
        // Setup file chooser for documentation selection
        chooser = new JFileChooser();
        File documentationFolder = null;
        try {
			documentationFolder = new File(ClassLoader.getSystemResource("Documentations").toURI());
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		}
        chooser.setCurrentDirectory(documentationFolder);
        FileNameExtensionFilter filter = new FileNameExtensionFilter("tabular","csv");
        chooser.setFileFilter(filter);
        
     // Setup list of selected documentation
        JList<String> selectedDocumentationsList = new JList<String>();
        DefaultListModel<String> listModel = new DefaultListModel<String>();
        selectedDocumentationsList.setModel(listModel);
        JScrollPane listScroller = new JScrollPane(selectedDocumentationsList);
        
        // Setup button for documentation file selection
        JButton chooseDocumentationBtn = new JButton("Add Documentation");
        chooseDocumentationBtn.addActionListener(e -> {
        int returnVal = this.chooser.showOpenDialog(this);
        if(returnVal == JFileChooser.APPROVE_OPTION) {
        	this.extractionController.addFile(this.chooser.getSelectedFile());
        	listModel.addElement(this.chooser.getSelectedFile().getName());
        	documentationSelected = true;
        	if(templateSelected && documentationSelected) {
            	evalDocumenationBtn.setEnabled(true);
            }
        	System.out.println("You chose to open this file: " +
        			this.chooser.getSelectedFile().getName());
          }
        });
        chooseDocumentationBtn.setVisible(true);
        
     // Setup button for documentation file selection
        JButton removeDocumentationBtn = new JButton("Remove Documentation");
        removeDocumentationBtn.addActionListener(e -> {
        	int index = selectedDocumentationsList.getSelectedIndex();
        	this.extractionController.removeFile(index);
        	((DefaultListModel<String>)selectedDocumentationsList.getModel()).remove(index);
        	if(extractionController.getNumberOfDocumentationFiles() < 1) {
        		documentationSelected = false;
        		evalDocumenationBtn.setEnabled(false);
        	}
        	if(templateSelected && documentationSelected) {
            	evalDocumenationBtn.setEnabled(true);
            }
        });
        removeDocumentationBtn.setVisible(true);
        
        // Setup button for documentation evaluation
        evalDocumenationBtn = new JButton("Extract Information");
        evalDocumenationBtn.addActionListener(e -> {
        	//extract values
            this.extractionController.extractInfo(this.controller);
            //Relate the extracted info to the input fields
            this.controller.presentExtractedValues();
        });
        evalDocumenationBtn.setVisible(true);
        evalDocumenationBtn.setEnabled(false);
        
        
        // Setup documentation selection pane
        JPanel documentationSelectionPanel = new JPanel();
        documentationSelectionPanel.setLayout(new BoxLayout(documentationSelectionPanel,BoxLayout.Y_AXIS));
        documentationSelectionPanel.add(listScroller);
        documentationSelectionPanel.add(chooseDocumentationBtn);
        documentationSelectionPanel.add(removeDocumentationBtn);
        documentationSelectionPanel.add(evalDocumenationBtn);

        //Setup show template editor checkbox
        JCheckBox showCheckBox = new JCheckBox("Show Editor");
        showCheckBox.setSelected(true);
//        showCheckBox.addItemListener(e -> {
//            if (showCheckBox.isSelected()) {
////                this.controller.showTemplateEditor();
//            } else {
////                this.hideEditor();
//            }
//        });

        // Setup render template Button
        JButton renderTemplateBtn = new JButton("Render Template");
        renderTemplateBtn.addActionListener(e -> {
            this.controller.renderTemplate();
        });
        renderTemplateBtn.setVisible(true);


        JButton generation = new JButton("Open Generation Pane");
        generation.setVisible(true);


        // Build template editorBar
        templateEditorBar.add(showCheckBox);
        templateEditorBar.add(renderTemplateBtn);
        templateEditorBar.add(generation);

        // Build Editor GUI
        this.add(treeScrollPane, BorderLayout.WEST);
        this.add(templateEditorBar, BorderLayout.SOUTH);
        this.add(templateTabPane, BorderLayout.CENTER);
        this.add(documentationSelectionPanel, BorderLayout.EAST);
    }


    //
    public void hideEditor() {
//        kindOfTemplates.setVisible(false);
//        templateTabEditor.setVisible(false);
//        creatTemplateButton.setVisible(false);
    }
//
//    public void showEditor() {
//        kindOfTemplates.setVisible(true);
//        if (templateTabEditor.getTemplateClassesPanel().size() > 0) {
//            creatTemplateButton.setVisible(true);
//
//        }
//        templateTabEditor.setVisible(true);
//    }
//


    public TemplateTabPane getTemplateTabPane() {
        return templateTabPane;
    }

    public void update(String experimentText) {
        this.resultTextArea.setText(experimentText);
    }

    public void reset() {
        this.templateTabPane.deleteAllTabs();
    }
}
