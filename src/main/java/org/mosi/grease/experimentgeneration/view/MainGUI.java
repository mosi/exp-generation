package org.mosi.grease.experimentgeneration.view;

import org.mosi.grease.experimentgeneration.controller.TemplateController;
import org.mosi.grease.experimentgeneration.extraction.ExtractionController;
import org.mosi.grease.experimentgeneration.view.templates.TemplateEditor;

import javax.swing.*;
import java.awt.*;

public class MainGUI extends JFrame {
    private TemplateController controller;
    private ExtractionController extractionController;

    public MainGUI() {
        this.controller = new TemplateController();
        this.extractionController = new ExtractionController();
        this.setup();
    }

    private void setup() {
        this.setTitle("Experiment-Generator");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());

        JTextArea resultText = new JTextArea();
        JScrollPane resultTextScroll = new JScrollPane(resultText);
        resultTextScroll.setBorder(BorderFactory.createTitledBorder("Experiment Text"));
        resultTextScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        TemplateEditor editor = new TemplateEditor(controller, extractionController, resultText);
        editor.setBorder(BorderFactory.createTitledBorder("Template Editor"));
        controller.setEditor(editor);

        this.add(editor, BorderLayout.SOUTH);
        this.add(resultTextScroll, BorderLayout.CENTER);
        this.setSize(1024, 768);
        this.setVisible(true);
    }


}
