package org.mosi.grease.experimentgeneration.view.templates;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EditorInputContainer extends JPanel {
    private List<JComponent> components;
    private JPanel menuBar;
    private Class type;

    public EditorInputContainer(Class type) {
        this.components = new ArrayList<>();
        this.type = type;
        this.setup();
    }

    private void setup() {
        LayoutManager manager = new BoxLayout(this, BoxLayout.PAGE_AXIS);
        this.setLayout(manager);

        menuBar = new JPanel();
        JButton addBtn = new JButton("+");
        addBtn.addActionListener(e -> this.addNewComponent());
        JButton deleteBtn = new JButton("-");
        deleteBtn.addActionListener(e -> this.deleteLastComponent());
        menuBar.add(addBtn);
        menuBar.add(deleteBtn);
        menuBar.setVisible(false);

        this.add(menuBar);
    }

    private void deleteLastComponent() {
        JComponent lastAddedComponent = this.components.get(this.components.size() - 1);
        this.components.remove(lastAddedComponent);
        this.remove(lastAddedComponent);
        this.revalidate();
    }

    private void addNewComponent() {
        try {
            JComponent component = (JComponent) type.newInstance();
            this.addInput(component);
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public void addInput(JComponent input) {
        this.components.add(input);
        this.add(input);
        this.revalidate();
    }


    public void showMenuBar() {
        this.menuBar.setVisible(true);
        this.revalidate();
    }

    public List<Object> getValue() {
        List<Object> value = new ArrayList();
        for (JComponent component : this.components) {
            if (component instanceof JTextComponent) {
                value.add(((JTextComponent) component).getText());
            } else if (component instanceof JComboBox) {
                value.add(((JComboBox) component).getSelectedItem());
            } else if (component instanceof JCheckBox) {
                throw new RuntimeException("Get Value of Checkboxes is not implemented");
            }
        }

        return value;

    }
    
    public void setValue(String variable, Object extractedValues, Object defaultValues) {
    	
    	boolean isList = false;
    	Iterator<String> it = null;
    	if(extractedValues instanceof String) {
    		isList = false;
    	}
    	else if(extractedValues.getClass() == ArrayList.class) {
    		isList = true;
    		it = ((ArrayList<String>)extractedValues).iterator();
    	}
    	 
    	//existing fields
        for (JComponent component : this.components) {
            if (component instanceof JTextComponent) {
            	if(!isList && !extractedValues.equals("")) {
            		((JTextComponent) component).setText((String)extractedValues);
            	}
            	else if(!isList && !((ArrayList<String>)defaultValues).isEmpty()) {
            		((JTextComponent) component).setText(((ArrayList<String>)defaultValues).get(0));
            	}
            	else if(isList) {
            		if(it.hasNext()) {
            			((JTextComponent) component).setText(it.next());	
            		}
            	}
                
            } else if (component instanceof JComboBox) {
            	if(!isList) {
            		if(!extractedValues.equals("")) {
            			((JComboBox) component).addItem((String)extractedValues);
            		}	
            	}
            	else if(isList) {
            		while(it.hasNext()) {
            			((JComboBox) component).addItem(it.next());
            		}
            	}
                
            } else if (component instanceof JCheckBox) {
                throw new RuntimeException("Set Value of Checkboxes is not implemented");
            }
        }
        
        //add remaining items
		while(it != null && it.hasNext()) {
			this.createNewTextField(it.next());
		}
    }

	
	private void createEmptyComboBox() {
      JComboBox comboBox = new JComboBox();
      this.addInput(comboBox);
	}
	
	private void createNewTextField(String text) {
      JTextField textField = new JTextField(30);
      textField.setText(text);
      this.addInput(textField);
	}
}

