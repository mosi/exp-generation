package org.mosi.grease.experimentgeneration.view;

import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import org.mosi.grease.experimentgeneration.model.ExperimentModel;

/**
 * Created by Marcus on 15.07.2019.
 */
public class EditorView extends TabPane {
    private ExperimentModel experimentModel;
    private Tab jsonTab;
    private Tab experimentTab;
    private TextArea jsonTextArea;
    private TextArea experimentTextArea;

    public EditorView(ExperimentModel experimentModel) {
        this.experimentModel = experimentModel;
        jsonTab = new Tab("JSON-Specification");
        experimentTab = new Tab("Experiment-Specification");
        setup();
    }

    public TextArea getExperimentTextArea() {
        return experimentTextArea;
    }

    public void setExperimentTextArea(TextArea experimentTextArea) {
        this.experimentTextArea = experimentTextArea;
    }

    public ExperimentModel getExperimentModel() {
        return experimentModel;
    }

    public Tab getJsonTab() {
        return jsonTab;
    }

    public Tab getExperimentTab() {
        return experimentTab;
    }

    public TextArea getJsonTextArea() {
        return jsonTextArea;
    }

    private void setup() {
        jsonTab.setId("json");
        experimentTab.setId("experiment");
        this.getTabs().add(jsonTab);
        this.getTabs().add(experimentTab);
        
        this.setTabClosingPolicy(TabClosingPolicy.UNAVAILABLE);

        jsonTextArea = new TextArea();
        jsonTextArea.setEditable(false);

        jsonTab.setContent(jsonTextArea);

        experimentTextArea = new TextArea();
        experimentTextArea.setEditable(false);

        experimentTab.setContent(experimentTextArea);


    }
}
