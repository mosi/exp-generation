package org.mosi.grease.experimentgeneration.view;

import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import org.mosi.grease.experimentgeneration.model.BackendType;
import org.mosi.grease.experimentgeneration.model.ExperimentDesign;
import org.mosi.grease.experimentgeneration.model.ExperimentModel;
import org.mosi.grease.experimentgeneration.model.Domain;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.LinkedHashMap;

/**
 * Created by Marcus on 02.08.2019.
 */
public class ExperimentTab extends Tab {
    View view;
    private ExperimentModel experimentModel;
    private BorderPane borderLayout;
    private Label selectExperimentDesign;
    private ComboBox<String> experimentDesign;
    private ComboBox<String> domain;
    private ComboBox<String> backend;
    private TextField experimentName;
    private Label specifyExperimentName;
    private Label selectExperimentType;
    private Label selectBackend;
    private GridPane gridPane;
    private ExperimentDesign currentExperimentDesign;
    private BackendType backendType;

    public ExperimentTab(ExperimentModel experimentModel, View view) {
        this.experimentModel = experimentModel;
        this.setText("Meta Model");
        this.setClosable(false);
        this.gridPane = new GridPane();
        this.specifyExperimentName = new Label("Specify Experiment Name");
        this.selectExperimentType = new Label("Select Type of Simulation");
        this.selectExperimentDesign = new Label("Add Experiment Design (optional)");
        this.selectBackend = new Label("Select Backend");
        this.experimentName = new TextField();
        this.domain = new ComboBox<>();
        this.experimentDesign = new ComboBox<>();
        this.backend = new ComboBox<>();
        borderLayout = new BorderPane();

        this.view = view;
        setup();
    }

    private void setup() {
        domain.getItems().add("Stochastic Discrete-Event Simulation");
        domain.getItems().add("Virtual Prototyping of Heterogeneous Systems");
        domain.getItems().add("Finite Element Analysis");
        domain.setOnAction(event -> {
            if (domain.getSelectionModel().getSelectedItem().equals("Stochastic Discrete-Event Simulation")) {
                generateExperiment(Domain.STOCHASTIC_DISCRETE_EVENT);
            } else if (domain.getSelectionModel().getSelectedItem().equals("Virtual Prototyping of Heterogeneous Systems")) {
                generateExperiment(Domain.HETEROGENEOUS_SYSTEMS);
            } else if (domain.getSelectionModel().getSelectedItem().equals("Finite Element Analysis")) {
                generateExperiment(Domain.FINITE_ELEMENT);
            }
        });

        experimentDesign.getItems().add("");
        experimentDesign.getItems().add("Parameter Scan");
        experimentDesign.getItems().add("Local Sensitivity Analysis");
        experimentDesign.getItems().add("Global Sensitivity Analysis");
        experimentDesign.getItems().add("Statistical Model Checking");
        experimentDesign.getItems().add("Convergence Test");
        this.currentExperimentDesign = ExperimentDesign.NONE;
        this.experimentDesign.setOnAction(event -> {
            if(experimentDesign.getSelectionModel().getSelectedItem().equals("")){
                handleExperimentDesign(ExperimentDesign.NONE);
            } else if(experimentDesign.getSelectionModel().getSelectedItem().equals("Parameter Scan")){
                handleExperimentDesign(ExperimentDesign.PARAMETER_SCAN);
            } else if(experimentDesign.getSelectionModel().getSelectedItem().equals("Local Sensitivity Analysis")){
                handleExperimentDesign(ExperimentDesign.LOCAL_SENSITIVITY_ANALYSIS);
            } else if(experimentDesign.getSelectionModel().getSelectedItem().equals("Global Sensitivity Analysis")){
                handleExperimentDesign(ExperimentDesign.GLOBAL_SENSITIVITY_ANALYSIS);
            } else if(experimentDesign.getSelectionModel().getSelectedItem().equals("Statistical Model Checking")){
                handleExperimentDesign(ExperimentDesign.STATISTICAL_MODEL_CHECKING);
            } else if(experimentDesign.getSelectionModel().getSelectedItem().equals("Convergence Test")){
                handleExperimentDesign(ExperimentDesign.CONVERGENCE_TEST);
            }

        });

        backend.getItems().add("SESSL");
        backend.getItems().add("YAML");
        backend.getItems().add("Python");
        this.backendType = BackendType.SESSL;
        this.backend.setOnAction(event -> {
            if(backend.getSelectionModel().getSelectedItem().equals("SESSL")){
                this.backendType = BackendType.SESSL;
            } else if (backend.getSelectionModel().getSelectedItem().equals("YAML")){
                this.backendType = BackendType.YAML;
            } else if (backend.getSelectionModel().getSelectedItem().equals("Python")){
                this.backendType = BackendType.PYTHON;
            }
        });

        gridPane.add(specifyExperimentName, 0, 0);
        gridPane.add(experimentName, 1, 0);

        gridPane.add(selectExperimentType, 0, 1);
        gridPane.add(domain, 1, 1);

        gridPane.add(selectExperimentDesign, 0, 2);
        gridPane.add(experimentDesign, 1, 2);

        gridPane.add(selectBackend, 0, 3);
        gridPane.add(backend,1,3);

        gridPane.setPadding(new Insets(5, 5, 5, 5));
        gridPane.setHgap(10);

        borderLayout.setCenter(gridPane);
        this.setContent(borderLayout);

    }

    public void generateExperiment(Domain d) {
        experimentModel.clearData();
        experimentModel.setDomain(d);

        //Reset View
        view.clearView();

        String resource = "";
        if(d.equals(Domain.STOCHASTIC_DISCRETE_EVENT)) {
            resource = "schemas/des-schema.json";
        } else if(d.equals(Domain.HETEROGENEOUS_SYSTEMS)) {
            resource = "schemas/vp-schema.json"; //Virtual Prototyping of Heterogeneous Systems
        } else if(d.equals(Domain.FINITE_ELEMENT)) {
            resource = "schemas/fem-schema.json";
        }

        // this is the path within the jar file
        InputStream input = this.getClass().getResourceAsStream("/resources/" + resource);
        if (input == null) {
            // this is how we load file within editor (eg eclipse)
            input = this.getClass().getClassLoader().getResourceAsStream(resource);
        }

        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.getFactory().disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);
        try {
            map = mapper.readValue(input, LinkedHashMap.class);
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        view.generateInputElements(map);
        JSONObject jsonObject = new JSONObject(map);
        experimentModel.setSchemaFile(jsonObject);
    }

    public void setDomainValue(Domain d) {
        if(d.equals(Domain.STOCHASTIC_DISCRETE_EVENT)) {
            this.domain.getSelectionModel().select(0);
        } else if(d.equals(Domain.HETEROGENEOUS_SYSTEMS)) {
            this.domain.getSelectionModel().select(1);
        } else if(d.equals(Domain.FINITE_ELEMENT)) {
            this.domain.getSelectionModel().select(2);
        }
    }

    public ComboBox<String> getExperimentDesign() {
        return experimentDesign;
    }

    public void setExperimentDesign(ExperimentDesign design){
        if (design == ExperimentDesign.PARAMETER_SCAN) {
            experimentDesign.getSelectionModel().select(1);
        } else if (design == ExperimentDesign.LOCAL_SENSITIVITY_ANALYSIS) {
            experimentDesign.getSelectionModel().select(2);
        } else if (design == ExperimentDesign.GLOBAL_SENSITIVITY_ANALYSIS) {
            experimentDesign.getSelectionModel().select(3);
        } else if (design == ExperimentDesign.STATISTICAL_MODEL_CHECKING) {
            experimentDesign.getSelectionModel().select(4);
        } else if (design == ExperimentDesign.CONVERGENCE_TEST) {
        experimentDesign.getSelectionModel().select(5);
        } else if (design == ExperimentDesign.NONE) {
            experimentDesign.getSelectionModel().select(0);
        }
        handleExperimentDesign(design);
    }

    private void handleExperimentDesign(ExperimentDesign design) {
        if (design == ExperimentDesign.PARAMETER_SCAN) {
            addExperimentDesign(ExperimentDesign.PARAMETER_SCAN);
        } else if (design == ExperimentDesign.LOCAL_SENSITIVITY_ANALYSIS) {
            addExperimentDesign(ExperimentDesign.LOCAL_SENSITIVITY_ANALYSIS);
        } else if (design == ExperimentDesign.GLOBAL_SENSITIVITY_ANALYSIS) {
            addExperimentDesign(ExperimentDesign.GLOBAL_SENSITIVITY_ANALYSIS);
        } else if (design == ExperimentDesign.STATISTICAL_MODEL_CHECKING) {
            addExperimentDesign(ExperimentDesign.STATISTICAL_MODEL_CHECKING);
        } else if (design == ExperimentDesign.CONVERGENCE_TEST) {
            addExperimentDesign(ExperimentDesign.CONVERGENCE_TEST);
        } else if (design == ExperimentDesign.NONE) {
            experimentModel.removeExperimentDesign(view, currentExperimentDesign);
        }
        this.currentExperimentDesign = design;
    }

    private void addExperimentDesign(ExperimentDesign design) {

        String resource = "";

        if(design == ExperimentDesign.PARAMETER_SCAN){
            resource = "schemas/Types/scan-schema.json";
        } else if(design == ExperimentDesign.LOCAL_SENSITIVITY_ANALYSIS){
            resource = "schemas/Types/lsa-schema.json";
        } else if(design == ExperimentDesign.GLOBAL_SENSITIVITY_ANALYSIS){
            resource = "schemas/Types/gsa-schema.json";
        } else if(design == ExperimentDesign.STATISTICAL_MODEL_CHECKING){
            resource = "schemas/Types/smc-schema.json";
        } else if(design == ExperimentDesign.CONVERGENCE_TEST){
            resource = "schemas/Types/convergence-schema.json";
        }

        // this is the path within the jar file
        InputStream input = this.getClass().getResourceAsStream("/resources/" + resource);
        if (input == null) {
            // this is how we load file within editor (eg eclipse)
            input = this.getClass().getClassLoader().getResourceAsStream(resource);
        }
    
        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.getFactory().disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);
        try {
            map = mapper.readValue(input, LinkedHashMap.class);
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    
        view.generateInputElements(map);
    }

    public BackendType getBackendType() {
        return backendType;
    }

    public String getExperimentName() { return experimentName.getText();}

}
