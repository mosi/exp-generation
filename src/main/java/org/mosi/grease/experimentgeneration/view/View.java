package org.mosi.grease.experimentgeneration.view;


import com.scalified.tree.TreeNode;
import com.scalified.tree.multinode.ArrayMultiTreeNode;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.Pair;
import org.mosi.grease.experimentgeneration.model.ExperimentModel;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import static java.util.Arrays.asList;

/**
 * Created by Marcus on 19.06.2019.
 */


public class View {

    private ExperimentModel experimentModel;
    private Stage stage;
    private BorderPane outerBorderPane;
    private BorderPane innerBorderPane;
    private TabPane experimentTabs;
    private HBox buttonArea;
    private EditorView editorView;
    private ExperimentTab experimentTab;
    private Label loggerLabel;
    private VBox loggerView;
    private TextArea logger;
    private Button generateExperimentButton;
    private ExperimentMenu experimentMenu;

    public View(ExperimentModel experimentModel, Stage stage) {

        this.experimentModel = experimentModel;
        this.experimentMenu = new ExperimentMenu(this);
        this.stage = stage;
        experimentTab = new ExperimentTab(experimentModel, this);
        editorView = new EditorView(experimentModel);
        outerBorderPane = new BorderPane();
        innerBorderPane = new BorderPane();
        experimentTabs = new TabPane();
        buttonArea = new HBox();

        this.loggerLabel = new Label("Schema Validation Error Log");
        this.loggerView = new VBox();
        this.logger = new TextArea();

        generateExperimentButton = new Button("Generate Experiment");

        setup();
    }

    public void setup() {
//        buttonArea.setStyle("-fx-border-color: black;");
        outerBorderPane.setBottom(innerBorderPane);
//        outerBorderPane.setStyle("-fx-border-color: black;");

        logger.setPrefHeight(350);
        logger.setEditable(false);
        loggerView.getChildren().add(loggerLabel);
        loggerView.getChildren().add(logger);
//      VBox.setVgrow(logger, Priority.ALWAYS);
        loggerView.setPrefWidth(450);
        
        editorView.setMinWidth(300);
        
        outerBorderPane.setTop(experimentMenu);
        outerBorderPane.setRight(loggerView);
        outerBorderPane.setCenter(editorView);

        innerBorderPane.setCenter(experimentTabs);

        Scene scene = new Scene(outerBorderPane, 1024, 720);
        stage.setResizable(true);
        stage.setScene(scene);

        experimentTabs.getTabs().add(experimentTab);

        createButtonArea();
    }

    private void createButtonArea() {
        buttonArea.getChildren().add(generateExperimentButton);
        buttonArea.setPadding(new Insets(10));
        buttonArea.setAlignment(Pos.CENTER_LEFT);
        innerBorderPane.setBottom(buttonArea);
    }

    /**
     * Generates Input User Interface based on selected Experiment Schema
     *
     * @param jsonMap
     */
    public void generateInputElements(LinkedHashMap<String, Object> jsonMap) {
        experimentTab.getExperimentDesign().setDisable(false);
        HashMap<String, Tab> experimentTabs = experimentModel.getExperimentTabs();
        HashMap<String, Object> generatedGuiElements = experimentModel.getGeneratedGuiElements();
        LinkedHashMap<String, Object> schemaMap = jsonMap;//experimentModel.getJsonAsLinkedHashMap(inputStream); //schema
        TreeNode<String> structureForJasonGeneration = experimentModel.getExperimentRoot();

        if(schemaMap.containsKey("properties")){
            LinkedHashMap<String, Object> propertiesOfRoot = (LinkedHashMap<String, Object>) schemaMap.get("properties");

            //tabs contained in 1st instance of "properties":
            for (String TabKey : propertiesOfRoot.keySet()) {

                //mirror structure in data structure for experiment model:
                TreeNode<String> tabInStructureForJsonGeneration = new ArrayMultiTreeNode<String>(TabKey);
                structureForJasonGeneration.add(tabInStructureForJsonGeneration);

                LinkedHashMap<String, Object> tab = (LinkedHashMap<String, Object>) propertiesOfRoot.get(TabKey);

                String tabTitle = tab.get("title").toString();
                if (!tabTitle.isEmpty()) {
                    createNewTab(experimentTabs, tabTitle);
                }

                if(tab.containsKey("properties")){
                    LinkedHashMap<String, Object> propertiesOfTab = (LinkedHashMap<String, Object>) tab.get("properties");

                    //Elements that go on current tab:
                    for(String elementOnTabKey : propertiesOfTab.keySet()){
                        LinkedHashMap<String, Object> elementOnTab = (LinkedHashMap<String, Object>) propertiesOfTab.get(elementOnTabKey);

                        if (elementOnTab.containsKey("type")){

                            switch (elementOnTab.get("type").toString()) {
                                case "string":
                                    //Label + Textfield
                                case "integer":
                                case "boolean": {
                                    TreeNode<String> itemInStructureForJsonGeneration = new ArrayMultiTreeNode<String>(elementOnTabKey);
                                    tabInStructureForJsonGeneration.add(itemInStructureForJsonGeneration);
                                    generateBasicElements(experimentTabs, generatedGuiElements, elementOnTabKey, tabTitle, elementOnTab, schemaMap);
                                    break;
                                }
                                case "number": {
                                    TreeNode<String> itemInStructureForJsonGeneration = new ArrayMultiTreeNode<String>(elementOnTabKey);
                                    tabInStructureForJsonGeneration.add(itemInStructureForJsonGeneration);
                                    generateBasicElements(experimentTabs, generatedGuiElements, elementOnTabKey, tabTitle, elementOnTab, schemaMap);
                                    break;
                                }
                                //Label with multiple Textfields
                                case "array": {
                                    TreeNode<String> itemInStructureForJsonGeneration = new ArrayMultiTreeNode<String>(elementOnTabKey);
                                    tabInStructureForJsonGeneration.add(itemInStructureForJsonGeneration);
                                    generateDynamicInputFields(experimentTabs, elementOnTabKey, tabTitle, elementOnTab, generatedGuiElements);
                                    break;
                                }
                                //New Tab recursively
                                case "object": {
                                    TreeNode<String> itemInStructureForJsonGeneration = new ArrayMultiTreeNode<String>(elementOnTabKey);
                                    tabInStructureForJsonGeneration.add(itemInStructureForJsonGeneration);
                                    handleObjectCase(experimentTabs, generatedGuiElements, elementOnTab, tabTitle, schemaMap, itemInStructureForJsonGeneration, elementOnTabKey);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        experimentModel.initialDisable();
    }

    private void handleObjectCase(HashMap<String, Tab> experimentTabs, HashMap<String, Object> generatedGuiElements,
                                  LinkedHashMap<String, Object> elementObject,
                                  String tabTitle, LinkedHashMap<String, Object> schemaMap,
                                  TreeNode<String> ItemInStructureForJson, String elementObjectKey) {

        if (elementObject.containsKey("oneOf")) {
            handleOneOfCase(experimentTabs, generatedGuiElements, schemaMap, ItemInStructureForJson, tabTitle, elementObjectKey, elementObject);
        } else {
            generateStructureToGroupGuiElementsWithinObject(elementObjectKey, elementObject);

            LinkedHashMap<String, Object> itemObject = (LinkedHashMap<String, Object>) elementObject.get("properties");
            for (String objectKey : itemObject.keySet()) {
                LinkedHashMap<String, Object> object = (LinkedHashMap<String, Object>) itemObject.get(objectKey);
                for (String values : object.keySet()) {
                    if (values.equals("type")) {
                        switch (object.get(values).toString()) {
                            case "string":
                            case "number":
                            case "integer": {
                                TreeNode<String> subItemForJsonGeneration = new ArrayMultiTreeNode<String>(objectKey);
                                ItemInStructureForJson.add(subItemForJsonGeneration);
                                generateBasicElements(experimentTabs, generatedGuiElements, objectKey, tabTitle, object, schemaMap);
                                break;
                            }
                            case "array": {
                                TreeNode<String> subItemForJsonGeneration = new ArrayMultiTreeNode<String>(objectKey);
                                ItemInStructureForJson.add(subItemForJsonGeneration);
                                generateDynamicInputFields(experimentTabs, objectKey, tabTitle, object, generatedGuiElements);
                                break;
                            }
                            case "object": {
                                TreeNode<String> subItemForJsonGeneration = new ArrayMultiTreeNode<String>(objectKey);
                                ItemInStructureForJson.add(subItemForJsonGeneration);
                                handleObjectCase(experimentTabs, generatedGuiElements, object, tabTitle, schemaMap, subItemForJsonGeneration,objectKey);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    private String getPropertiesTitle(String propertyKey, LinkedHashMap<String, Object> item) {
        String labelText;
        if (item.containsKey("title")) {
            labelText = item.get("title").toString();
        } else {
            labelText = propertyKey;
        }
        return labelText;
    }

    public void clearView() {
        ArrayList<Tab> tabs = new ArrayList<>();
        for (Tab tab : experimentTabs.getTabs()) {
            if (tab != experimentTab) {
                tabs.add(tab);
            }
        }
        for (Tab todeleteTab : tabs) {
            experimentTabs.getTabs().remove(todeleteTab);
        }
    }

	//--------------------------dynamic Inputs:-------------------------------------------------------------------------

    private void generateDynamicInputFields(HashMap<String, Tab> tabHash, String propertyKey, String tabText, LinkedHashMap<String, Object> item, HashMap<String, Object> generatedGuiElement) {
        HashMap<String, Pair<Integer, Integer>> zipMarker = experimentModel.getZipInstanceMarker();
        HashMap<String, ArrayList<Pair<Button, Button>>> zipButtonMarker = experimentModel.getZipButtonMarker();
        LinkedHashMap<String, LinkedHashMap<String, ArrayList<Node>>> zipHash = experimentModel.getZipHash();
        HashMap<Tab, Pair<Integer, Integer>> gridPositionMarker = experimentModel.getGridPositionMarker();
        LinkedHashMap<String, ArrayList> zipInstances = experimentModel.getZipInstances();

        String zip = "";
        if (item.containsKey("zipName")) {
            zip = item.get("zipName").toString();
        }
        String finalZip = zip;

        Tab tab = tabHash.get(tabText);
        ScrollPane scrollPane = (ScrollPane) tab.getContent();
        GridPane layout = (GridPane) scrollPane.getContent();

        Label label = setPropertyLabel(propertyKey, item);
        TextField textField = generateNewTextField(propertyKey, item);

        Button addButton = createAddButton(propertyKey, finalZip, tab, zip);
        Button removeButton = createRemoveButton(zip, tab);

        Pair<Integer, Integer> indexPair = gridPositionMarker.get(tab);
        Integer rowIndex = indexPair.getValue();
        Integer columnIndex = indexPair.getKey();

        //If gridpane is empty
        if (rowIndex == 0 && columnIndex == 0) {
            addElementToEmptyPane(propertyKey, item, generatedGuiElement, zipMarker, zipButtonMarker, zipHash,
                    gridPositionMarker, zipInstances, label, textField, addButton, removeButton, tab, layout, rowIndex);

        }
        //If Gridpane is not Empty
        else {
            addElementToNonEmptyPane(propertyKey, item, generatedGuiElement, zipMarker, zipButtonMarker, zipHash,
                    gridPositionMarker, zipInstances, label, textField, addButton, removeButton, tab, layout, rowIndex, columnIndex, zip);

        }
    }

    private Label setPropertyLabel(String propertyKey, LinkedHashMap<String, Object> item) {
        String labelText = getPropertiesTitle(propertyKey, item);
        return new Label(labelText);

    }

    @NotNull
    private Button createAddButton(String propertyKey, String finalZip, Tab tab, String zip) {
        Button addButton = new Button("Add");
        HashMap<String, Object> generatedGuiElements = experimentModel.getGeneratedGuiElements();
        List<Button> buttons = new ArrayList<>();

        if(generatedGuiElements.containsKey("addButtons" + zip)){
            buttons = (List<Button>) generatedGuiElements.get("addButtons" + zip);
        } else {
            generatedGuiElements.put("addButtons" + zip, buttons);
        }
        if (!generatedGuiElements.containsKey("addButtons" + propertyKey)){
            generatedGuiElements.put("addButtons" + propertyKey, buttons);
        }
        buttons.add(addButton);
        addButton.setOnAction(event -> {
            experimentModel.pushGridElementsDown(tab, addButton);
            experimentModel.addNewRow(tab, addButton, finalZip, propertyKey);
        });
        return addButton;
    }

    @NotNull
    private Button createRemoveButton(String zip, Tab tab) {
        Button removeButton = new Button("Remove");
        removeButton.setId(zip);
        return removeButton;
    }

	//--------------------------zip Structures:-------------------------------------------------------------------------

    private void generateStructureToGroupGuiElementsWithinObject(String elementObjectKey, LinkedHashMap<String, Object> elementObject) {
        LinkedHashMap<String,Object> groupedItems = (LinkedHashMap<String, Object>) elementObject.get("properties");
        for(String groupedItemKey : groupedItems.keySet()){
            LinkedHashMap<String,Object> groupedItem = (LinkedHashMap<String, Object>) groupedItems.get(groupedItemKey);
            groupedItem.put("zipName", elementObjectKey);
        }
    }

    private void updateZipStructure(String propertyKey, LinkedHashMap<String, ArrayList> zipInstances, String zipName) {
        if (!zipInstances.containsKey(zipName)) {
            ArrayList zipList = new ArrayList();
            zipList.add(propertyKey);
            zipInstances.put(zipName, zipList);
        } else {
            ArrayList arrayList = zipInstances.get(zipName);
            arrayList.add(propertyKey);
        }
    }

    private void updateZipStructure(String propertyKey, LinkedHashMap<String, LinkedHashMap<String, ArrayList<Node>>> zipHash, TextField textField, String zipName) {
        LinkedHashMap<String, ArrayList<Node>> zipInstance = zipHash.get(zipName);
        ArrayList<Node> additionalElementList = new ArrayList<>();
        additionalElementList.add(textField);
        zipInstance.put(propertyKey, additionalElementList);
        LinkedHashMap<String, Object> generatedGuiElements = experimentModel.getGeneratedGuiElements();
        generatedGuiElements.put(propertyKey, additionalElementList);
    }

    private void createNewZipInstance(String propertyKey, LinkedHashMap<String, LinkedHashMap<String, ArrayList<Node>>> zipHash, TextField textField, String zip) {
        LinkedHashMap<String, ArrayList<Node>> individualZip = new LinkedHashMap<>();
        LinkedHashMap<String, Object> generatedGuiElements = experimentModel.getGeneratedGuiElements();
        ArrayList<Node> propertyElements = new ArrayList<>();
        propertyElements.add(textField);
        individualZip.put(propertyKey, propertyElements);
        zipHash.put(zip, individualZip);
        generatedGuiElements.put(propertyKey, propertyElements);
    }

	//--------------------------OneOf Structures:-----------------------------------------------------------------------

    private void handleOneOfCase(HashMap<String, Tab> experimentTabs, HashMap<String, Object> generatedGuiElements, LinkedHashMap<String, Object> schemaMap, TreeNode<String> treeNodeItem, String tabTitle, String elementContainingOneOfKey, LinkedHashMap<String, Object> elementContainingOneOf) {
        ArrayList<Object> oneOfArray = (ArrayList<Object>) elementContainingOneOf.get("oneOf");
        String title = getPropertiesTitle(elementContainingOneOfKey, elementContainingOneOf);

        ComboBox<String> comboBox = generateComboBoxForOneOf(experimentTabs, generatedGuiElements, schemaMap, tabTitle, title, oneOfArray);

        for(Object arrayElement : oneOfArray){
            LinkedHashMap<String, Object> subItem = (LinkedHashMap<String, Object>) arrayElement;
            fillListsToEnableAndDisableGuiElements(oneOfArray, subItem);
            handleOneOfObjectCase(experimentTabs, generatedGuiElements,subItem, tabTitle, schemaMap,treeNodeItem);

            generateLookUpForImportingJason(generatedGuiElements, comboBox, subItem);
        }
    }

    private void generateLookUpForImportingJason(HashMap<String, Object> generatedGuiElements, ComboBox<String> comboBox, LinkedHashMap<String, Object> subItem) {
        if(!generatedGuiElements.containsKey("oneOfLookup")){
            HashMap<String, List<Object>> oneOfLookup = new HashMap<>();
            generatedGuiElements.put("oneOfLookup", oneOfLookup);
        }
        HashMap<String, List<Object>> oneOfLookup = (HashMap<String, List<Object>>) generatedGuiElements.get("oneOfLookup");

        ArrayList<String> elementsToEnable = new ArrayList<>();
        addSubItemsToEnableList(subItem,elementsToEnable);
        for(String elementToEnable : elementsToEnable){
            List<Object> comboBoxes = new ArrayList<>();
            if(oneOfLookup.containsKey(elementToEnable)){
                comboBoxes = oneOfLookup.get(elementToEnable);
            }
            Pair<ComboBox, String> comboBoxAndItemToChoose = new Pair<>(comboBox,(String) subItem.get("title"));
            comboBoxes.add(comboBoxAndItemToChoose);
            oneOfLookup.put(elementToEnable, comboBoxes);
        }
    }

    private void handleOneOfObjectCase(HashMap<String, Tab> experimentTabs, HashMap<String, Object> generatedGuiElements, LinkedHashMap<String, Object> elementObject, String tabTitle, LinkedHashMap<String, Object> schemaMap, TreeNode<String> treeNodeItem) {
        LinkedHashMap<String, Object> itemObject = (LinkedHashMap<String, Object>) elementObject.get("properties");
        for (String objectKey : itemObject.keySet()) {
            LinkedHashMap<String, Object> object = (LinkedHashMap<String, Object>) itemObject.get(objectKey);
            if(object.containsKey("type")){
                String type = (String) object.get("type");
                switch (type) {
                    case "string":
                    case "number":
                    case "integer": {
                        TreeNode<String> propertyChild = new ArrayMultiTreeNode<String>(objectKey);
                        treeNodeItem.add(propertyChild);
                        generateBasicElements(experimentTabs, generatedGuiElements, objectKey, tabTitle, object, schemaMap);
                        break;
                    }
                    case "array": {
                        TreeNode<String> propertyChild = new ArrayMultiTreeNode<String>(objectKey);
                        treeNodeItem.add(propertyChild);
                        if(!object.containsKey("zipName")){
                            object.put("zipName", objectKey);
                        }
                        generateDynamicInputFields(experimentTabs, objectKey, tabTitle, object, generatedGuiElements);
                        break;
                    }
                    case "object": {
                        TreeNode<String> propertyChild = new ArrayMultiTreeNode<String>(objectKey);
                        treeNodeItem.add(propertyChild);
                        handleObjectCase(experimentTabs, generatedGuiElements, object, tabTitle, schemaMap, propertyChild,objectKey);
                        break;
                    }
                }
            }
        }
    }

    private ComboBox<String> generateComboBoxForOneOf(HashMap<String, Tab> experimentTabs, HashMap<String, Object> generatedGuiElements, LinkedHashMap<String, Object> schemaMap, String tabText, String propertyTitle, ArrayList<Object> oneOfArray) {
        ArrayList<String> enumList= new ArrayList<>();

        for(Object arrayElement : oneOfArray){
            LinkedHashMap<String, Object> subItem = (LinkedHashMap<String, Object>) arrayElement;
            String title = (String) subItem.get("title");
            enumList.add(title);
        }
        LinkedHashMap<String, Object> item = new LinkedHashMap<>();
        item.put("enum",enumList);

        Label label = new Label(propertyTitle);
        Tab tab = experimentTabs.get(tabText);
        ScrollPane scrollPane = (ScrollPane) tab.getContent();
        GridPane layout = (GridPane) scrollPane.getContent();
        HashMap<Tab, Pair<Integer, Integer>> gridPositionMarker = experimentModel.getGridPositionMarker();
        Pair<Integer, Integer> indexPair = gridPositionMarker.get(tab);
        Integer rowIndex = indexPair.getValue();
        Integer columnIndex = indexPair.getKey();

        ArrayList<Object> enumArray = (ArrayList<Object>) item.get("enum");
        return generateEnumerationElements(generatedGuiElements, propertyTitle, enumArray, label, tab, layout, gridPositionMarker, indexPair, rowIndex, columnIndex, schemaMap);
    }

    private void fillListsToEnableAndDisableGuiElements(ArrayList<Object> oneOfArray, LinkedHashMap<String, Object> subItem) {
        String itemTitle = (String) subItem.get("title");
        fillListToEnableGuiElements(subItem, itemTitle);
        fillListToDisableGuiElements(oneOfArray, subItem, itemTitle);
    }

    private void fillListToDisableGuiElements(ArrayList<Object> oneOfArray, LinkedHashMap<String, Object> subItem, String itemTitle) {
        ArrayList<String> elementsToDisable = new ArrayList<>();
        for(Object anotherArrayElement : oneOfArray){
            LinkedHashMap<String, Object> anotherSubItem = (LinkedHashMap<String, Object>) anotherArrayElement;
            if(!subItem.get("title").equals(anotherSubItem.get("title"))){
                addSubItemsToEnableList(anotherSubItem,elementsToDisable);
            }
        }
        experimentModel.updateDisabledElements(itemTitle,elementsToDisable);
    }

    private void fillListToEnableGuiElements(LinkedHashMap<String, Object> subItem, String itemTitle) {
        ArrayList<String> elementsToEnable = new ArrayList<>();
        addSubItemsToEnableList(subItem,elementsToEnable);
        experimentModel.updateEnabledElements(itemTitle,elementsToEnable);
    }

    private void addSubItemsToEnableList(LinkedHashMap<String, Object> subItem, ArrayList<String> elementsToEnable) {
        LinkedHashMap<String, Object> propertiesOfSubItem = (LinkedHashMap<String, Object>) subItem.get("properties");
        for(String propertyOfSubItemKey : propertiesOfSubItem.keySet()){
            LinkedHashMap<String,Object> propertyOfSubItem = (LinkedHashMap<String, Object>) propertiesOfSubItem.get(propertyOfSubItemKey);
            if(propertyOfSubItem.containsKey("type")){
                if(propertyOfSubItem.get("type").equals("object")){
                    elementsToEnable.add((String) propertyOfSubItem.get("title"));
                    addSubItemsToEnableList(propertyOfSubItem,elementsToEnable);
                }else{
                    elementsToEnable.add(propertyOfSubItemKey);
                }
            }
        }
    }

    //--------------------------Basic Elements:-------------------------------------------------------------------------

    private Object generateBasicElements(HashMap<String, Tab> tabHash, HashMap<String, Object> generatedGuiElements, String propertyKey, String tabText, LinkedHashMap<String, Object> item, LinkedHashMap<String, Object> map) {
        String labelText = getPropertiesTitle(propertyKey, item);
        Label label = new Label(labelText);

        Tab tab = tabHash.get(tabText);
        ScrollPane scrollPane = (ScrollPane) tab.getContent();
        GridPane layout = (GridPane) scrollPane.getContent();

        HashMap<Tab, Pair<Integer, Integer>> gridPositionMarker = experimentModel.getGridPositionMarker();
        Pair<Integer, Integer> indexPair = gridPositionMarker.get(tab);
        Integer rowIndex = indexPair.getValue();
        Integer columnIndex = indexPair.getKey();

        if (item.containsKey("enum")) {
            ArrayList<Object> enumArray = (ArrayList<Object>) item.get("enum");
            return generateEnumerationElements(generatedGuiElements, propertyKey, enumArray, label, tab, layout,
                    gridPositionMarker, indexPair, rowIndex, columnIndex, map);
        } else if (item.get("type").equals("boolean")) {
            ArrayList<Object> enumArray = new ArrayList<>(asList("false", "true"));
            return generateEnumerationElements(generatedGuiElements, propertyKey, enumArray, label, tab, layout,
                    gridPositionMarker, indexPair, rowIndex, columnIndex, map);
        }
        else {
            return generateInputTextField(generatedGuiElements, propertyKey, label, tab, layout,
                    gridPositionMarker, indexPair, rowIndex, columnIndex, item);
        }
    }

    private ComboBox generateEnumerationElements(HashMap<String, Object> generatedGuiElements, String propertyKey,
                                                 ArrayList<Object> enumArray, Label label, Tab tab,
                                                 GridPane layout, HashMap<Tab, Pair<Integer, Integer>> gridPositionMarker,
                                                 Pair<Integer, Integer> indexPair, Integer rowIndex, Integer columnIndex,
                                                 LinkedHashMap<String, Object> map) {
        ComboBox<String> comboBox = new ComboBox<>();
        for (Object enumerationValues : enumArray) {
            comboBox.getItems().add(enumerationValues.toString());
        }
        generatedGuiElements.put(propertyKey, comboBox);
        comboBox.setId(propertyKey);
        comboBox.getSelectionModel().select(0);
        if (indexPair.getKey() == 0 && rowIndex == 0) {
            layout.add(label, 0, 0);
            layout.add(comboBox, 1, 0);
            Pair<Integer, Integer> updatedPair = new Pair<>(0, 1);
            gridPositionMarker.put(tab, updatedPair);
        }
        //if tab is not empty start with new row
        else if (indexPair.getKey() == 0 && rowIndex != 0) {
            layout.add(label, columnIndex, rowIndex + 1);
            layout.add(comboBox, columnIndex + 1, rowIndex + 1);
            Pair<Integer, Integer> updatedPair = new Pair<>(0, rowIndex + 1);
            gridPositionMarker.put(tab, updatedPair);
        }

        comboBox.setOnAction(e -> {
            System.out.print(comboBox.getSelectionModel().getSelectedItem());
            experimentModel.updateEdibility(generatedGuiElements, comboBox);
        });

        return comboBox;
    }

    private TextField generateNewTextField(String propertyKey, LinkedHashMap<String, Object> item) {
        TextField textField = new TextField();
        textField.setId(propertyKey);
        textField.setPromptText(getPropertiesTitle(propertyKey, item));
        return textField;
    }

    private Object generateInputTextField(HashMap<String, Object> generatedGuiElements, String propertyKey, Label label, Tab tab, GridPane layout, HashMap<Tab, Pair<Integer, Integer>> gridPositionMarker, Pair<Integer, Integer> indexPair, Integer rowIndex, Integer columnIndex, LinkedHashMap<String, Object> item) {
        TextField textField = generateNewTextField(propertyKey, item);
        generatedGuiElements.put(propertyKey, textField);
        //If Textfield is the first element in the tab
        if (indexPair.getKey() == 0 && rowIndex == 0) {
            layout.add(label, 0, 0);
            layout.add(textField, 1, 0);
            Pair<Integer, Integer> updatedPair = new Pair<>(0, rowIndex + 1);
            gridPositionMarker.put(tab, updatedPair);
        }
        //if tab is not empty start with new row
        else if (indexPair.getKey() == 0 && rowIndex != 0) {
            layout.add(label, columnIndex, rowIndex + 1);
            layout.add(textField, columnIndex + 1, rowIndex + 1);
            Pair<Integer, Integer> updatedPair = new Pair<>(0, rowIndex + 1);
            gridPositionMarker.put(tab, updatedPair);
        }
        return textField;
    }

    private void addElementToNonEmptyPane(String propertyKey, LinkedHashMap<String, Object> item, HashMap<String, Object> generatedGuiElement, HashMap<String, Pair<Integer, Integer>> zipMarker, HashMap<String, ArrayList<Pair<Button, Button>>> zipButtonMarker, LinkedHashMap<String, LinkedHashMap<String, ArrayList<Node>>> zipHash, HashMap<Tab, Pair<Integer, Integer>> gridPositionMarker, LinkedHashMap<String, ArrayList> zipInstances, Label label, TextField textField, Button addButton, Button removeButton, Tab tab, GridPane layout, Integer rowIndex, Integer columnIndex, String zip) {
        //if Element is part of a Zip
        if (item.containsKey("zipName")) {
            String zipName = item.get("zipName").toString();
            //if one zip part already exists and another is added
            updateZipStructure(propertyKey, zipInstances, zipName);

            if (zipMarker.containsKey(zipName)) {

                Pair<Integer, Integer> zipMark = zipMarker.get(zipName);
                Integer column = zipMark.getKey();
                Integer row = zipMark.getValue();

                for(Pair<Button, Button> buttonButtonPair : zipButtonMarker.get(zipName)) {
                    Button key = buttonButtonPair.getKey();
                    Button value = buttonButtonPair.getValue();

                    layout.getChildren().remove(key);
                    layout.getChildren().remove(value);
                }

                layout.add(label, column, row);
                layout.add(textField, column + 1, row);
                layout.add(addButton, column + 2, row);
                layout.add(removeButton, column + 3, row);

                updateZipStructure(propertyKey, zipHash, textField, zipName);
                zipMarker.put(zipName, new Pair<>(column + 2, row));
                if(!zipButtonMarker.containsKey(zipName)) {
                    zipButtonMarker.put(zipName, new ArrayList<Pair<Button, Button>>());
                }
                zipButtonMarker.get(zipName).add(new Pair<>(addButton, removeButton));
            } else
            //if it's the first zip part
            {
                //Update Zip structure
                createNewZipInstance(propertyKey, zipHash, textField, zip);

                layout.add(label, columnIndex, rowIndex + 1);
                layout.add(textField, columnIndex + 1, rowIndex + 1);
                layout.add(addButton, columnIndex + 2, rowIndex + 1);
                layout.add(removeButton, columnIndex + 3, rowIndex + 1);


                if(!zipButtonMarker.containsKey(zipName)) {
                    zipButtonMarker.put(zipName, new ArrayList<Pair<Button, Button>>());
                }
                zipButtonMarker.get(zipName).add(new Pair<>(addButton, removeButton));
                gridPositionMarker.put(tab, new Pair<>(0, rowIndex + 1));
                zipMarker.put(zipName, new Pair<>(columnIndex + 2, rowIndex + 1));
            }
        }
        //If element is not part of a zip and layout is not empty
        else {
            ArrayList<Node> list = new ArrayList<>();
            list.add(textField);
            generatedGuiElement.put(propertyKey, list);

            addGuiElementsToLayout(gridPositionMarker, label, textField, addButton, removeButton, tab, layout, rowIndex, columnIndex, rowIndex + 1, columnIndex + 1, columnIndex + 2, columnIndex + 3);
            if(!zipButtonMarker.containsKey(propertyKey)) {
                zipButtonMarker.put(propertyKey, new ArrayList<Pair<Button, Button>>());
            }
            zipButtonMarker.get(propertyKey).add(new Pair<>(addButton, removeButton));
        }
    }

    private void addElementToEmptyPane(String propertyKey, LinkedHashMap<String, Object> item, HashMap<String, Object> generatedGuiElement, HashMap<String, Pair<Integer, Integer>> zipMarker, HashMap<String, ArrayList<Pair<Button, Button>>> zipButtonMarker, LinkedHashMap<String, LinkedHashMap<String, ArrayList<Node>>> zipHash, HashMap<Tab, Pair<Integer, Integer>> gridPositionMarker, LinkedHashMap<String, ArrayList> zipInstances, Label label, TextField textField, Button addButton, Button removeButton, Tab tab, GridPane layout, Integer rowIndex) {
        addGuiElementsToLayout(gridPositionMarker, label, textField, addButton, removeButton, tab, layout, rowIndex, 0, 0, 1, 2, 3);

        //N-Tuple Zip
        if (item.containsKey("zipName")) {
            String zipName = item.get("zipName").toString();


            updateZipStructure(propertyKey, zipInstances, zipName);
            createNewZipInstance(propertyKey, zipHash, textField, zipName);

            zipMarker.put(zipName, new Pair<>(2, 0));
            if(!zipButtonMarker.containsKey(zipName)) {
                zipButtonMarker.put(zipName, new ArrayList<Pair<Button, Button>>());
            }
            zipButtonMarker.get(zipName).add(new Pair<>(addButton, removeButton));
        } else {
            ArrayList<Node> nodes = new ArrayList<>();
            nodes.add(textField);
            generatedGuiElement.put(propertyKey, nodes);
            if(!zipButtonMarker.containsKey(propertyKey)) {
                zipButtonMarker.put(propertyKey, new ArrayList<Pair<Button, Button>>());
            }
            zipButtonMarker.get(propertyKey).add(new Pair<>(addButton, removeButton));
        }
    }

    private void addGuiElementsToLayout(HashMap<Tab, Pair<Integer, Integer>> gridPositionMarker, Label label, TextField textField, Button addButton, Button removeButton, Tab tab, GridPane layout, Integer rowIndex, int i, int i2, int i3, int i4, int i5) {
        layout.add(label, i, i2);
        layout.add(textField, i3, i2);
        GridPane.setHalignment(addButton, HPos.LEFT);
        layout.add(addButton, i4, i2);
        GridPane.setHalignment(removeButton, HPos.LEFT);
        layout.add(removeButton, i5, i2);
        gridPositionMarker.put(tab, new Pair<>(0, rowIndex + 1));
    }

    private void createNewTab(HashMap<String, Tab> tabHashMap, String text) {

        Tab tab = new Tab(text);
        tab.setClosable(false);
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setPrefHeight(230);
        experimentModel.getGridPositionMarker().put(tab, new Pair<>(0, 0));
        GridPane gridPane = new GridPane();
        gridPane.setHgap(5);
        gridPane.setVgap(5);
        gridPane.setPadding(new Insets(15, 15, 15, 15));
        gridPane.setVisible(true);
        scrollPane.setContent(gridPane);
        tab.setContent(scrollPane);

        if (!tabHashMap.containsKey(tab.getText())) {
            tabHashMap.put(tab.getText(), tab);
            experimentTabs.getTabs().add(tab);
        }

    }

    //--------------------------getter and setter:----------------------------------------------------------------------

    public ExperimentTab getExperimentTab() {
        return experimentTab;
    }

    public Stage getStage() {
        return stage;
    }

    public ExperimentModel getExperimentModel() {
        return experimentModel;
    }

    public TextArea getLogger() {
        return logger;
    }

    public Button getGenerateExperimentButton() {
        return generateExperimentButton;
    }

    public EditorView getEditorView() {
        return editorView;
    }

    public TabPane getExperimentTabs() {
        return experimentTabs;
    }
}
