package org.mosi.grease.experimentgeneration.view.templates;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Marcus on 21.08.2017.
 */
public class TemplateTabPane extends JTabbedPane {
    //List of TemplateTab each one as a tab
    private Map<String, TemplateTab> templateTabMap;

    public TemplateTabPane() {
        templateTabMap = new HashMap<>();
    }

    public TemplateTab addTemplateTab(String name) {
        TemplateTab tab = new TemplateTab();
        this.templateTabMap.put(name, tab);
        this.add(name, tab);
        return tab;
    }

    public TemplateTab getTemplateTab(String name) {
        return this.templateTabMap.get(name);
    }

    public void deleteAllTabs() {
        this.removeAll();
        this.templateTabMap.clear();
        this.validate();
        this.repaint();
    }


    public Map<String, TemplateTab> getTemplateTabMap() {
        return templateTabMap;
    }
}
