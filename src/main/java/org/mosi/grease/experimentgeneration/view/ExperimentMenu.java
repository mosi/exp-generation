package org.mosi.grease.experimentgeneration.view;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;

import java.io.*;


public class ExperimentMenu extends MenuBar {

    private VBox box;
    private Menu menu;
    private View view;
    public ExperimentMenu(View view){
        this.box = new VBox();
        this.menu = new Menu("Menu");
        this.view = view;
        setUp();
    }

    private void setUp() {
        MenuItem saveJson = new MenuItem("Save JSON File");
        saveJson.setOnAction(actionEvent -> {
            saveJsonFile();
        });
        MenuItem saveExperiment = new MenuItem("Save Experiment File");
        saveExperiment.setOnAction( actionEvent -> saveExperimentFile());
        menu.getItems().add(saveJson);
        menu.getItems().add(saveExperiment);
        this.getMenus().add(menu);
    }

    private void saveJsonFile() {
        FileChooser fileChooser = new FileChooser();

        //Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("JSON Files (*.json)", "*.json");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showSaveDialog(null);

        PrintWriter writer;
        try {
            writer = new PrintWriter(file);
            String jsonSpecification = view.getExperimentModel().getJsonSpecification();
            writer.println(jsonSpecification);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    private void saveExperimentFile() {
        FileChooser fileChooser = new FileChooser();

        //Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Experiment Files", "*.scala", "*.yaml", "*.py");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showSaveDialog(null);

        PrintWriter writer;
        try {
            writer = new PrintWriter(file);
            String jsonSpecification = view.getExperimentModel().getExperimentSpecificationInTargetLanguage();
            writer.println(jsonSpecification);
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
