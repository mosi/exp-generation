package org.mosi.grease.experimentgeneration.view.templates;


import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;


/**
 * Created by Marcus on 07.06.2017.
 */
public class TemplateTab extends JPanel {
    private Map<String, EditorInputContainer> variableInputContainerMap;
    //    private ArrayList multipleInstancesStrings;
//    private HashMap<String, ArrayList<JTextField>> listHash;
//    private JComponent[][] positions;
    private int currentRow = 0;


    public TemplateTab() {
//        listHash = new HashMap();
//        multipleInstancesStrings = new ArrayList();
        this.setName("General");
        variableInputContainerMap = new HashMap<>();
//        positions = new JComponent[30][30];
        this.setLayout(new GridBagLayout());
    }
//
//
//    public HashMap getListHash() {
//        return listHash;
//    }
//
//    public ArrayList getMultipleInstancesStrings() {
//        return multipleInstancesStrings;
//    }
//
//
//    //ActionListener for Button
//
//    public JComponent[][] getPositions() {
//        return positions;
//    }

    public GridBagConstraints positionInGrid(int column, int row) {
        GridBagConstraints templateLayoutConstraints = new GridBagConstraints();
        templateLayoutConstraints.insets = new Insets(5, 5, 5, 5);
        templateLayoutConstraints.anchor = GridBagConstraints.NORTHWEST;
        templateLayoutConstraints.gridx = column;
        templateLayoutConstraints.gridy = row;
        templateLayoutConstraints.gridwidth = 1;
        templateLayoutConstraints.fill = GridBagConstraints.HORIZONTAL;
        return templateLayoutConstraints;
    }


    public void addRow(JLabel label, EditorInputContainer component, String variableName) {
        this.variableInputContainerMap.put(variableName, component);
        this.add(label, positionInGrid(0, currentRow));
        this.add(component, positionInGrid(1, currentRow));
        currentRow += 1;
        this.revalidate();
    }


    public Map<String, EditorInputContainer> getVariableInputContainerMap() {
        return variableInputContainerMap;
    }
}
