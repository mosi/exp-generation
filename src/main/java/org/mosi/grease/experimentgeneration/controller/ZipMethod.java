package org.mosi.grease.experimentgeneration.controller;

import freemarker.template.SimpleScalar;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;
import org.mosi.grease.experimentgeneration.view.templates.EditorInputContainer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by andreas on 9/13/17.
 */
public class ZipMethod implements TemplateMethodModelEx {
    TemplateController controller;

    public ZipMethod(TemplateController controller) {
        this.controller = controller;
    }


    @Override
    public Object exec(List arguments) throws TemplateModelException {
        List zipTuples = new ArrayList();

        Map<String, List<Object>> valueMap = new HashMap<>();
        Integer referenceLength = null;


        for (Object variableArgument : arguments) {
            SimpleScalar argument = (SimpleScalar) variableArgument;
            String variable = argument.getAsString();
            EditorInputContainer container = this.controller.findEditorInputContainer(variable);
            List variableValue = container.getValue();
            valueMap.put(variable, variableValue);
            if (referenceLength == null) {
                referenceLength = variableValue.size();
            } else {
                referenceLength = Math.min(referenceLength, variableValue.size());
            }
        }

        for (int i = 0; i < referenceLength; i++) {
            Map<String, Object> tuple = new HashMap<>();
            for (Map.Entry<String, List<Object>> entry : valueMap.entrySet()) {
                String variable = entry.getKey();
                Object value = entry.getValue().get(i);
                tuple.put(variable, value);
            }
            zipTuples.add(tuple);
        }
        return zipTuples;

    }
}