package org.mosi.grease.experimentgeneration.controller;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.commons.io.FileUtils;
import org.mosi.grease.experimentgeneration.syntaxchecker.SyntaxChecker;
import org.mosi.grease.experimentgeneration.view.templates.EditorInputContainer;
import org.mosi.grease.experimentgeneration.view.templates.TemplateEditor;
import org.mosi.grease.experimentgeneration.view.templates.TemplateTab;
import org.mosi.grease.experimentgeneration.view.templates.TemplateTabPane;
//import sessl.parser.ExperimentRunner;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.io.*;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by andreas on 5/23/17.
 */
public class TemplateController {
    private TemplateEditor editor = null;
    private Template currentTemplate;
    private Map<String,Object> defaultValueMap;
    private Map<String,Object> expectedValueMap;
    private boolean sealed = false;
    private SyntaxChecker syntaxChecker;
    
    public TemplateController() {
    	defaultValueMap = new HashMap<>();
    	this.syntaxChecker = new SyntaxChecker();
    }

    public boolean isSealed() {
        return sealed;
    }

    public void setEditor(TemplateEditor editor) {
        this.editor = editor;
    }

    private void buildNodes(File directory, DefaultMutableTreeNode node) {
        File[] subDirectories = directory.listFiles(File::isDirectory);
        File[] templateFiles = directory.listFiles(File::isFile);
        for (File subDir : subDirectories) {
            FileNode nodeContent = new FileNode(subDir);
            DefaultMutableTreeNode dirNode = new DefaultMutableTreeNode(nodeContent);
            this.buildNodes(subDir, dirNode);
            node.add(dirNode);
        }
        for (File templateFile : templateFiles) {
            FileNode nodeContent = new FileNode(templateFile);
            node.add(new DefaultMutableTreeNode(nodeContent));
        }
    }

    private File loadTemplateFolder() {
        File templateFolder = null;
        try {
            templateFolder = new File(ClassLoader.getSystemResource("Templates").toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return templateFolder;
    }

    public DefaultMutableTreeNode buildTemplateTree() {
        File templateFolder = this.loadTemplateFolder();
        FileNode nodeContent = new FileNode(templateFolder);
        DefaultMutableTreeNode templateTreeRootNode = new DefaultMutableTreeNode(nodeContent);
        this.buildNodes(templateFolder, templateTreeRootNode);
        return templateTreeRootNode;
    }


    public void openTemplate(DefaultMutableTreeNode node) {

        FileNode nodeContent = (FileNode) node.getUserObject();
        File templateFile = nodeContent.getFile();
        Template template = this.loadTemplate(templateFile);

        if (template != null) {
            if (this.currentTemplate != null && !template.getName().equals(this.currentTemplate.getName())) {
                this.editor.reset();

                this.sealed = false;
            }
            this.currentTemplate = template;
            try {
                String templateText = String.join("\n", FileUtils.readLines(templateFile));
                Writer out = new OutputStreamWriter(System.out);
                expectedValueMap = this.buildExpectedValueMap(templateText);
                defaultValueMap = this.buildDefaultValueMap(templateText);
                template.process(expectedValueMap, out);
                this.clearTemplateText();
                this.sealed = true;
            } catch (TemplateException | IOException e) {
                e.printStackTrace();
            }
        } else {
            throw new RuntimeException("Template is null");
        }


    }

    private Map buildExpectedValueMap(String templateText) {
        Map<String, Object> result = new HashMap<>();
        String variableNameRegex = "<@.*variable=\"(\\S*)\".*\\>";
        Pattern variableNamePattern = Pattern.compile(variableNameRegex);
        Matcher variableNameMatcher = variableNamePattern.matcher(templateText);
        while (variableNameMatcher.find()) {
            String wholeLine = variableNameMatcher.group(0);
            String variableName = variableNameMatcher.group(1);
            result.put(variableName, "");

            String listName = getListNameFromDirective(wholeLine);
            if (listName != null) {

                result.put(variableName, new ArrayList<>());
            }


        }
        return result;
    }
    
    private Map buildDefaultValueMap(String templateText) {
        Map<String, Object> result = new HashMap<>();
        String variableNameRegex = "<@.*variable=\"(\\S*)\".*\\>";
        Pattern variableNamePattern = Pattern.compile(variableNameRegex);
        Matcher variableNameMatcher = variableNamePattern.matcher(templateText);
        while (variableNameMatcher.find()) {
            String wholeLine = variableNameMatcher.group(0);
            String variableName = variableNameMatcher.group(1);
            result.put(variableName, "");

            String listName = getListNameFromDirective(wholeLine);
            if (listName != null) {

                result.put(variableName, new ArrayList<>());
            }


        }
        return result;
    }

    private String getListNameFromDirective(String directive) {
        String listNameRegex = "<@.*listInput=\"(.*)\".*\\/>";
        Pattern listNamePattern = Pattern.compile(listNameRegex);
        Matcher listNameMatcher = listNamePattern.matcher(directive);
        String result = null;
        if (listNameMatcher.find()) {
            result = listNameMatcher.group(1);
        }
        return result;
    }

    private void clearTemplateText() {
        JTextArea templateTextArea = this.editor.getResultTextArea();
        String templateText = templateTextArea.getText();
        String cleanedTemplateText = templateText.replaceAll("(\\S|\\s)*<--\\/>\\n", "");
        templateTextArea.setText(cleanedTemplateText);
    }

    private Template loadTemplate(File templateFile) {
        Configuration templateEngineConfiguration = this.configurateTemplateEngine(templateFile);
        if (templateEngineConfiguration == null) {
            return null;
        }
        Template template = null;
        try {
            template = templateEngineConfiguration.getTemplate(templateFile.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return template;
    }


    private Configuration configurateTemplateEngine(File templateFile) {
        Configuration configuration = new Configuration();
        configuration.setDefaultEncoding("UTF-8");
        try {
            configuration.setDirectoryForTemplateLoading(templateFile.getParentFile());
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        configuration.setSharedVariable("interpolationData", new TemplateDirective(this));
        configuration.setSharedVariable("zipMethod", new ZipMethod(this));


        //        configuration.setClassForTemplateLoading(this.getClass(), "/");
//        configuration.setSharedVariable("interpolationData", new TemplateDirective(controller, templateEditor, experimentDescriptionEditor));
//        //Selecting currently selected Template in Treenode (default TestTemplate.ftl)
//        template = configuration.getTemplate(templateDirectory);
//        //Feeding HashMap with default Inputs
//        saveInterpolationsIntoList(fileDirectory);
        return configuration;

    }

    public TemplateTab getOrCreateTemplateTab(String trait) {
        TemplateTabPane tabs = this.editor.getTemplateTabPane();
        TemplateTab tab = tabs.getTemplateTab(trait);
        if (tab == null) {
            tab = tabs.addTemplateTab(trait);
        }
        return tab;
    }

    public void renderTemplate() {
        Writer writer = new StringWriter();
        Map<String, Object> valueMap = this.collectInputValues();
        try {
            this.currentTemplate.process(valueMap, writer);
            String templateText = writer.toString();
            String cleanTemplateText = this.cleanTemplateText(templateText);
            String compilerMessage = this.syntaxChecker.check(cleanTemplateText);

            if(compilerMessage == null || compilerMessage.equals("")) {
                this.editor.getResultTextArea().setForeground(Color.BLACK);
                this.editor.getResultTextArea().setText(cleanTemplateText);
            }
            else {
                this.editor.getResultTextArea().setForeground(Color.RED);
                this.editor.getResultTextArea().setText(compilerMessage);
            }
//            ExperimentRunner runner = new ExperimentRunner();
//            boolean isValidExperiment = runner.isValidExperimentText(cleanTemplateText);
//            System.out.println("Is experiment Valid? " + isValidExperiment);
//            if (!isValidExperiment) {
//                System.out.println(runner.getLastError());
//            }
        } catch (TemplateException | IOException e) {
            e.printStackTrace();
        }
    }

    private String cleanTemplateText(String templateText) {

        return templateText.replaceFirst( "(\\S|\\s)*<--\\/>","")
                .replaceAll(",[\\s]+\\)","\n\t)");
    }

    private Map<String, Object> collectInputValues() {
        Map<String, Object> result = new HashMap<>();
        Map<String, TemplateTab> tabMap = this.editor.getTemplateTabPane().getTemplateTabMap();
        for (TemplateTab tab : tabMap.values()) {
            Map<String, EditorInputContainer> variableComponentMap = tab.getVariableInputContainerMap();
            for (Map.Entry<String, EditorInputContainer> entry : variableComponentMap.entrySet()) {
                String variable = entry.getKey();
                EditorInputContainer container = entry.getValue();
                List variableValue = container.getValue();

                Object value = null;
                if (variableValue.size() == 1) {
                    value = variableValue.get(0);
                } else if (variableValue.size() > 1) {
                    value = variableValue;
                }
                result.put(variable, value);
            }
        }
        return result;

    }

    public EditorInputContainer findEditorInputContainer(String variable) {
        TemplateTabPane tabs = this.editor.getTemplateTabPane();
        for (Map.Entry<String, TemplateTab> tabEntry : tabs.getTemplateTabMap().entrySet()) {
            TemplateTab tab = tabEntry.getValue();
            Map<String, EditorInputContainer> inputContainerMap = tab.getVariableInputContainerMap();
            if (inputContainerMap.containsKey(variable)) {
                return inputContainerMap.get(variable);
            }
        }
        return null;
    }

    private class FileNode {
        private File file;

        public FileNode(File file) {
            this.file = file;
        }

        @Override
        public String toString() {
            String name = file.getName();
            if (name.equals("")) {
                return file.getAbsolutePath();
            } else {
                return name;
            }
        }

        public File getFile() {
            return file;
        }
    }
    
    public Template getCurrentTemplate() {
    	return currentTemplate;
    }
    
    public Map getDefaultValueMap() {
    	return defaultValueMap;
    }
    
    public Map getExpectedValueMap() {
    	return expectedValueMap;
    }

	public void addDefaultValues(String variable, List<String> values) {
		defaultValueMap.put(variable, values);
	}

	public void presentExtractedValues() {
		Map<String, TemplateTab> tabMap = this.editor.getTemplateTabPane().getTemplateTabMap();
        for (TemplateTab tab : tabMap.values()) {
            Map<String, EditorInputContainer> variableComponentMap = tab.getVariableInputContainerMap();
            for (Map.Entry<String, EditorInputContainer> entry : variableComponentMap.entrySet()) {
            	//container variable
                String variable = entry.getKey();
                //container
                EditorInputContainer container = entry.getValue();
                //set extracted value(s) OR default
                container.setValue(variable, expectedValueMap.get(variable), defaultValueMap.get(variable));
            }
        }
	}
}
