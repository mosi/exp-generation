package org.mosi.grease.experimentgeneration.controller;


import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import org.grease.proveance.gui.controller.MainProvenanceViewCotroller;
import org.mosi.grease.backend.BackendController;
import org.mosi.grease.experimentgeneration.controller.generation.controller.*;
import org.mosi.grease.experimentgeneration.model.BackendType;
import org.mosi.grease.experimentgeneration.model.ExperimentModel;
import org.mosi.grease.experimentgeneration.model.Domain;
import org.mosi.grease.experimentgeneration.view.View;
import org.json.JSONObject;
import org.mosi.grease.provenancefiltering.controller.EditJsonExperimentWithGuiController;

import java.io.*;

/**
 * Created by Marcus on 17.12.2018.
 */
public class MainExperimentController {
    private ParsingController parsingController;
    private Configuration configuration;
    private ExperimentModel experimentModel;
    private BackendController backendController;
    private GenerationController generationController;

    private MainProvenanceViewCotroller mainProvenanceViewCotroller;
    private View view;
    private boolean editingExperiment = false; //this will be set to true, when application is used by provenance pattern application to edit an experiment
    EditJsonExperimentWithGuiController editJsonExperimentWithGuiController;
    private boolean skipExecution = false;

    public MainExperimentController(ExperimentModel experimentModel, View view) {
        this.experimentModel = experimentModel;
        this.parsingController = new ParsingController(this);
        this.backendController = new BackendController(this);
        this.generationController = new GenerationController(this);
        this.view = view;
        this.configuration = configureTemplateEngine();

        view.getStage().show();

        view.getGenerateExperimentButton().setOnAction(e -> {

                // This generates and shows the JSON Specification
                experimentModel.generateExperiment(view);

                // If the gui is used to edit an experiment by the provenance-pattern project the generate button sends
                // the experiment as json back to the experimentGeneratorController of the provenance-pattern project
                if(editingExperiment){
                    editJsonExperimentWithGuiController.stopEditingCurrentExperiment(experimentModel);
                }
                // If schema checks passed, generate specification for backend
                if (parsingController.schemaCheckExperimentParameter(experimentModel.getSchemaFile(), experimentModel.getJsonInput())) {
                    // this sets the json specification and backend type in the experiment model
                    generationController.updateExperimentModelFromGui();
                    File experimentFile = generationController.transformJsonToBackendSpecification(
                            experimentModel.getJsonInput());
                    // If backend binding available and file paths specified, execute automatically
                    File modelFile = new File(getModelFilePath(experimentModel.getJsonInput()));
                    try {
                        backendController.executeExperiment(experimentFile, modelFile, "", experimentModel.getBackendType(), false);
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
            }
        );
    }

    private String getModelFilePath(JSONObject jsonInput) {
        if(jsonInput.has("model")){
            JSONObject model = jsonInput.getJSONObject("model");
            if(model.has("modelPath")){
                return model.getString("modelPath");
            }
        }
        return null;
    }

    private Configuration configureTemplateEngine() {
        Configuration cfg = new Configuration();

        ClassTemplateLoader loader = new ClassTemplateLoader(
                    this.getClass(), "/templates");
        cfg.setTemplateLoader(loader);
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);

        // cfg.setDirectoryForTemplateLoading(new File(".\\src\\main\\resources\\Templates"));
        //cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        return cfg;

    }

    public boolean isEditingExperiment() {
        return editingExperiment;
    }

    public void setEditingExperiment(boolean editingExperiment) {
        this.editingExperiment = editingExperiment;
    }

    public View getView() {
        return view;
    }

    public ParsingController getParsingController() {
        return parsingController;
    }

    public ExperimentModel getExperimentModel() {
        return experimentModel;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public void setEditJsonExperimentController(EditJsonExperimentWithGuiController editJsonExperimentWithGuiController) {
        this.editJsonExperimentWithGuiController = editJsonExperimentWithGuiController;
    }

    public MainProvenanceViewCotroller getMainProvenanceViewCotroller() {
        return mainProvenanceViewCotroller;
    }

    public void setMainProvenanceViewCotroller(MainProvenanceViewCotroller mainProvenanceViewCotroller) {
        this.mainProvenanceViewCotroller = mainProvenanceViewCotroller;
    }

    public BackendController getBackendController() {
        return backendController;
    }

    public GenerationController getGenerationController() {
        return generationController;
    }

    public void setSkipExecution(boolean b) {
        this.skipExecution = b;
    }

    public boolean getSkipExecution() {
        return this.skipExecution;
    }
}
