package org.mosi.grease.experimentgeneration.controller.generation.controller;


import javafx.scene.control.TextArea;
import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.mosi.grease.experimentgeneration.controller.MainExperimentController;
import org.mosi.grease.experimentgeneration.model.BackendType;

import org.json.JSONObject;
import org.json.JSONTokener;


import java.io.*;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Marcus on 17.12.2018.
 */
public class ParsingController implements Observer {

    MainExperimentController mainExperimentController;

    public ParsingController(MainExperimentController mainExperimentController){
        this.mainExperimentController = mainExperimentController;

    }

    protected JSONObject getExperimentParameter(File jsonSchema, File jsonInputs) throws IOException {
        String schemaName = jsonSchema.getName();
        String jsonFileName = jsonInputs.getName();


        try(InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(schemaName)){
            JSONObject rawSchema = new JSONObject(new JSONTokener(inputStream));

            InputStream jsonFile = this.getClass().getClassLoader().getResourceAsStream(jsonFileName);
            JSONObject jsonObject = new JSONObject((new JSONTokener(jsonFile)));

            schemaCheckExperimentParameter(rawSchema, jsonObject);

            return jsonObject;
        } catch (IOException e) {
            e.printStackTrace();
        }
       return null;
    }

    /**
     * Validates the JSONObject based on the selected schema
     * @param rawSchema
     * @param jsonObject
     */
    public boolean schemaCheckExperimentParameter(JSONObject rawSchema, JSONObject jsonObject) {
        //use Draft 7 loader
//        SchemaLoader loader = SchemaLoader.builder()
//                .schemaJson(rawSchema)
//                .draftV7Support()
//                .build();
        Schema schema = SchemaLoader.load(rawSchema);
        TextArea logger = mainExperimentController.getView().getLogger();
        try{
            schema.validate(jsonObject);
            mainExperimentController.getView().getLogger().setText("Schema validation successful");
            System.out.println("Schema validation successful");
        } catch (ValidationException e){
            String s ="";
            s = s+ e.getErrorMessage() + "\n";

            for (String message : e.getAllMessages()){
                    s = s + message + "\n";
            }
            logger.setText(s);
            //debug
            //System.out.println(s);
            return false;
        }
        return true;
    }

    public File getFile(String fileName){
        ClassLoader classLoader = getClass().getClassLoader();
        return new File(classLoader.getResource(fileName).getFile());
    }

    protected BackendType identifyBackend(JSONObject jsonObject){
        switch (jsonObject.get("backend").toString()){
            case "Python":{
                return BackendType.PYTHON;
            }
            case "SESSL/ML-Rules":{
                return BackendType.SESSL;
            }
            case "EMStimTools/YAML":
                return BackendType.YAML;
        }
        return null;
    }

    @Override
    public void update(Observable o, Object arg) {

    }
}
