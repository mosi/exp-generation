package org.mosi.grease.experimentgeneration.controller;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import org.mosi.grease.experimentgeneration.view.templates.EditorInputContainer;
import org.mosi.grease.experimentgeneration.view.templates.TemplateTab;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <pre>
 *  The template language used is called Freemarker.
 *
 *  To ensure further understanding of the language we encourage you to visit the documentation website of freemarker.
 *  http://freemarker.org/docs/index.html
 *
 *  This class generates the GUI Editor by gathering data from the directives defined in the .ftl files.
 *  The elements are displayed in a GridBagLayout, in which you're currently able to specify line breaks.
 *  The following paragraphs will be concerned with the syntax description.
 *
 * ______________________________________________________________________________________________________________________________________________________
 *
 *  A directive is <u>always</u> specified with:
 *      &lt;@interpolationData/&gt;
 *
 *
 * The following attributes <u>must</u> be included in (with the exception of one) <u>all</u> directives:
 *          variable            :=Name of the Freemarker interpolation you want to specify; e.g name="interpolation"
 *          type            :=Defines the Java.Swing GUI element you want to instantiate; e.g type="Text" (Currently able to generate CheckBox, ComboBox, Text)
 *
 *
 *
 * Here is a simple example for a TextField  that further defines the interpolation "Create":
 *      &lt;@interpolationData variable="Create", type="TextField", /&gt;
 *
 * The following attribute <u>must</u> be specified in the <u>last</u> directive
 *      lastElement         := Marks the last element and the transition to the actual template. Currently needs to be added to ensure that only
 *                             <u>one</u> instance is generated while processing the template
 *
 * ______________________________________________________________________________________________________________________________________________________
 *
 * The following attributes are <u>optional</u> and enable the user to further specify <u>all</u> interpolations:
 *      trait :=Specifies to which tab the GUI element belongs to. If the element has no trait it will be displayed in the tab called 'General'
 *      e.g trait=" Observation"
 *
 *      label        :=Generates a label supporting the element by informing user what input is to be expected;
 *      e.g label="observationRange"
 *
 *      markEndOfLine       :=Defines the line break mentioned at the beginning. The new element after this will get displayed in a new row;
 *      e.g markEndOfLine="yes"
 *
 * The following attributes are <u>optional</u> and enable the user to further specify <u>certain</u> interpolations:
 *      values              :=Defines values supporting the element. (Used in <u>type="Checkbox" and type="ComboBox"</u>);
 *      e.g values="[Observation, Verification]"
 *
 *      listInput   :=A special attribute that needs further understanding of the freemarker Template Engine. It can't be associated with a normal interpolation
 *                            The template has to include a list sequence. The following example demonstrates how to set up such an Element.
 *                            It is <u>important and even necessary</u> that the text in 'multipleInstances' matches the list sequence names. (Used in JTextField)
 *                            The directive generates a '+' Button that enables the user to generate another instance of this element.
 *                            If another element is generated, it can be deleted by clicking the generated '-' Button or another instance can be added by the generated '+' Button
 *                            The original Element <u>can not be deleted</u>
 *
 *
 *
 *
 *      The directive part:
 *      e.g &lt;@interpolationData name="observe" type="TextField"   includeLabel="Observe Variable: " markEndOfLine = "yes"  includedInTrait=" Verification"  multipleInstances="observe"/&gt;
 *      The template part:
 *      e.g &lt;#list observe as observe/&gt;
 *              observe =  " ${observe} "
 *          &lt;/#list/&gt;
 *
 *
 * ______________________________________________________________________________________________________________________________________________________
 *
 * You're able to list certain elements simultaneously by using the zipMethod function.
 *
 * &lt;@interpolationData  name="modelParameterName" type="TextField"  includeLabel="Function Name: "  multipleInstances="modelParameterName" /&gt;
 *
 * &lt;@interpolationData  name="Intervall" type="TextField"  includeLabel="Function Arguments: " markEndOfLine = "yes" multipleInstances="Intervall"  /&gt;
 *
 *
 * &lt;#list zipMethod("modelParameterName", "Intervall") as functionBlock/&gt;
 *              ${functionBlock["modelParameterName"] and ${functionBlock["Intervall"]
 *          &lt;/#list/&gt;
 *
 * Given that "modelParameterName" contains [1,2] and Intervall [2,3]
 *  the template results to:
 *      1 and 2
 *      2 and 3
 *
 *
 *
 * ______________________________________________________________________________________________________________________________________________________
 *
 * The end of the specification area is initiated with:
 *      &lt;--/&gt;
 * It's sole purpose is to cut out the empty lines that occur after the template is processed.
 *
 * ______________________________________________________________________________________________________________________________________________________
 *
 * The following example underlines the current possibilities:
 *
 * &lt;@interpolationData  name="traits" values="[ Observation, Verification, ParallelExecution]"/&gt;
 *
 * &lt;@interpolationData  name="fileName" type="TextField"   includeLabel="File: " markEndOfLine = "yes" includedInTrait=" Observation"/&gt;
 *
 * &lt;@interpolationData  name="modelParameterName" type="TextField"  includeLabel="Function Name: "  multipleInstances="modelParameterName" /&gt;
 *
 * &lt;@interpolationData  name="Intervall" type="TextField"  includeLabel="Function Arguments: " markEndOfLine = "yes" multipleInstances="Intervall" /&gt;
 *
 * &lt;@interpolationData  name="replications" type="TextField" includeLabel="Replications: " markEndOfLine = "yes" multipleInstances="replications"  /&gt;
 *
 * &lt;@interpolationData  name="AfterWallClockTime" type="ComboBox" values="[AfterWallClockTime, AfterSimTime]"  includeLabel="Stop Condition: " includedInTrait=" ParallelExecution"/&gt;
 *
 * &lt;@interpolationData  name="AfterWallClockTimeSeconds" type="TextField" includedInTrait=" ParallelExecution"/&gt;
 *
 * &lt;@interpolationData  name="AfterSimTime" type="ComboBox" values="[AfterWallClockTime, AfterSimTime]" includedInTrait=" ParallelExecution" /&gt;
 *
 * &lt;@interpolationData  name="AfterSimTimeNumber" type="TextField" markEndOfLine = "yes" includedInTrait=" ParallelExecution" /&gt;
 *
 * &lt;@interpolationData  name="observe" type="TextField"   includeLabel="Observe Variable: " markEndOfLine = "yes"  includedInTrait=" Verification"  multipleInstances="observe"/&gt;
 *
 * &lt;@interpolationData  name="observeAt" type="TextField"  includeLabel="Observation Range: " markEndOfLine = "yes" includedInTrait=" ParallelExecution"/&gt;
 *
 * &lt;@interpolationData  name="resultVariable" type="TextField"  includeLabel="Result Variable: " markEndOfLine = "yes" includedInTrait=" ParallelExecution" lastElement="yes" /&gt;
 *
 * &lt;--/&gt;
 *
 * &lt;#list zipMethod("modelParameterName", "Intervall") as functionBlock/&gt;
 * scan(" ${functionBlock["modelParameterName"]} " &lt;~ (${functionBlock["Intervall"]}))
 * &lt;/#list/&gt;
 *
 * &lt;#list replications as replications/&gt;
 * replications = ${replications}
 * &lt;/#list/&gt;
 *
 * </pre>
 * <p>
 * <img src="./doc-files/MultipleInstances.png" alt="Directive" >
 * <p>
 */
public class TemplateDirective implements TemplateDirectiveModel {
    //TODO: Checkbox Event making tabs visible and invisible
    private HashMap<TemplateTab, Integer> columnHash;
    private HashMap<TemplateTab, Integer> rowHash;
    private TemplateController templateController;

    public TemplateDirective(TemplateController templateController) {
        this.templateController = templateController;
        columnHash = new HashMap();
        rowHash = new HashMap();

    }


    public void execute(Environment env, Map params, TemplateModel[] loopVars, TemplateDirectiveBody body) throws TemplateException, IOException {
        if (this.templateController.isSealed()) {
            return;
        }


        //Has Label Tag
        String variable = params.getOrDefault("variable", "").toString();
        String trait = params.getOrDefault("trait", "General").toString();
        String label = params.getOrDefault("label", "").toString();
        String inputType = params.getOrDefault("type", "").toString();
        String listInput = params.getOrDefault("listInput", "").toString();
        String valueString = params.getOrDefault("values", "").toString();

        TemplateTab tab = this.templateController.getOrCreateTemplateTab(trait);
        this.templateController.addDefaultValues(variable,buildValues(valueString));

        JLabel elementLabel = new JLabel(label);
        EditorInputContainer inputContainer = null;

        switch (inputType) {
            case "CheckBox":
                inputContainer = this.buildCheckbox(params);
                break;
            case "TextField":
                inputContainer = this.buildTextField();
                break;
            case "ComboBox":
                inputContainer = this.buildComboBox(params);
                break;
            default:
                inputContainer = null;
                break;
        }
        if (!listInput.equals("")) {
            inputContainer.showMenuBar();
        }
        inputContainer.setName(variable);
        tab.addRow(elementLabel, inputContainer, variable);

    }

    private EditorInputContainer buildComboBox(Map params) {
        EditorInputContainer comboBoxContainer = new EditorInputContainer(JComboBox.class);
        String valueString = params.getOrDefault("values", "").toString();
        List<String> comboBoxValues = this.buildValues(valueString);
        JComboBox comboBox = new JComboBox(comboBoxValues.toArray());
        comboBoxContainer.addInput(comboBox);
        return comboBoxContainer;
    }

    private List<String> buildValues(String valueString) {
        String[] rawValues = valueString.replace("[", "").replace("]", "").split(",");
        List<String> values = new ArrayList<>();
        for (String rawValue : rawValues) {
            String value = rawValue.trim();
            values.add(value);
        }
        return values;

    }

    private EditorInputContainer buildTextField() {
        EditorInputContainer textFieldContainer = new EditorInputContainer(JTextField.class);
        JTextField textField = new JTextField(30);
        textFieldContainer.addInput(textField);
        return textFieldContainer;
    }

    private EditorInputContainer buildCheckbox(Map params) {
        String valueString = params.getOrDefault("values", "").toString();
        List<String> checkBoxValues = this.buildValues(valueString);
        EditorInputContainer checkBoxGroup = new EditorInputContainer(JCheckBox.class);
        for (String checkBoxValue : checkBoxValues) {
            JCheckBox checkBox = new JCheckBox(checkBoxValue);
            checkBoxGroup.addInput(checkBox);
        }
        return checkBoxGroup;
    }
    
}