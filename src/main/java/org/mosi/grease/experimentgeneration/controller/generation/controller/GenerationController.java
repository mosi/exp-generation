package org.mosi.grease.experimentgeneration.controller.generation.controller;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mosi.grease.experimentgeneration.controller.MainExperimentController;
import org.mosi.grease.experimentgeneration.model.BackendType;
import org.mosi.grease.experimentgeneration.model.Domain;
import org.mosi.grease.experimentgeneration.model.ExperimentModel;
import org.mosi.grease.provenancefiltering.model.entities.experiments.Experiment;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

public class GenerationController {
    MainExperimentController mainExperimentController;
    private final YamlExperimentGenerator yamlExperimentGenerator;
    private final PythonExperimentGenerator pythonExperimentGenerator;
    private final SesslExperimentGenerator sesslExperimentGenerator;

    public GenerationController(MainExperimentController mainExperimentController) {
        this.mainExperimentController = mainExperimentController;
        this.yamlExperimentGenerator = new YamlExperimentGenerator(this);
        this.pythonExperimentGenerator = new PythonExperimentGenerator(this);
        this.sesslExperimentGenerator = new SesslExperimentGenerator(this);
    }

    public File transformJsonToBackendSpecification(JSONObject transformedInputs) {
        if(transformedInputs != null) {
            flattenExperimentInputs(transformedInputs);
            // Render with templates
            Template template = selectTemplate();
            renderTemplate(mainExperimentController.getExperimentModel(), template);
            // write and return experiment file
            return writeExperimentFile(mainExperimentController.getExperimentModel());
        }
        else {
            return null;
        }
    }

    private Template selectTemplate() {
        Domain experimentType = mainExperimentController.getExperimentModel().getDomain();
        BackendType backendType = mainExperimentController.getExperimentModel().getBackendType();
        if (backendType == BackendType.SESSL && experimentType == Domain.STOCHASTIC_DISCRETE_EVENT) {
            try {
                return mainExperimentController.getConfiguration().getTemplate(
                        "SESSL_ML-Rules/stochastic_master.ftl");
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (backendType == BackendType.PYTHON && experimentType == Domain.STOCHASTIC_DISCRETE_EVENT) {
            try {
                return mainExperimentController.getConfiguration().getTemplate(
                        "SESSL_ML-Rules/stochastic_master.ftl");
            } catch (IOException e) {
                e.printStackTrace();
            }

        } else if (backendType == BackendType.YAML && experimentType == Domain.FINITE_ELEMENT){
            try {
                return mainExperimentController.getConfiguration().getTemplate(
                        "YAML_EMStimTools/electromagnetic_master.ftl");
            } catch (IOException e){
                e.printStackTrace();
            }
        }
        return null;
    }

    public void updateExperimentModelFromGui() {
        switch (mainExperimentController.getView().getExperimentTab().getBackendType()) {
            case PYTHON:
                break;
            case SESSL: {
                mainExperimentController.getExperimentModel().setBackendType(BackendType.SESSL);
                break;
            }
            case YAML: {
                mainExperimentController.getExperimentModel().setBackendType(BackendType.YAML);
                break;
            }
        }
    }

    public void flattenExperimentInputs(JSONObject experimentInputs) {
        HashMap flattenedInputs = mainExperimentController.getExperimentModel().getFlattenedInputs();
        flattenedInputs.clear();
        extractExperimentParameter(flattenedInputs, experimentInputs);
        mainExperimentController.getExperimentModel().extractExperimentSpecification(flattenedInputs);
    }

    public void renderTemplate(ExperimentModel experimentModel, Template template) {
        generateExperiment(experimentModel, template);
    }

    public File writeExperimentFile(ExperimentModel experimentModel) {
        File experimentFile = null;
        try {
            String experimentDescription = experimentModel.getExperimentSpecificationInTargetLanguage();
            String experimentName = experimentModel.getExperimentName();

            experimentFile = new File( "src/main/resources/case-studies/TOMACS/experiments/" +
                    File.separator + experimentName + ".scala");
            PrintWriter printWriter = new PrintWriter(experimentFile);
            printWriter.write(experimentDescription);
            printWriter.flush();
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return experimentFile;
    }

    public void generateExperiment(ExperimentModel experimentModel, Template template) {
        try {
            Writer stringWriter = new StringWriter();
            template.process(experimentModel.getExperimentSpecificationInJSON(), stringWriter);
            String experimentDescription = stringWriter.toString();
            experimentModel.setExperimentSpecificationInTargetLanguage(experimentDescription);
            mainExperimentController.getView().getEditorView().getExperimentTextArea().setText(experimentDescription);
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }
    }

    public void extractExperimentParameter(HashMap experimentParameter, JSONObject jsonObject) {
        ExperimentModel experimentModel = mainExperimentController.getExperimentModel();

        LinkedHashMap<String, ArrayList> zipInstances = experimentModel.getZipInstances();
        for (String key : jsonObject.keySet()) {
            Object jsonProperty = jsonObject.get(key);
            //If Property is Json Object
            if (jsonProperty instanceof JSONObject) {
                if (zipInstances.containsKey(key) && zipInstances.get(key).size() >= 2) {
                    for (String objectKey : ((JSONObject) jsonProperty).keySet()) {
                        Object subProperty = ((JSONObject) jsonProperty).get(objectKey);
                        if (subProperty instanceof JSONArray) {
                            ArrayList list;
                            list = (ArrayList) ((JSONArray) subProperty).toList();
                            experimentParameter.put(objectKey, list);
                        }
                    }
                } else {
                    extractExperimentParameter(experimentParameter, (JSONObject) jsonProperty);
                }
            }
            //If Property is JsonArray
            else if (jsonProperty instanceof JSONArray) {
                ArrayList<String> propertyItems = new ArrayList();
                for (Object arrayItem : ((JSONArray) jsonProperty)) {
                    propertyItems.add(arrayItem.toString());
                    experimentParameter.put(key, propertyItems);
                }
            } else {
                experimentParameter.put(key, jsonProperty.toString());
            }
        }
    }

    public void updateExperimentModel(String experimentName, BackendType backendType) {
        mainExperimentController.getExperimentModel().setExperimentName(experimentName);
        mainExperimentController.getExperimentModel().setBackendType(backendType);
        mainExperimentController.getExperimentModel().setDomain(Domain.STOCHASTIC_DISCRETE_EVENT);
//        JSONObject experimentJson = experiment.getJsonObject();
//        HashMap inputMap = (HashMap) experimentJson.toMap();
//        mainExperimentController.getExperimentModel().setExperimentJson(inputMap);
    }
}
