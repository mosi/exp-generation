package org.mosi.grease.experimentgeneration.controller.generation.controller;

import org.mosi.grease.experimentgeneration.model.ExperimentModel;

import java.io.*;

public class SesslExperimentGenerator {
    GenerationController generationController;

    public SesslExperimentGenerator(GenerationController generationController) {
        this.generationController = generationController;
    }

    File writeExperimentFile(ExperimentModel experimentModel) {
        File experimentFile = null;
        try {
            String experimentDescription = experimentModel.getExperimentSpecificationInTargetLanguage();
            String experimentName = experimentModel.getExperimentName();

            experimentFile = new File(generationController.mainExperimentController.getBackendController().BINDINGS_PATH
                    + "sessl-quickstart"+ File.separator + experimentName + ".scala");
            PrintWriter printWriter = new PrintWriter(experimentFile);
            printWriter.write(experimentDescription);
            printWriter.flush();
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return experimentFile;
    }
}
