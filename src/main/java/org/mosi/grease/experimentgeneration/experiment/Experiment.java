package org.mosi.grease.experimentgeneration.experiment;

import freemarker.template.Template;

import org.mosi.grease.experimentgeneration.model.ExperimentModel;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Marcus on 17.12.2018.
 */
public interface Experiment {

     boolean check(JSONObject jsonObject);
     void  adjustModelPath(HashMap parameters);
     void generateExperiment(ExperimentModel experimentModel, Template template);

}
