package org.mosi.grease.experimentgeneration.extraction;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class CSVReader {
	
	private CSVParser csvParser;
	private List<List<String>> records;
	
	public CSVReader(File file) {

        	FileReader reader = null;
			try {
				reader = new FileReader(file);
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			}
			
            try {
				this.csvParser = new CSVParser(reader, CSVFormat.DEFAULT
				        .withFirstRecordAsHeader()
				        .withIgnoreHeaderCase()
				        .withTrim());
				
				records = new ArrayList<List<String>>();
				for(CSVRecord record : csvParser.getRecords()) {
					ArrayList<String> values = new ArrayList<String>();
					for(int i = 0; i < record.size(); i++) {
						values.add(record.get(i));
					}
					records.add(values);
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

            try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

    }
	
	
	public List<List<String>> getRecords() {
		return records;
	}
	
	
	public Map<String,Integer> getHeaderMap() {
		return csvParser.getHeaderMap();
	}
	
	
	
}
