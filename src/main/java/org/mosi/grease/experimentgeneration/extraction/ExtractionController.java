package org.mosi.grease.experimentgeneration.extraction;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.mosi.grease.experimentgeneration.controller.TemplateController;

import freemarker.template.Template;

public class ExtractionController {
	
	private LinkedList<File> documentationFiles;
	private Template currentTemplate;
//	private HashMap<String,Object> identifiedInformation;
	
	public ExtractionController() {
		documentationFiles = new LinkedList<File>();
	}

	public HashMap<String, ArrayList<String>> extractInfo(TemplateController templateController) {
		
		//prepare ontology with metadata
		//initializeOntology();
				
		//prepare data structure... one list for each variable
		HashMap<String,ArrayList<String>> identifiedInformation = new HashMap<String, ArrayList<String>>();
		identifiedInformation = initializeInformationMap(identifiedInformation, templateController.getDefaultValueMap());
		
		//parse CSV files
		for(File documentationFile : documentationFiles) {
			System.out.println("parse CSV File: " + documentationFile.getPath());
			CSVReader csvReader = new CSVReader(documentationFile);
			
			//iterate over vocabulary items and documentations
			identifiedInformation = extractInformationForEachVariable(identifiedInformation, csvReader);
			
		}
		
		//select values
		selectValues(identifiedInformation, templateController.getExpectedValueMap());
		
		//return extracted information to GUI
		return identifiedInformation;
	}

	private Map<String,Object> selectValues(HashMap<String, ArrayList<String>> identifiedInformation, Map<String,Object> expectedValueMap) {
		// select values from the collection of possible values
		for(Map.Entry<String, Object> entry : expectedValueMap.entrySet()) {
			String key = entry.getKey();
			if (identifiedInformation.get(key).size() > 0) {
				if (entry.getValue() instanceof String) {
					expectedValueMap.put(key, identifiedInformation.get(key).get(0));
				} else if (entry.getValue().getClass() == ArrayList.class) {
					expectedValueMap.put(key, identifiedInformation.get(key));
				} 
			}
		}
		
		return expectedValueMap;
	}


	private HashMap<String, ArrayList<String>> initializeInformationMap(HashMap<String, ArrayList<String>> identifiedInformation, Map<String,Object> defaultMap) {
		
		for(String variable : defaultMap.keySet()) {
//			if(entry.getValue() instanceof String) {
//				identifiedInformation.put(entry.getKey(), entry.getValue());
//
//			}
//			else if(entry.getValue().getClass() == ArrayList.class) {
//				identifiedInformation.put(entry.getKey(), new ArrayList<String>());
//
//			}
			identifiedInformation.put(variable, new ArrayList<String>());
		}
		return identifiedInformation;
	}

	private HashMap<String, ArrayList<String>> extractInformationForEachVariable(HashMap<String, ArrayList<String>> identifiedInformation, CSVReader csvReader) {
		
		for(Entry<String, ArrayList<String>> entry : identifiedInformation.entrySet()) {
			ArrayList<String> result = extractValuesForVariable(entry.getKey(), csvReader);
			ArrayList<String> oldList = identifiedInformation.get(entry.getKey());
			oldList.addAll(result);
			identifiedInformation.put(entry.getKey(), oldList);
		}
		
		return identifiedInformation;
		
	}


	private ArrayList<String> extractValuesForVariable(String tv, CSVReader csvReader) {
		ArrayList<String> extracted = new ArrayList<String>();
		//1st headers only
		ArrayList<Integer> collectedColumns = new ArrayList<Integer>();
		for(Map.Entry<String, Integer> headerItem : csvReader.getHeaderMap().entrySet()) {
			if(cleanString(headerItem.getKey()).equals(cleanString(tv))) {
				collectedColumns.add(headerItem.getValue());
			}
		}
		//get corresponding values
		if(!collectedColumns.isEmpty()) {
			for(List<String> record  : csvReader.getRecords()) {
				extracted.add(record.get(collectedColumns.get(0)));
			}
		}
		return extracted;
	}

	public void addFile(File file) {
		documentationFiles.add(file);
	}
	
	public void removeFile(int index) {
		documentationFiles.remove(index);
	}
	
	public int getNumberOfDocumentationFiles( ) {
		return documentationFiles.size();
	}
	
	public String cleanString(String oldString) {
		String newString = "";
		for(char ch : oldString.toCharArray()) {
			if(Character.isLetter(ch)) {
				newString += Character.toLowerCase(ch);
			} 
			else if(Character.isDigit(ch)) {
				newString += ch;
			}
		}
		return newString;
	}

}
