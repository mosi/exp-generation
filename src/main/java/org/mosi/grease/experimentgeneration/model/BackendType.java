package org.mosi.grease.experimentgeneration.model;

/**
 * Created by Marcus on 07.02.2019.
 */
public enum BackendType {
    PYTHON, SESSL, YAML, R_SESSL, R_JULIA, SEDML
}
