package org.mosi.grease.experimentgeneration.model;

public enum ExperimentDesign {
    NONE, PARAMETER_SCAN, LOCAL_SENSITIVITY_ANALYSIS, GLOBAL_SENSITIVITY_ANALYSIS, STATISTICAL_MODEL_CHECKING, CONVERGENCE_TEST
}
