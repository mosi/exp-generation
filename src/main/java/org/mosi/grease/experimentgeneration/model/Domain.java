package org.mosi.grease.experimentgeneration.model;

/**
 * Created by Marcus on 21.10.2019.
 */
public enum Domain {
    STOCHASTIC_DISCRETE_EVENT, HETEROGENEOUS_SYSTEMS, FINITE_ELEMENT
}
