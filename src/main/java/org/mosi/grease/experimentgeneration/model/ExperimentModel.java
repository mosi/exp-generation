package org.mosi.grease.experimentgeneration.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.scalified.tree.TreeNode;
import com.scalified.tree.multinode.ArrayMultiTreeNode;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.util.Pair;
import org.mosi.grease.experimentgeneration.view.View;
import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by Marcus on 07.02.2019.
 */
public class ExperimentModel extends Observable {
    // Experiment "code" after Pipeline
    private String experimentSpecificationInTargetLanguage;

    private String experimentName;

    private Domain domain;
    //Storage of user Inputs that are used within the pipeline to generate the experiment Description
    private HashMap experimentSpecificationInJSON;
    private BackendType backendType;
    //Storage of all User Input Elements //Key is sometimes key of object in schema and sometimes the objects title
    private LinkedHashMap<String, Object> generatedGuiElements;
    //Current Schema with which the GUI is generated and Inputs are checked
    private File jsonSchema;
    private JSONObject schemaFile;
    //Experiment Parameter extracted from JSON User Input Specification
    private HashMap flattenedInputs;
    //Storage for all User Input Tabs
    private HashMap<String, Tab> experimentTabs;
    //Assistant Flag for GridPane position in every Tab
    private HashMap<Tab, Pair<Integer, Integer>> gridPositionMarker;
    //Assistant Flag to mark zip Position within a row(in case new Zip Element is added and Row needs to be re-arranged)
    private HashMap<String, Pair<Integer, Integer>> zipInstanceMarker;
    //Assistant Flag to get Add/Remove Buttons for a given GridRow
    private HashMap<String, ArrayList<Pair<Button, Button>>> zipButtonMarker;
    //Store which enabled element automatically disables another
    private HashMap<String, ArrayList<String>> disabledElements;
    //Store which which enabled element automatically enables another
    private HashMap<String, ArrayList<String>> enabledElements;
    private ArrayList<String> currentlyDisabledElements;
    //Individual Storage for Elements that are dependent on each other
    private LinkedHashMap<String, LinkedHashMap<String, ArrayList<Node>>> zipHash;
    //JSON Specification
    private String jsonSpecification;
    //JSON Object given as Input to Pipeline
    private JSONObject jsonInput;
    private TreeNode<String> experimentRoot;
//    private File schemaDirectory;
    private LinkedHashMap<String, Object> userInputs;
    //Structure to memorize which Instances are deeply connected with each other
    private LinkedHashMap<String, ArrayList> zipInstances;

    public ExperimentModel() {
        this.generatedGuiElements = new LinkedHashMap<>();
        this.flattenedInputs = new HashMap();
        this.experimentTabs = new HashMap<>();
        this.gridPositionMarker = new HashMap<>();
        this.zipInstanceMarker = new HashMap<>();
        this.zipButtonMarker = new HashMap<>();
        this.zipHash = new LinkedHashMap<>();
        this.disabledElements = new HashMap<>();
        this.enabledElements = new HashMap<>();
        this.currentlyDisabledElements = new ArrayList<>();
        this.experimentRoot = new ArrayMultiTreeNode<String>("Experiment");
        this.userInputs = new LinkedHashMap<>();
        this.zipInstances = new LinkedHashMap<>();

        this.jsonSpecification = "";
    }

    private boolean isInteger(String text) {
        try {
            Integer.parseInt(text);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private boolean isDouble(String text) {

        try {
            Double.parseDouble(text);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void pushGridElementsDown(Tab tab, Button button) {
        ScrollPane scrollPane = (ScrollPane) tab.getContent();
        GridPane layout = (GridPane) scrollPane.getContent();

        int rowIndex = GridPane.getRowIndex(button);

        ObservableList<Node> children = layout.getChildren();
        for (Node child : children) {
            Integer childRowIndex = GridPane.getRowIndex(child);
            if (childRowIndex <= rowIndex) {

            } else if (childRowIndex > rowIndex) {
                GridPane.setRowIndex(child, childRowIndex + 1);
            }
        }

    }

    public void addNewRow(Tab tab, Button button, String zipName, String propertyName) {
        ScrollPane scrollPane = (ScrollPane) tab.getContent();
        GridPane layout = (GridPane) scrollPane.getContent();

        layout.requestLayout();

        Integer rowIndex = GridPane.getRowIndex(button);
        ArrayList<Node> rowElements = new ArrayList<>();
        ObservableList<Node> children = layout.getChildren();

        for (Node child : children) {
            if (GridPane.getRowIndex(child) == rowIndex) {
                rowElements.add(child);
            }
        }

        for (Node rowElement : rowElements) {
            Integer columnIndex = GridPane.getColumnIndex(rowElement);
            Integer newRow = GridPane.getRowIndex(rowElement) + 1;

            if (rowElement instanceof Label) {
                Label additionalLabel = new Label(((Label) rowElement).getText());
                layout.add(additionalLabel, columnIndex, newRow);
            } else if (rowElement instanceof TextField) {
                TextField additionalTextField = new TextField();
                additionalTextField.setPromptText(((TextField) rowElement).getPromptText());
                additionalTextField.setId((rowElement).getId());
                if (rowElement.isDisabled()) {
                    additionalTextField.setDisable(true);
                }
                if (!((TextField) rowElement).isEditable()) {
                    additionalTextField.setEditable(false);
                }
                if (zipName.isEmpty()) {
                    ArrayList<Node> list = (ArrayList<Node>) generatedGuiElements.get(propertyName);
                    list.add(additionalTextField);
                } else if (!zipName.isEmpty()) {
                    LinkedHashMap<String, ArrayList<Node>> instance = zipHash.get(zipName);
                    String id = additionalTextField.getId();
                    ArrayList<Node> instanceList = instance.get(id);
                    instanceList.add(additionalTextField);
                }
                layout.add(additionalTextField, columnIndex, newRow);
            } else if (rowElement instanceof Button) {
                Button additionalButton = new Button(((Button) rowElement).getText());
                additionalButton.setId(rowElement.getId());
                layout.add(additionalButton, columnIndex, newRow);
                if (additionalButton.getText().equals("Add")) {
                    additionalButton.setOnAction(e -> {
                        pushGridElementsDown(tab, additionalButton);
                        addNewRow(tab, additionalButton, zipName, propertyName);
                    });
                    if(zipButtonMarker.containsKey(propertyName)) {
                    	zipButtonMarker.get(propertyName).add(new Pair<Button, Button>(additionalButton,additionalButton));
                    }
                } else if (additionalButton.getText().equals("Remove")) {
                    additionalButton.setOnAction(e -> removeRow(tab, additionalButton));
                    if(zipButtonMarker.containsKey(propertyName)) {
                        ArrayList<Pair<Button, Button>> lst = zipButtonMarker.get(propertyName);
                        Pair<Button, Button> pair = lst.get(lst.size() - 1);
                        Button first = pair.getKey();
                        lst.remove(pair);
                        lst.add(new Pair<Button, Button>(first, additionalButton));                    	
                    }
                }
            }
        }
        tab.getTabPane().requestLayout();
    }

    public void pushElementsUp(Tab tab, Button button) {
        ScrollPane scrollPane = (ScrollPane) tab.getContent();
        GridPane layout = (GridPane) scrollPane.getContent();

        Integer rowIndex = GridPane.getRowIndex(button);

        ObservableList<Node> children = layout.getChildren();
        for (Node child : children) {
            Integer childRowIndex = GridPane.getRowIndex(child);
            if (childRowIndex <= rowIndex) {
            } else if (childRowIndex > rowIndex) {
                GridPane.setRowIndex(child, childRowIndex - 1);
            }
        }
        tab.getTabPane().requestLayout();
    }

    public void removeRow(Tab tab, Button button) {
        boolean flag = false;
        ScrollPane scrollPane = (ScrollPane) tab.getContent();
        GridPane layout = (GridPane) scrollPane.getContent();
        Integer rowIndex = GridPane.getRowIndex(button);
        ArrayList<Node> nodes = new ArrayList<>();
        LinkedHashMap<String, ArrayList<Node>> zipHashInstance = zipHash.get(button.getId());
        if (zipHashInstance != null) {
            for (String string : zipHashInstance.keySet()) {
                flag = zipHashInstance.get(string).size() > 1;
            }
            if (flag) {
                for (Node node : layout.getChildren()) {
                    if (GridPane.getRowIndex(node) == rowIndex) {
                        nodes.add(node);
                    }
                }
                for (Node elements : nodes) {
                    generatedGuiElements.remove(elements);
                    for (String key : zipHash.keySet()) {
                        LinkedHashMap<String, ArrayList<Node>> stringArrayListLinkedHashMap = zipHash.get(key);
                        for (String key2 : stringArrayListLinkedHashMap.keySet()) {
                            ArrayList<Node> list = stringArrayListLinkedHashMap.get(key2);
                            list.remove(elements);
                        }
                    }
                    layout.getChildren().remove(elements);
                }
                pushElementsUp(tab, button);
                tab.getTabPane().requestLayout();
            }
        } else {
            Parent parent = button.getParent();
            if (parent instanceof GridPane) {
                GridPane pane = (GridPane) parent;
                pane.getChildren().removeIf(node -> GridPane.getRowIndex(node) == rowIndex);
                pushElementsUp(tab, button);
            }
        }
    }

    /**
     * Clear Data upon changing schema
     */
    public void clearData() {
        this.generatedGuiElements.clear();
        this.flattenedInputs.clear();
        this.experimentTabs.clear();
        this.gridPositionMarker.clear();
        zipInstanceMarker.clear();
        zipButtonMarker.clear();
        zipHash.clear();
        disabledElements.clear();
        enabledElements.clear();
        jsonSpecification = "";
        if (jsonInput != null) {
            while (jsonInput.length() > 0)
                jsonInput.remove(jsonInput.keys().next());
        }
        flattenedInputs.clear();
        userInputs.clear();
        zipInstances.clear();
        experimentRoot.clear();

    }

    public void removeExperimentDesign(View view, ExperimentDesign currentExperimentDesign) {
        String resource = "";

        if(currentExperimentDesign == ExperimentDesign.PARAMETER_SCAN){
            resource = "schemas/Types/scan-schema.json";
        }
        else if(currentExperimentDesign == ExperimentDesign.LOCAL_SENSITIVITY_ANALYSIS){
            resource = "schemas/Types/lsa-schema.json";
        }
        else if(currentExperimentDesign == ExperimentDesign.GLOBAL_SENSITIVITY_ANALYSIS){
            resource = "schemas/Types/gsa-schema.json";
        }
        else if(currentExperimentDesign == ExperimentDesign.STATISTICAL_MODEL_CHECKING){
            resource = "schemas/Types/scm-schema.json";
        }
        else if(currentExperimentDesign == ExperimentDesign.CONVERGENCE_TEST){
            resource = "schemas/Types/convergence-schema.json";
        }
        else if (currentExperimentDesign == ExperimentDesign.NONE){
            return;
        }

        // this is the path within the jar file
        InputStream input = this.getClass().getResourceAsStream("/resources/" + resource);
        if (input == null) {
            // this is how we load file within editor (eg eclipse)
            input = this.getClass().getClassLoader().getResourceAsStream(resource);
        }

        LinkedHashMap<String, Object> map = new LinkedHashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        mapper.getFactory().disable(JsonParser.Feature.AUTO_CLOSE_SOURCE);
        try {
            map = mapper.readValue(input, LinkedHashMap.class);
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        LinkedHashMap<String, Object> designTabs = (LinkedHashMap<String, Object>) map.get("properties");

        for(Object designTabKey : designTabs.keySet()){
            LinkedHashMap<String, Object> designTab = (LinkedHashMap<String, Object>) designTabs.get(designTabKey);
            removeTabWithTitle(view, designTab.get("title"));
        }
    }

    private void removeTabWithTitle(View view, Object title) {
        Tab tab = experimentTabs.get(title);
        ScrollPane content = (ScrollPane) tab.getContent();
        GridPane grid = (GridPane) content.getContent();
        gridPositionMarker.remove(tab);
        for (Node nodes : grid.getChildren()) {
            if (zipInstanceMarker.containsKey(nodes.getId())){
                zipInstanceMarker.remove(nodes.getId());
            }
            if (generatedGuiElements.containsValue(nodes)) {
                generatedGuiElements.values().remove(nodes);

            } else {
                String id = nodes.getId();
                generatedGuiElements.remove(id);
            }

        }
        view.getExperimentTabs().getTabs().remove(tab);
        experimentTabs.remove(title);
    }

    //-----------------------------Handle Edibility:--------------------------------------------------------------------

    public void updateEdibility(HashMap<String, Object> generatedGuiElements, @NotNull ComboBox<String> comboBox) {
        currentlyDisabledElements.clear();
        String selectedItem = comboBox.getSelectionModel().getSelectedItem();
        disableElements(generatedGuiElements, selectedItem);
        enableElements(generatedGuiElements, selectedItem);

    }

    private void enableElements(HashMap<String, Object> generatedGuiElements, String selectedItem) {
        ArrayList<String> elementsToEnable = enabledElements.get(selectedItem);
        if (elementsToEnable == null) {
            elementsToEnable = new ArrayList<>();
        }
        for (String enabledElement : elementsToEnable) {
            Object object = generatedGuiElements.get(enabledElement);
            if (object instanceof TextField) {
                ((TextField) object).setDisable(false);
                ((TextField) object).setEditable(true);
            } else if(object instanceof ComboBox) {
                ((ComboBox) object).setDisable(false);
            } else if (object instanceof ArrayList) {
                ArrayList<TextField> array = ((ArrayList) object);
                for (TextField field : array) {
                    field.setDisable(false);
                    field.setEditable(true);
                }
            }
            else {
                enableHashElements(generatedGuiElements, enabledElement);
            }

            if(zipButtonMarker.containsKey(enabledElement)) {
                for(Pair<Button, Button> buttons : zipButtonMarker.get(enabledElement)) {
                    buttons.getKey().setDisable(false);
                    buttons.getValue().setDisable(false);
                }
            }
        }
    }

    private void disableElements(HashMap<String, Object> generatedGuiElements, String selectedItem) {
        if (disabledElements.containsKey(selectedItem)) {
            ArrayList<String> elementsToDisable = disabledElements.get(selectedItem);
            if (elementsToDisable == null) {
                elementsToDisable = new ArrayList<String>();
            }
            for (String elementTitle : elementsToDisable) {
                currentlyDisabledElements.add(elementTitle);
                if (generatedGuiElements.containsKey(elementTitle)) {
                    Object object = generatedGuiElements.get(elementTitle);
                    if (object instanceof TextField) {
                        ((TextField) object).setEditable(false);
                        ((TextField) object).setDisable(true);
                    } else if(object instanceof ComboBox) {
                        ((ComboBox) object).setDisable(true);
                    } else if (object instanceof ArrayList) {
                        ArrayList<TextField> array = ((ArrayList) object);
                        for (TextField field : array) {
                            field.setEditable(false);
                            field.setDisable(true);
                        }
                    }
                } else {
                    disableHashElements(generatedGuiElements, elementTitle);
                }

                if(zipButtonMarker.containsKey(elementTitle)) {
                    for(Pair<Button, Button> buttons : zipButtonMarker.get(elementTitle)) {
                        buttons.getKey().setDisable(true);
                        buttons.getValue().setDisable(true);
                    }
                }
            }
        }
    }

    private void enableHashElements(HashMap<String, Object> generatedGuiElements, String enabledElement) {
        for (String key : generatedGuiElements.keySet()) {
            Object entry = generatedGuiElements.get(key);
            if (entry instanceof LinkedHashMap) {
                LinkedHashMap entryMap = (LinkedHashMap) entry;

                if (entryMap.containsKey(enabledElement)) {
                    Object elementMap = entryMap.get(enabledElement);
                    if (elementMap instanceof ArrayList) {
                        ArrayList<TextField> array = ((ArrayList) elementMap);
                        for (TextField field : array) {
                            field.setEditable(true);
                            field.setDisable(false);
                        }
                    }
                }
            }
        }
    }

    private void disableHashElements(HashMap<String, Object> generatedGuiElements, String element) {
        for (String key : generatedGuiElements.keySet()) {
            Object entry = generatedGuiElements.get(key);
            if (entry instanceof LinkedHashMap) {
                LinkedHashMap entryMap = (LinkedHashMap) entry;
                if (entryMap.containsKey(element)) {
                    Object elementMap = entryMap.get(element);
                    if (elementMap instanceof ArrayList) {
                        ArrayList<TextField> array = ((ArrayList) elementMap);
                        for (TextField field : array) {
                            field.setEditable(false);
                            field.setDisable(true);
                        }
                    }
                }
            }
        }
    }

    public void updateDisabledElements(String keyword, ArrayList<String> disabledList) {
        disabledElements.put(keyword, disabledList);
    }

    public void updateEnabledElements(String keyword, ArrayList<String> enabledList){
        enabledElements.put(keyword,enabledList);
    }

    public void initialDisable() {
        for (String key : generatedGuiElements.keySet()) {
            Object object = generatedGuiElements.get(key);
            if (object instanceof ComboBox) {
                updateEdibility(generatedGuiElements,(ComboBox<String>) object);
            }
        }
    }

    //--------------------------------parse to json:--------------------------------------------------------------------
    /**
     * Iterates through generated gui elements, extracts inputs and transforms them into JSON
     *
     * @param view
     */
    public void generateExperiment(View view) {
        setExperimentName(view.getExperimentTab().getExperimentName());
        extractExperimentInput(experimentRoot, userInputs);
        displayJsonExperimentSpecification(view);
    }

    private void extractExperimentInput(TreeNode<String> experimentRoot, LinkedHashMap<String, Object> userInputs) {
        //Clear inputs
        userInputs.clear();
        Collection<? extends TreeNode<String>> subtrees = experimentRoot.subtrees();
        for (TreeNode<String> node : subtrees) {
            String data = node.data();
            Object element = generatedGuiElements.get(data);
            //System.out.println(data + " " + element); //debug
            //If Element is basic (String, Array, ..)
            if (node.isLeaf()) {
                if (currentlyDisabledElements.contains(data)) {
                } else {
                    if (element instanceof TextField) {
                        parseTextFieldToJson(userInputs, data, (TextField) element);
                    } else if (element instanceof ArrayList) {
                        parseArrayListToJson(userInputs, data, element);
                    } else if (element instanceof ComboBox) {
                        parseComboBoxToJson(userInputs, data, (ComboBox) element);
                    } else if (element instanceof LinkedHashMap) {
                        parseLinkedHashMapToJson(userInputs, (LinkedHashMap<String, ArrayList<Node>>) element, data);
                    }
                }
            }
            //If Element is an Object
            else if (!node.isLeaf() && !node.isRoot()) {
                if (element instanceof LinkedHashMap) {
                    parseLinkedHashMapToJson(userInputs, (LinkedHashMap<String, ArrayList<Node>>) element, data);
                }
                else {
                    LinkedHashMap<String, Object> nodeMap = new LinkedHashMap<>();
                    extractExperimentInput(node, nodeMap);
                    if (!nodeMap.isEmpty()) {
                        userInputs.put(node.data(), nodeMap);
                    }
                }
            }
        }
    }

    private void displayJsonExperimentSpecification(View view ) {
        generateJsonObject();
        writeJsonInputs(view);
    }

    private void writeJsonInputs(View view) {
        ObservableList<Tab> tabs = view.getEditorView().getTabs();
        for (Tab tab : tabs) {
            if (tab.getId().equals("json")) {
                ((TextArea) tab.getContent()).setText(jsonSpecification);
            }
        }
    }

    private void generateJsonObject() {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        jsonSpecification = gson.toJson(userInputs, LinkedHashMap.class);
        jsonInput = new JSONObject(jsonSpecification);
    }

    private void parseLinkedHashMapToJson(LinkedHashMap<String, Object> userInputs, LinkedHashMap<String, ArrayList<Node>> element, String key) {
        boolean checkIfElementDisabled = false;
        LinkedHashMap<String, ArrayList<Node>> parent = new LinkedHashMap<>();
        for (String keys : element.keySet()) {
            ArrayList<Node> instance = element.get(keys);
            ArrayList inputList = new ArrayList<>();
            for (Node node : instance) {
                if (node instanceof TextField) {
                    String id = node.getId();
                    if (currentlyDisabledElements.contains(id)) {
                        checkIfElementDisabled = true;
                    }
                    String text = ((TextField) node).getText();
                    if (isInteger(text)) {
//                        If its a hex number
//                        if (text.matches("-?[0-9a-fA-F]+")){
//                            inputList.add(text);
//                        }else{
                        int intValue = Integer.parseInt(text);
                        inputList.add(intValue);
//                        }
                    } else if (isDouble(text)) {
//                        if (text.matches("-?[0-9a-fA-F]+")){
//                            inputList.add(text);
//                        }else{
                        double value = Double.parseDouble(text);
                        inputList.add(value);

//                        }
                    } else if (text.isEmpty()) {
                        //omit from json
                    } else {
                        inputList.add(text);
                    }
                }
            }
            if (!checkIfElementDisabled) {
                if (!inputList.isEmpty()) {
                    parent.put(keys, inputList);
                }
            }
        }
        if (!parent.isEmpty()) {
            userInputs.put(key, parent);
        }
    }

    private void parseComboBoxToJson(LinkedHashMap<String, Object> userInputs, String key, ComboBox element) {
        userInputs.put(key, element.getSelectionModel().getSelectedItem().toString());
    }

    private void parseTextFieldToJson(LinkedHashMap<String, Object> userInputs, String key, TextField element) {

        if (!currentlyDisabledElements.contains(element.getId())) {
            if(element.getText().isEmpty()) {
                // omit from json
            } else if (isInteger(element.getText())) {
                int intValue = Integer.parseInt(element.getText());
                userInputs.put(key, intValue);
            } else if (element.getText().matches("-?[0-9a-fA-F]+")) {
                userInputs.put(key, element.getText());
            } else if (isDouble(element.getText())) {
                Double doubleValue = Double.parseDouble(element.getText());
                userInputs.put(key, doubleValue);
            } else {
                userInputs.put(key, element.getText());
            }
        }
    }

    private void parseArrayListToJson(LinkedHashMap<String, Object> userInputs, String key, Object element) {
        ArrayList<Node> nodeList = (ArrayList<Node>) element;
        ArrayList list = new ArrayList<>();
        for (Node node : nodeList) {
            if (node instanceof TextField) {
                if (!currentlyDisabledElements.contains(node.getId())) {
                    //If Input is a number
                    String text = ((TextField) node).getText();
//                    System.out.println(text);
                    if (isInteger(text)) {
                        int intValue = Integer.parseInt(text);
                        list.add(intValue);
                    } else if (isDouble(text)) {
                        float floatValue = Float.parseFloat(text);
                        list.add(floatValue);
                    } else if (text.isEmpty()) {
                        //omit field from json
                    } else {
                        list.add(text);
                    }
                }
            }
        }
        if (!list.isEmpty()) {
            userInputs.put(key, list);
        }
    }

    //------------------------------getter and setter:------------------------------------------------------------------

    public LinkedHashMap<String, Object> getGeneratedGuiElements() {
        return generatedGuiElements;
    }

    public HashMap getExperimentSpecificationInJSON() {
        return experimentSpecificationInJSON;
    }

    public String getExperimentSpecificationInTargetLanguage() {
        return experimentSpecificationInTargetLanguage;
    }

    public void setExperimentSpecificationInTargetLanguage(String experimentSpecificationInTargetLanguage) {
        this.experimentSpecificationInTargetLanguage = experimentSpecificationInTargetLanguage;
    }

    public void setExperimentName(String experimentName) {
        this.experimentName = experimentName;
    }

    public BackendType getBackendType() {
        return backendType;
    }

    public void setBackendType(BackendType backendType) {
        this.backendType = backendType;
    }

    public HashMap getFlattenedInputs() {
        return flattenedInputs;
    }

    //Fill experimentSpecification
    public void extractExperimentSpecification(HashMap specification) {
        this.experimentSpecificationInJSON = specification;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    public LinkedHashMap<String, ArrayList> getZipInstances() {
        return zipInstances;
    }

    public TreeNode<String> getExperimentRoot() {
        return experimentRoot;
    }

    public JSONObject getSchemaFile() {
        return schemaFile;
    }

    public void setSchemaFile(JSONObject schemaFile) {
        this.schemaFile = schemaFile;
    }

    public JSONObject getJsonInput() {
        return jsonInput;
    }

    public String getJsonSpecification() {
        return jsonSpecification;
    }

    public LinkedHashMap<String, LinkedHashMap<String, ArrayList<Node>>> getZipHash() {
        return zipHash;
    }

    public HashMap<String, ArrayList<Pair<Button, Button>>> getZipButtonMarker() {
        return zipButtonMarker;
    }

    public HashMap<String, Pair<Integer, Integer>> getZipInstanceMarker() {
        return zipInstanceMarker;
    }

    public HashMap<Tab, Pair<Integer, Integer>> getGridPositionMarker() {
        return gridPositionMarker;
    }

    public HashMap<String, Tab> getExperimentTabs() {
        return experimentTabs;
    }

    public HashMap<String, ArrayList<String>> getEnabledElements() {
        return enabledElements;
    }

    public String getExperimentName() {
        return experimentName;
    }

    public void setExperimentJson(HashMap json) {
        experimentSpecificationInJSON = json;
    }

}
