package org.mosi.grease.backend;

import junit.framework.Assert;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.mosi.grease.experimentgeneration.model.BackendType;
import org.mosi.grease.provenancefiltering.model.entities.experiments.Experiment;
import org.mosi.grease.provenancefiltering.model.enums.ExperimentRole;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class SesslBackendControllerTest {

    @Test
    public void testRunningExperiment() throws IOException {
        String pathToExperimentFiles = "src"+ File.separator +"main"+ File.separator +"resources"+ File.separator
                +"case-studies"+ File.separator +"TOMACS" + File.separator + "experimens" + File.separator;

        String pathToModelFiles = "src"+ File.separator +"main"+ File.separator +"resources"+ File.separator
                +"case-studies"+ File.separator +"TOMACS" + File.separator + "models" + File.separator;

        String pathToExperimentResults = "src"+ File.separator +"main"+ File.separator +"resources"+ File.separator
                +"case-studies"+ File.separator +"TOMACS" + File.separator + "dataGeneratedByExperiments" + File.separator;

        //To execute an experiment we need the path to the experiment file and the model file:
        Experiment experiment = new Experiment("testExperiment","testStudy", ExperimentRole.UNKNOWN);
        experiment.setReference(pathToExperimentFiles + "Example.scala" );
        experiment.setModelPath(pathToModelFiles + "prey-predator.mlrj");

        BackendController backendController = new BackendController(null);

        File experimentFile = new File(experiment.getReference());
        File modelFile = new File(experiment.getModelPath());
        String result = backendController.executeExperiment(experimentFile, modelFile, "", BackendType.SESSL, false);
        Path targetForResult = Paths.get(pathToExperimentResults + new File(result).getName());

        try {
            FileUtils.copyDirectory(new File(result), targetForResult.toFile());
            FileUtils.deleteDirectory(new File(result));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testCopyingFileToBindingsPath() throws IOException {
        String pathToExperimentFiles = "src"+ File.separator +"main"+ File.separator +"resources"+ File.separator
                +"case-studies"+ File.separator +"TOMACS" + File.separator + "experiments" + File.separator;
        File experimentFile = new File(pathToExperimentFiles + "Example.scala");
        BackendController backendController = new BackendController(null);
        SesslBackendController sesslBackendController = backendController.getSesslBackendController();
        File resultFile = sesslBackendController.copyExperimentFileToBindingsPath(experimentFile.toPath());

        String pathQuickstartDict = "src"+ File.separator +"main"+ File.separator +"resources"+ File.separator +"bindings"+ File.separator +"sessl-quickstart" + File.separator;
        File quickstartDictionaryPath = new File(pathQuickstartDict);
        String contents[] = quickstartDictionaryPath.list();
        for(String content : contents){
            if(content.startsWith("Example")){
                assertTrue(true);
                FileUtils.forceDelete(resultFile);
                return;
            }
        }
        fail();
    }

    @Test
    public void testRunningSESSL() throws IOException, InterruptedException {
        String pathQuickstartDict = "src" + File.separator + "main" + File.separator + "resources" + File.separator +
                "bindings" + File.separator + "sessl-quickstart" + File.separator;
        File experimentFile = new File(pathQuickstartDict + "Example.scala");
        File modelFile = new File(pathQuickstartDict + "prey-predator.mlrj");
        BackendController backendController = new BackendController(null);
        String result = backendController.executeExperiment(experimentFile, modelFile, "", BackendType.SESSL, false);
        Assert.assertNotNull(result);
        FileUtils.deleteDirectory(new File(result));
    }

    @Test
    public void testCmd() throws IOException {
        String pathToquickstartDict = "src"+ File.separator +"main"+ File.separator +"resources"+ File.separator +"bindings"+ File.separator +"sessl-quickstart" + File.separator;
        ProcessBuilder processBuilder = new ProcessBuilder("cmd", "/C", "run.bat");
        File dir = new File(pathToquickstartDict);
        if (dir.exists()){
            //System.out.println("Directory Found");
        }
        processBuilder.directory(dir);
        System.out.println("Running Experiment");

        processBuilder.start();
    }

}