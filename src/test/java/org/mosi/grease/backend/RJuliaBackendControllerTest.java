package org.mosi.grease.backend;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.*;

class RJuliaBackendControllerTest {

    @Test
    void remoteExecutionTest() {
        BackendController backendController = new BackendController(null);
        RJuliaBackendController rJuliaBackendController = new RJuliaBackendController(backendController);
        File expFile = new File("src/test/resources/org/mosi/grease/backend/testExperiments/E1.sh");
        File modelFile = new File("src/test/resources/org/mosi/grease/backend/testModels/M1.txt");
        String modelProvName = "M3".toLowerCase();
        String resultPath = rJuliaBackendController.executeRJuliaExperimentAndGetResult(expFile, modelFile, modelProvName, true);
        assertNotNull(resultPath);
    }
}