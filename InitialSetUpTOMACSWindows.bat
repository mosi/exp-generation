
cd ..

:: download and install provenance editor
git clone -b ProvenanceModel "https://git.informatik.uni-rostock.de/mosi/provenance-editor.git"
cd provenance-editor
call mvn clean install -U
if not "%ERRORLEVEL%" == "0" exit /b
cd ..

:: download and install provenance gui
git clone "https://git.informatik.uni-rostock.de/mosi/GREASE/provenance-gui.git"
cd provenance-gui
call mvn clean install -U
if not "%ERRORLEVEL%" == "0" exit /b
cd ..

:: download case study material - migration
git clone "https://github.com/jasonhilton/screen_run.git --config core.autocrlf=false"
git clone "https://github.com/mhinsch/rgct_data.git --config core.autocrlf=false"

:: download case study material - wnt cross-validation
powershell -Command "& {Invoke-WebRequest -Uri https://www.ebi.ac.uk/biomodels/model/download/BIOMD0000000658.3 -OutFile BIOMD0000000658.zip;Expand-Archive BIOMD0000000658.zip BIOMD0000000658}"
